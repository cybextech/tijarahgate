<div class="container">
    <section class="effectivtool feedback">
        <h2>Thank you for sharing your feedback with us.</h2>
        <p>We regularly conduct different type of user studies including surveys, interviews, and usability studies. Please complete this survey and we'll contact you when a research study looks like a match with your background.</p>
        <p>Your participation will help us to improve our website and in turn provide a better experience for you. In addition, those who qualify and participate in research projects will receive a monetary compensation (checks, coupons or gift cards) as a token of appreciation.</p>
    </section>
</div>
<div class="container border">
    <section class="rang">
        <input value="0,25" type="range" multiple>
    </section>
    <section class="myradio">
        <h2 class="indiv">1. Approximately how long have you been using TijaraGate.com?</h2>
        <p>Please select one:</p>
        <div class="radiodiv">
            <input type="radio" name="rtest" id="rad1" />
            <label for="rad1" title="Unchecked state">Less than 1 month</label>
        </div>
        <div class="radiodiv">
            <input type="radio" name="rtest" id="rad1" />
            <label for="rad1" title="Unchecked state">1 to 12 months</label>
        </div>
        <div class="radiodiv">
            <input type="radio" name="rtest" id="rad1" />
            <label for="rad1" title="Unchecked state">more than 1 year</label>
        </div>
        <div class="radiodiv">
            <input type="radio" name="rtest" id="rad1" />
            <label for="rad1" title="Unchecked state">I have heard of TijaraGate.com, but never used it.</label>
        </div>
    </section>
    <section class="check">
        <h2 class="indiv">2. Which ones of the following categories are you interested in sourcing at TijaraGate.com?</h2>
        <p>Please select all that apply:</p>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Agriculture & Food</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Apparel, Textiles & Accessories</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Auto & Transportation</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Bags, Shoes & Accessories</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Electronics</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Electrical Equipment, Components & Telecom</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Gifts, Sports & Hobbies</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Health & Beauty</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Home, Lights & Construction</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Jewelry, Bags & Shoes</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Machinery, Hardware & Tools</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Packaging, Advertising & Office</label>
        </div>
        <div class="checkdiv">
            <input type="checkbox" />
            <label title="Unchecked state" for="chk1">Other (please specify)</label>
        </div>
        <input class="emptyinput" type="text" />
    </section>
    <section class="sectionbtn">
        <a href="#" class="prevbtn"><img src="<?php echo base_url() ?>/template/front/assets/images/img63.png">Prev</a>
        <a href="#" class="nextbtn">Next<img src="<?php echo base_url() ?>/template/front/assets/images/img62.png"></a>
    </section>
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>