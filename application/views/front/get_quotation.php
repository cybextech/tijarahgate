<script src="<?php echo base_url(); ?>template/front/assets/js/tinymce.min.js"></script>
<div class="container ">
    <div class="quot">
        <h2>Get Quotation</h2>
        <label><i class="fa fa-user" aria-hidden="true"></i>Supplier Company Name Here</label>
        <strong>PRoduct Info</strong>
        <form class="quotForm">
            <div class="air_cond">
                <div class="imgside">
                    <div class="quotImg">
                        <img src="<?php echo base_url(); ?>template/front/assets/images/q_img01.png">
                    </div>
                    <div class="qoutTxt">
                        <p>Air Conditioner Air Cooled Water Chiller and Heat Pump</p>
                        <span><label>Certification</label>: CB,CE,GS,RoHS,SASO</span>
                        <span><label>Type</label>: Split Wall Mounted Air Conditioners</span>
                        <span><label>Use</label>: Room</span>
                        <span><label>Power Source</label>: Electrical Power</span>
                        <span><label>Type</label>: AC </span>
                        <span><label>Condition</label>: New</span>
                    </div>
                </div>
                <div class="slctside">
                    <label>Quantity</label>
                    <input type="text" name="" placeholder="100" />
                    <label>Unit</label>
                    <select>
                        <option value="v1">Piece/s</option>
                        <option value="v2">Option 1</option>
                        <option value="v3">Option 2</option>
                        <option value="v4">Option 3</option>
                        <option value="v5">Option 4</option>
                        <option value="v6">Option 5</option>
                    </select>
                </div>	
            </div>
            <strong>Trade Protection</strong>
            <div class="quotRadio">
                <div class="row">
                    <input type="radio" name="rtest" checked="checked" id="rad1" data-jcf='{"wrapNative": true}' />
                    <label for="rad1" title="Checked state">Option 1</label>
                </div>
                <div class="row">
                    <input type="radio" name="rtest" checked="checked" id="rad2" data-jcf='{"wrapNative": true}' />
                    <label for="rad2" title="Checked state">Option 2</label>
                </div>
                <div class="row">
                    <input type="radio" name="rtest" checked="checked" id="rad3" data-jcf='{"wrapNative": true}' />
                    <label for="rad3" title="Checked state">Option 3</label>
                </div>
            </div>
            <strong>Message</strong>
            <div class="qoutEdit">
                <div class="editor">
                    <textarea>
                    </textarea>
                </div>
                <div class="qoutAttach">
                    <div class="element">
                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                        <span class="myFile">Add Attachments</span>
                        <input type="file" name="" id="vediocam">
                    </div>
                    <p>Remaining Characters: <span> 16000 </span></p>
                </div>
            </div>
            <div class="quotCheck">
                <input type="checkbox" id="chk1" />
                <label title="Unchecked state" for="chk1">I have read and agree to the <a href="#">Tijara Gate Terms & Conditions</a></label>
            </div>
            <ul class="qoutUl">
                <li>
                    <div class="varification">
                        <span>1 + 4 =</span>
                        <input type="text" placeholder="Solve Varification">
                    </div>
                </li>
                <li>
            </ul>	
            <ul class="cancleUl"> 
                <li> 
                    <div class="btn-u btn-u-cust btn-block reg_btn v_logup_btn signup-btn"  > Send Now </div>
                </li>
                <li> 
                    <a href="#">Cancel</a>
                </li>
            </ul>		
        </form>	
    </div>
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });

    //for uploading vedio
    $(".fa-paperclip").click(function () {
        $("#vediocam").trigger('click');
    });

    $('#vediocam').on('change', function () {
        var val = $(this).val();
        $(this).siblings('.myFile').text(val);
    })

    //for tiny text editor
    tinymce.init({
        selector: 'textarea',
        height: 500,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: '//www.tinymce.com/css/codepen.min.css'
    });
</script>