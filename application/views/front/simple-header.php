<body class="header-fixed">
    <div class="wrapper">
        <div class="header-<?php echo $theme_color; ?> ">
            <div class="topbar-v3">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 logo-col">
                            <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/home/">
                                <img id="logo-header" src="<?php echo $this->crud_model->logo('home_top_logo'); ?>" alt="Logo" class="img-responsive" style="width:250px;">
                            </a>
                            <!-- Topbar Navigation -->
                        </div>
                        
                        
                    </div>
                </div><!--/container-->
            </div>
            <!-- End Topbar v3 -->
            <!-- Navbar -->
                   
            <!-- End Navbar -->
        </div>
        <!--=== End Header style1 ===-->
        <style>

            div.shadow {
                max-height:205px;
                min-height:205px;
                overflow:hidden;
                -webkit-transition: all .4s ease;
                -moz-transition: all .4s ease;
                -o-transition: all .4s ease;
                -ms-transition: all .4s ease;
                transition: all .4s ease;
            }
            .shadow:hover {
                background-size: 110% auto !important;
            }

            .custom_item{
                border: 1px solid #ccc;
                border-radius: 4px !important;
                transition: all .2s ease-in-out;
                margin-top:10px !important;	
            }
            .custom_item:hover{
                webkit-transform: translate3d(0, -5px, 0);
                -moz-transform: translate3d(0, -5px, 0);
                -o-transform: translate3d(0, -5px, 0);
                -ms-transform: translate3d(0, -5px, 0);
                transform: translate3d(0, -5px, 0);
                border:1px solid #AB00FF;
            }
            .tab_hov{
                transition: all .5s ease-in-out;	
            }
            .tab_hov:hover{
                opacity:0.7;
                transition: all .5s ease-in-out;
            }
            .tab_hov:active{
                opacity:0.7;
            }
        </style>