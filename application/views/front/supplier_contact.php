<div id="wrapper" class="mainSupp">
    <div class="container ">
        <section class="sup_nav">
            <ul>
                <li><a href="<?php echo base_url() ?>/index.php/home/supplier_home">Home</a></li>
                <li >
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Product Categories
                            <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url() ?>/home/supplier_category">All Categories</a></li>
                            <li><a href="#">Category One</a></li>
                            <li><a href="#">Category Two</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Company Profile
                            <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile">Home</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradingSection">Trading</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#productionSection">Production</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#researchDevelopment">Research & Development</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#buyerInteractions">Buyer Interactions</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#transactionHistory">Transaction History</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradeShows">Trade Shows</a></li>
                        </ul>
                    </div>
                </li>
                <li class="active"><a href="<?php echo base_url() ?>/index.php/home/supplier_contact">Contact</a></li>
            </ul>
        </section>
        <section class="imgBanner">
            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img03.png"></a>
        </section>
        <h1>Contact Us</h1>
        <section class="startOrder">
            <a href="#">Start Order</a>
        </section>
    </div>
    <section class="container">
        <div class="cat_name sendMsge">
            <h2>Send a message to this supplier</h2>
            <form class="contform">
                <div class="row1">
                    <label>To:</label>
                    <span>Supplier Name</span>
                </div>
                <div class="row1">
                    <div>
                        <label>Your Message</label>
                        <div class="inputDiv">
                            <p>Dear Sir/Madam,</p>
                            <p>I'm looking for products with the following specifications:</p>
                            <input type="text" name="" />
                            <div class="element">
                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <span class="myFile"></span>
                                <input type="file" name="" id="vediocam">
                            </div>
                        </div>
                    </div>
                    <span class="styleitalic">Your message must be between 20-8000 characters</span>
                </div>
                <div class="varification">
                    <span>99 + 57 </span>
                    <input type="text" placeholder="Solve Varification">
                </div>
                <input class="inqSbmtbtn" type="submit" value="Send">
            </form>
        </div>	
    </section>
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
    //for uploading vedio
    $(".fa-paperclip").click(function () {
        $("#vediocam").trigger('click');
    });
    $('#vediocam').on('change', function () {
        var val = $(this).val();
        $(this).siblings('.myFile').text(val);
    })
</script>