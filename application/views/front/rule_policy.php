<div id="wrapper " class="rule">
    <!--start of Html for Rule & policy-->
    <section class="finance ">
        <div class="memdetailBan">
            <h2>Policy and Rules</2>
        </div>
    </section>
    <div class="container">
        <section class="rulemain">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="ruleleft">
                        <strong>For Buyers</strong>
                        <ul>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Electronic Communications</a></li>
                            <li><a href="#"> Copyright, Trademarks, Patents, License and Access</a></li>
                            <li><a href="#">Your Account, Reviews, Comments, Communications, and other Content</a></li>
                            <li><a href="#">Copyright Complaints</a></li>
                            <li><a href="#">Risk of Loss, Returns, Refunds & Title</a></li>
                            <li><a href="#"> Products Descriptions</a></li>
                            <li><a href="#"> Pricing</a></li>
                            <li><a href="#">Trade Rules</a></li>
                            <li><a href="#">Posting Rules</a></li>
                            <li><a href="#">Dispute Rules</a></li>
                        </ul>
                        <strong>For Suppliers</strong>
                        <ul>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">lectronic Communications</a></li>
                            <li><a href="#">Copyright, Trademarks, Patents, License and Access</a></li>
                            <li><a href="#">Your Account, Reviews, Comments Communications, And Other Content</a></li>
                            <li><a href="#">Copyright Complaints</a></li>
                            <li><a href="#">Risk of Loss, Returns, Refunds & Title</a></li>
                            <li><a href="#"> Products Descriptions</a></li>
                            <li><a href="#"> Pricing</a></li>
                            <li><a href="#">Trade Rules</a></li>
                            <li><a href="#">Posting Rules</a></li>
                            <li><a href="#">Dispute Rules</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="ruleright">
                        <div class="righthead">
                            <span><i class="fa fa-info-circle" aria-hidden="true"></i>Updated as of June 2016</span>
                            <a href="#" class="print"><i class="fa fa-print" aria-hidden="true"></i>Print this</a>
                        </div>
                        <div class="rightmain">
                            <h2>Privacy Policy </h2>
                            <p>Alibaba.com and Aliexpress.com (each a "Site", together the "Sites") are electronic commerce platforms are predominantly used by business entities to facilitate electronic commerce and such business use does not generally involve the collection of personal information of individuals. Alibaba recognizes the importance of privacy as well as the importance of maintaining the confidentiality of personal information. This Privacy Policy applies to all products and services provided by us and sets out how we may collect, use and disclose information in relation to users of the Sites.</p>
                            <p>You may use our services and products via a mobile device either through mobile applications or mobile optimized websites. This Privacy Policy also applies to such use of our services and products.</p>
                            <p>All capitalized terms not defined in this document shall have the meanings ascribed to them in the Terms of Use of the Site, which can be found here. If you are a user from mainland China, the Alibaba entity that you are contracting with is Alibaba Advertising Co., Ltd. If you are a user from Hong Kong and Macau, you are contracting with Alibaba.com Hong Kong Limited. If you are a user outside mainland China, Hong Kong and Macau, you are contracting with Alibaba.com Singapore E-Commerce Private Limited (�Alibaba�, �we�, �us�, �our�).</p>
                            <strong>A. COLLECTION OF INFORMATION</strong>
                            <p> 1. Your privacy is important to us and we have taken steps to ensure that we do not collect more information from you than is necessary for us to provide you with our services and to protect your account.</p>
                            <p>2. Information including, but not limited to, user name, address, phone number, fax number, email address, gender, date and/or year of birth and user preferences ("Registration Information") may be collected at the time of user registration on the Sites.</p>
                            <p>3. In connection with any transaction and payment services or services under our buyer protection schemes we provide on the Sites, information, including but not limited to, bank account numbers, billing and delivery information, credit/debit card numbers and expiration dates and tracking information from cheques or money orders ("Account Information") may be collected to, among other things, facilitate the sale and purchase as well as the settlement of purchase price of the products or services transacted on or procured through the Sites.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--end of Html for Rule & policy-->
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>