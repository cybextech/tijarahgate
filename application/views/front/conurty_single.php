<?php 
 switch ($country){
     case 'uae':
         $country_full_name = 'United Arab Emirates';
         break;
     case 'kuwait':
         $country_full_name = 'Kuwait';
         break;
     case 'behrain':
         $country_full_name = 'Bahrain';
         break;
     case 'saudiarabia':
         $country_full_name = 'Saudi Arabia';
         break;
     case 'oman':
         $country_full_name = 'Oman';
         break;
     case 'qatar':
         $country_full_name = 'Qatar';
         break;
     case 'india':
         $country_full_name = 'India';
         break;
 }
?>
<div class="container">
    <section class="countrybanner">
        <div class="banimg">
            <img src="<?php echo base_url() ?>/template/front/assets/images/<?php echo $country ?>.jpg" alt="">
        </div>
    </section>
    <section class="effectivtool counsingle">
        <h2>Welcome to <span class="blue"><?php echo $country_full_name ?></span> Trading Page</h2>
        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
    </section>
    <h2 class="heading wholesaler-head">Popular Products from <span class="blue"><?php echo $country_full_name ?></span></h2>
    <section class="WhlslrSlier counsingle">
        <!-- OWL CAROUSEL -->
        <div id="owl-demo" class="owl-carousel owl-theme">
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
            <a href="#">
                <div class="item">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img61.png" alt="Owl Image">
                    <div class="itemtxt">
                        <p>Product Name here</p>
                    </div>	
                </div>
            </a>
        </div>
        <!-- OWL CAROUSEL -->
    </section>
    <h2 class="heading wholesaler-head">Featured Products from <span class="blue"><?php echo $country_full_name ?></span></h2>
    <section class="categories">
        <div class="cat">
            <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img35.png">Metallurgy, Chemicals, Rubber & Plastics</h2>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Minerals & Metallurgy</strong></li>
                    <li><a href="#">Steel</a> </li>
                    <li><a href="#">Metal Scrap</a> </li>
                    <li><a href="#">Aluminum</a> </li>
                    <li><a href="#">Ore</a> </li>
                    <li><a href="#">Ingots</a> </li>
                    <li><a href="#">Magnetic Materials</a> </li>
                    <li><a href="#">Wire Mesh</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Chemicals</strong></li>
                    <li><a href="#">Steel</a> </li>
                    <li><a href="#">Metal Scrap</a> </li>
                    <li><a href="#">Aluminum</a> </li>
                    <li><a href="#">Ore</a> </li>
                    <li><a href="#">Ingots</a> </li>
                    <li><a href="#">Magnetic Materials</a> </li>
                    <li><a href="#">Wire Mesh</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Rubber & Plastics</strong></li>
                    <li><a href="#">Steel</a> </li>
                    <li><a href="#">Metal Scrap</a> </li>
                    <li><a href="#">Aluminum</a> </li>
                    <li><a href="#">Ore</a> </li>
                    <li><a href="#">Ingots</a> </li>
                </ul>
            </div>
        </div>
        <div class="cat">
            <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img36.png">Agriculture & Food</h2>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Agriculture</strong></li>
                    <li><a href="#">Fruit</a> </li>
                    <li><a href="#">Vegetables</a> </li>
                    <li><a href="#">Grain (Rice)</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Food & Beverage</strong></li>
                    <li><a href="#">Seafood</a> </li>
                    <li><a href="#">Dairy</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
            </div>
        </div>
        <div class="cat">
            <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img37.png">Machinery, Industrial Parts & Tools</h2>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Machinery</strong></li>
                    <li><a href="#">Agriculture Machinery & Equipment</a> </li>
                    <li><a href="#">Engineering & Construction Machinery</a> </li>
                    <li><a href="#">Grain (Rice)</a> </li>
                    <li><a href="#">Plastic & Rubber Machinery</a> </li>
                    <li><a href="#">Apparel & Textile Machinery</a> </li>
                    <li><a href="#">GBuilding Material Machinery</a> </li>
                    <li><a href="#">Metal & Metallurgy Machinery</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Mechanical Parts AND Fabrication Services </strong></li>
                    <li><a href="#">Pumps & Parts</a> </li>
                    <li><a href="#">Valves</a> </li>
                    <li><a href="#">Moulds</a> </li>
                    <li><a href="#">Bearings</a> </li>
                    <li><a href="#">Pipe Fittings</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
	$(function() {
		jcf.replaceAll();
	});

	//FOR SLIDER
	$('.bxslider').bxSlider({
	  //auto: true,
	  autoControls: true
	});

	//FOR MULTI IMAGES OWL CAROUSEL
    $(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel({
          itemsCustom : [
            [0, 2],
            [450, 2],
            [600, 3],
            [700, 4],
            [1000, 4],
            [1200, 5],
            [1400, 5],
            [1600, 5]
          ],
          navigation : true
      });
    });
    
     $(document).ready(function() {
      var owl = $("#my-demo");
      owl.owlCarousel({
          itemsCustom : [
            [0, 2],
            [450, 2],
            [600, 3],
            [700, 4],
            [1000, 4],
            [1200, 5],
            [1400, 5],
            [1600, 8]
          ],
          navigation : true
      });
    });
</script>