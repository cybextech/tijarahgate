<div class="comapny_info">
    <div class="page-head">
        <h2>Tijara Gate Company</h2>
    </div>
    <div class="container">
        <div class="page-body-main">
            <section class="about-text">
                <h2 class="about-text-title">About Us</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehe nderitin voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </section>
            <section class="about-text">
                <h2 class="about-text-title">Mission & Vision</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehe nderitin voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </section>
            <section class="about-text">
                <h2 class="about-text-title">Anytime, Anywhere</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehe nderitin voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </section>
            <section class="contact-us">
                <h2 class="about-text-title">Contact Us</h2>
                <div class="conact-us-image">
                    <img src="<?php echo base_url() ?>/uploads/common_images/contact_company_info.jpg">
                </div>
                <div class="contact-us-content">
                    <p>P.O Box 22353 Safat, 13084 Kuwait</p>
                    <p><span>Telephone:</span> <span>(+965) 2491 6111</span></p>
                    <p><span></span><span>(+965) 2492 6111</span></p>
                    <p><span>Fax:</span> <span>(+965) 2491 8111</span></p>
                </div>
            </section>
        </div>
    </div>
</div>