<style>
    .img-holder > img {
        height: auto;
        width: 100%;
    }
    .carousel .slide {
        margin: 0 12px 0 0;
        width: 160px !important;
    }
    .carousel .slide .img-holder {
        height: 160px;
        width: 160px;
    }
    .carousel .slide .heading {
        color: hsl(0, 0%, 20%);
        font-size: 13px;
        line-height: 16px;
        text-align: left;
        text-transform: uppercase;
    }
    .carousel .slide .title {
        color: hsl(36, 52%, 54%);
        font-size: 13px;
        margin: 0;
    }
    .carousel .slide > a {
        color: hsl(195, 100%, 55%);
    }

    .pagination .btn-u-sea {
        background: hsl(0, 0%, 100%) none repeat scroll 0 0;
        border: 1px solid hsl(0, 0%, 83%);
        color: hsl(0, 0%, 83%);
    }
    .pagination .btn-u-red {
        background: hsl(0, 0%, 100%) none repeat scroll 0 0;
        border: 1px solid hsl(196, 100%, 65%);
        color: hsl(196, 100%, 65%);
    }
    .pagination .btn-u {
        margin: 0 0 0 6px;
    }


</style>
<?php
$side_bar_pos = $this->db->get_where('ui_settings', array('type' => "side_bar_pos_category"))->row()->value;
?>
<!--=== Content Part ===-->
<div class="content container product_list_page">
    <div class="row">
        <div class="col-md-3 filter-by-block md-margin-bottom-60 pull-<?php echo $side_bar_pos; ?>-md">
            <!--<h1><?php echo translate('filter_by'); ?></h1>-->

            <div class="panel-group" id="accordion-vy">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion-vy" href="#collapseFoury">
                                <?php echo translate('text'); ?>
                            </a>
                        </h2>
                    </div>
                    <div id="collapseFoury" class="panel-collapse collapse in">
                        <div class="panel-body" >
                            <div class="input-group input-group-lg">
                                <input type="text" id="txtr" value="<?php echo $text; ?>" name="text" class="form-control" >
                                <span class="input-group-btn">
                                    <span class="btn btn-input_type custom srch" ><span class="glyphicon glyphicon-search"></span></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <?php echo translate('category'); ?>
                            </a>
                        </h2>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body sky-form">
                            <ul class="list-unstyled checkbox-list">
                                <li>
                                    <label class="radio state-success">
                                        <input type="radio" name="checkbox" onclick="sub_clear();"  class="check_category"
                                               value="0" />
                                        <i class="rounded-x" style="font-size:30px !important;"></i>
                                        <?php echo translate('all_categories'); ?> 
                                    </label> 
                                </li>
                                <?php
                                foreach ($all_category as $row) {
                                    ?>
                                    <li>
                                        <label class="radio state-success">
                                            <input type="radio" name="checkbox"  class="check_category"
                                                   onclick="sub_clear(); toggle_subs(<?php echo $row['category_id']; ?>);" 
                                                   value="<?php echo $row['category_id']; ?>" />
                                            <i class="rounded-x" style="font-size:30px !important;"></i>
                                            <?php echo $row['category_name']; ?> 
                                            <small>
                                                (<?php echo $this->db->get_where('product', array('category' => $row['category_id'], 'status' => 'ok'))->num_rows(); ?>)
                                            </small>
                                        </label> 
                                    </li>
                                    <li>
                                        <ul class="list-unstyled checkbox-list sub_cat" style="display:none;" id="subs_<?php echo $row['category_id']; ?>">
                                            <?php
                                            $sub_category = $this->db->get_where('sub_category', array('category' => $row['category_id']))->result_array();
                                            foreach ($sub_category as $row1) {
                                                ?>
                                                <li>
                                                    <label class="checkbox state-success">
                                                        <input type="checkbox" name="check_<?php echo $row['category_id']; ?>"  
                                                               class="check_sub_category"
                                                               onclick="filter('click', 'none', 'none', '0')" 
                                                               value="<?php echo $row1['sub_category_id']; ?>" />
                                                        <i class="square-x"></i>
                                                        <?php echo $row1['sub_category_name']; ?> 
                                                        <small>
                                                            (<?php echo $this->db->get_where('product', array('sub_category' => $row1['sub_category_id'], 'status' => 'ok'))->num_rows(); ?>)
                                                        </small>
                                                    </label>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>        
                        </div>
                    </div>
                </div>
            </div><!--/end panel group-->

            <?php
            if ($vendor_system == 'ok') {
                ?>

                <div class="panel-group" id="accordion-v4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title bg-green">
                                <a data-toggle="collapse" data-parent="#accordion-v4" href="#collapseFour">
                                    <?php echo translate('price'); ?>
                                </a>
                            </h2>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse in">
                            <div class="panel-body" id="range">
                                <input type="text" id="rangelvl" value="<?php echo $range; ?>" name="range" />
                            </div>
                        </div>
                    </div>
                </div>
                <!--/end panel group-->
                <?php
            }
            ?>
            <div class="panel-group supplier-side" id="accordion1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title blue-bg">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapsevendor">
                                <?php echo translate('supplier'); ?>
                            </a>
                        </h2>
                    </div>
                    <div id="collapsevendor" class="panel-collapse collapse in">
                        <div class="panel-body sky-form">
                            <ul class="list-unstyled checkbox-list">
                                <li>
                                    <label class="radio state-success">
                                        <input type="radio" name="vendor"  class="check_vendor"
                                               value="0" />
                                        <i class="rounded-x" style="font-size:30px !important;"></i>
                                        <?php echo translate('all_vendors'); ?> 
                                    </label> 
                                </li>
                                <?php
                                $vendors = $this->db->get_where('vendor', array('status' => 'approved'))->result_array();
                                foreach ($vendors as $row) {
                                    ?>
                                    <li>
                                        <label class="radio state-success">
                                            <input type="radio" name="vendor"  class="check_vendor"
                                                   value="<?php echo $row['vendor_id']; ?>" />
                                            <i class="rounded-x" style="font-size:30px !important;"></i>
                                            <?php echo $row['display_name']; ?> 
                                            <small>
                                                (<?php echo $this->db->get_where('product', array('added_by' => '{"type":"vendor","id":"' . $row['vendor_id'] . '"}', 'status' => 'ok'))->num_rows(); ?>)
                                            </small>
                                        </label> 
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>        
                        </div>
                    </div>
                </div>
            </div>
            <section class="addsBanner" style="margin:20px 0;">
                <?php
                $place = 'side_banner';
                $query = $this->db->order_by('banner_id', 'RANDOM');
                $query = $this->db->limit(1);
                $query = $this->db->get_where('banner', array('page' => 'category', 'place' => $place, 'status' => 'ok'));

                $banners = $query->result_array();
                foreach ($banners as $row) {
                    ?>
                    <a href="<?php echo $row['link']; ?>">
                        <img src="<?php echo $this->crud_model->file_view('banner', $row['banner_id'], '', '', 'no', 'src') ?>">
                    </a>
                    <?php
                }
                ?>
            </section>
            <!--=== home banner ===-->
            <div class="row margin-bottom-20">
                <?php
                $place = 'after_filters';
                $query = $this->db->get_where('banner', array('page' => 'category', 'place' => $place, 'status' => 'ok'));
                $banners = $query->result_array();
                if ($query->num_rows() > 0) {
                    $r = 12 / $query->num_rows();
                }
                foreach ($banners as $row) {
                    ?>
                    <a href="<?php echo $row['link']; ?>" >
                        <div class="col-md-<?php echo $r; ?> md-margin-bottom-30">
                            <div class="overflow-h">
                                <div class="illustration-v1 illustration-img1">
                                    <div class="illustration-bg banner_cat" 
                                         style="background:url('<?php echo $this->crud_model->file_view('banner', $row['banner_id'], '', '', 'no', 'src') ?>') no-repeat center center; background-size: 100% auto;" >										
                                    </div>    
                                </div>
                            </div>    
                        </div>
                    </a>
                    <?php
                }
                ?>
            </div>
            <!--=== home banner ===-->

            <!-- MOST SOLD -->
            <!--<div class="sdbar posts margin-bottom-20">
                <h4 class="text_color center_text mr_top_0"><?php echo translate('most_sold'); ?></h4>
            <?php
            $i = 0;
            $most_popular = $this->crud_model->most_sold_products();
            foreach ($most_popular as $row2) {
                $i++;
                if ($i <= 4) {
                    if (!empty($most_popular[$i])) {
                        $now = $this->db->get_where('product', array('product_id' => $most_popular[$i]['id']))->row();
                        ?>
                                                                                                                                                                                                                <dl class="dl-horizontal">
                                                                                                                                                                                                                    <dt>
                                                                                                                                                                                                                        <a href="<?php echo $this->crud_model->product_link($now->product_id); ?>">
                                                                                                                                                                                                                            <img src="<?php echo $this->crud_model->file_view('product', $now->product_id, '', '', 'thumb', 'src', 'multi', 'one'); ?>" alt="" />
                                                                                                                                                                                                                        </a>
                                                                                                                                                                                                                    </dt>
                                                                                                                                                                                                                    <dd>
                                                                                                                                                                                                                        <p>
                                                                                                                                                                                                                            <a href="<?php echo $this->crud_model->product_link($now->product_id); ?>">
                        <?php echo $now->title; ?>
                                                                                                                                                                                                                            </a>
                                                                                                                                                                                                                        </p>
                                                                                                                                                                                                                        <p>
                        <?php if ($this->crud_model->get_type_name_by_id('product', $now->product_id, 'discount') > 0) { ?>
                                                                                                                                                                                                                                                                                            <span>
                            <?php echo currency() . $this->crud_model->get_product_price($now->product_id); ?>
                                                                                                                                                                                                                                                                                            </span>
                                                                                                                                                                                                                                                                                            <span style=" text-decoration: line-through;color:#c9253c;">
                            <?php echo currency() . $now->sale_price; ?>
                                                                                                                                                                                                                                                                                            </span>
                        <?php } else { ?>
                                                                                                                                                                                                                                                                                            <span>
                            <?php echo currency() . $now->sale_price; ?>
                                                                                                                                                                                                                                                                                            </span>
                        <?php } ?>
                                                                                                                                                                                                                        </p>
                                                                                                                                                                                                                    </dd>
                                                                                                                                                                                                                </dl>
                        <?php
                    }
                }
            }
            ?>
            </div>--><!--/posts-->
            <!-- End Posts -->

            <!-- MOST VIEWED -->
            <!--<div class="sdbar posts margin-bottom-20">
                <h4 class="text_color center_text mr_top_0"><?php echo translate('most_viewed_products'); ?></h4>
            <?php
            $this->db->limit(4);
            $this->db->order_by('number_of_view', 'desc');
            $this->db->where('status', 'ok');
            $most_viewed = $this->db->get('product')->result_array();
            foreach ($most_viewed as $row2) {
                ?>
                                                                                <dl class="dl-horizontal">
                                                                                    <dt>
                                                                                        <a href="<?php echo $this->crud_model->product_link($row2['product_id']); ?>">
                                                                                            <img src="<?php echo $this->crud_model->file_view('product', $row2['product_id'], '', '', 'thumb', 'src', 'multi', 'one'); ?>" alt="" />
                                                                                        </a>
                                                                                    </dt>
                                                                                    <dd>
                                                                                        <p>
                                                                                            <a href="<?php echo $this->crud_model->product_link($row2['product_id']); ?>">
                <?php echo $row2['title'] ?>
                                                                                            </a>
                                                                                        </p>
                                                                                        <p>
                <?php if ($this->crud_model->get_type_name_by_id('product', $row2['product_id'], 'discount') > 0) { ?>
                                                                                                                                                            <span>
                    <?php echo currency() . $this->crud_model->get_product_price($row2['product_id']); ?>
                                                                                                                                                            </span>
                                                                                                                                                            <span style=" text-decoration: line-through;color:#c9253c;">
                    <?php echo currency() . $row2['sale_price']; ?>
                                                                                                                                                            </span>
                <?php } else { ?>
                                                                                                                                                            <span>
                    <?php echo currency() . $row2['sale_price']; ?>
                                                                                                                                                            </span>
                <?php } ?>
                                                                                        </p>

                                                                                    </dd>
                                                                                </dl>
                <?php
            }
            ?>
            </div>--><!--/posts-->
            <!-- End Posts -->


        </div>

        <input type="hidden" id="viewtype" value="list" />
        <input type="hidden" id="fload" value="first" />

        <div class="col-md-9 search-content">
            <div class="overflow-h margin-bottom-10">
                <div class="result-category"></div>
                <div class="buying-post">
                    <strong class="txt-some"><span>3,290</span> Selected Supplier(s) you can ask quotations</strong>
                    <a href="#" class="btn-post-buying" data-target="#request_quot" onclick="set_pID_req_quotaion(1)"data-toggle="modal">Send Post Buying Request</a>
                </div>
            </div>
            <div class="row overflow-h margin-bottom-15">
                <div class="col-sm-8"><span class="txt-related-srch">Related Searches:   solar air conditioner, portable air conditioner, split air conditioner, tent air conditioner, air conditioner window, air condition</span></div>
                <div class="col-sm-4">
                    <ul class="list-grid">
                        <li>View as:</li>
                        <li class="grid-list-icons">
                            <span class="viewers" data-typ="list"><i class="fa fa-th-list"></i></span>
                            <span class="viewers" data-typ="grid"><i class="fa fa-th"></i></span>
                        </li>
                    </ul>
                </div>    
            </div><!--/end result category-->

            <div class="filter-results" id="list"></div>
            <div class="text-center pg_links" ></div>
            <!--<ul class="nav-pagination">
                    <li><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i> Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">100</a></li>
                    
                    <li><a href="#">Next <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
            </ul>-->
            <div class="popular-products">
                <h3>Popular Products for "<span class="txt-blue">Air Conditioner</span>"</h3>

                <div class="partner-block carousel">
                    <a href="#" class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                    <a href="#" class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    <div class="mask">
                        <div class="slideset">
                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                        </div>
                    </div>
                </div>




            </div>
            <div class="popular-products">
                <h3>Popular Suppliers for "<span class="txt-blue">Air Conditioner</span>"</h3>

                <div class="partner-block carousel">
                    <a href="#" class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                    <a href="#" class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    <div class="mask">
                        <div class="slideset">
                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" alt="image description"></div>
                                <p class="heading">Supplier Name Here from Oman</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">See Profile >></a>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

            <div class="send-holder">
                <span>Haven't found the right supplier yet ? Let matching verified suppliers find you.</span>
                <a href="#" class="btn-buying-req">Send Post Buying Request</a>
            </div>
            <section class="addsBanner" style="margin:20px 0;">
                <?php
                $place = 'bottom';
                $query = $this->db->order_by('banner_id', 'RANDOM');
                $query = $this->db->limit(1);
                $query = $this->db->get_where('banner', array('page' => 'category', 'place' => $place, 'status' => 'ok'));

                $banners = $query->result_array();
                foreach ($banners as $row) {
                    ?>
                    <a href="<?php echo $row['link']; ?>">
                        <img src="<?php echo $this->crud_model->file_view('banner', $row['banner_id'], '', '', 'no', 'src') ?>">
                    </a>
                    <?php
                }
                ?>

            </section>


            <!--/end filter resilts-->
        </div>

    </div><!--/end row-->
</div><!--/end container-->    
<!--=== End Content Part ===-->

<?php
echo form_open(base_url() . 'index.php/home/listed/', array(
    'method' => 'post',
    'id' => 'plistform',
    'enctype' => 'multipart/form-data'
));
?>
<input type="hidden" name="category" id="categoryaa">
<input type="hidden" name="sub_category" id="sub_categoryaa">
<input type="hidden" name="featured" id="featuredaa">
<input type="hidden" name="range" id="rangeaa">
<input type="hidden" name="text" id="search_text">
<?php
if ($vendor_system == 'ok') {
    ?>
    <input type="hidden" name="vendor" id="vendora">
    <?php
}
?>
</form>
<style>
    .sub_cat{
        padding-left:30px !important;
    }
</style>

<script>
    var range = '<?php echo $range; ?>';
    var cur_sub_category = '<?php echo $cur_sub_category; ?>';
    var cur_category = '<?php echo $cur_category; ?>';
    var search_text = '<?php echo $text; ?>';
    var base_url = '<?php echo base_url(); ?>';
    var tokn_nm = '<?php echo $this->config->item('csrf_token_name'); ?>';
</script>
<script src="<?php echo base_url(); ?>template/front/assets/js/custom/product_list.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/front/assets/js/jquery.carousel.js"></script>