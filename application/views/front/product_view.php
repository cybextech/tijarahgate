<style>
    .container.product_head {
        background: hsl(0, 0%, 100%) none repeat scroll 0 0;
    }
    .breadcrumb-v5 li a {
        color: hsl(0, 0%, 83%);
    }
    .flexslider {
        border-radius: 0;
    }
    #carousel.flexslider {
        border: medium none;
    }
    #carousel .slides li {
        border: 1px solid hsl(0, 0%, 83%) !important;
        margin: 0 5px 0 0;
    }
    #slider #zom {
        bottom: 2px;
        position: absolute;
        z-index: 10;
    }

    .breadcrumb-v5 .active a {
        color: hsl(0, 0%, 0%);
    }
    .shop-product-heading > h2 {
        color: hsl(0, 0%, 20%);
        font-weight: bold;
    }
    .shop-product-heading {
        margin: 7px 0 0;
    }
    .list-inline.inquiry > li {
        margin: 0 0 0 6px;
    }
    .list-inline.inquiry > li:nth-child(2) {
        color: hsl(0, 0%, 50%);
        margin: 0;
    }

    .list-inline.shop-product-prices > li:nth-child(1) {
        color: hsl(0, 0%, 60%);
        font-size: 14px;
        width: 150px;
    }
    .list-inline.shop-product-prices li:nth-child(2) {
        color: hsl(195, 100%, 55%);
        font-size: 16px;
        font-weight: bold;
    }
    .list-inline.shop-product-prices li:nth-child(3) {
        font-size: 16px;
        font-weight: bold;
    }

    .list-inline.product-color > li:nth-child(1) {
        color: hsl(0, 0%, 60%);
        font-size: 14px;
        margin: 0;
        padding: 0;
        width: 150px;
    }

    .list-inline.product-color > li {
        height: 24px;
    }
    .product-color {
        margin: 11px 0 0;
    }

    .list-inline.product-quantity > li:nth-child(1) {
        color: hsl(0, 0%, 60%);
        width: 150px;
    }

    .list-inline.product-quantity {
        height: 37px;
        margin: 15px 0 0;
    }
    .list-inline.product-quantity > li {
        height: 21px;
    }
    .list-inline.genral-option > li:nth-child(1) {
        color: hsl(0, 0%, 60%);
        font-size: 14px;
        width: 150px;
    }
    .list-inline li:nth-child(2) {
        margin: 0 0 0 30px;
    }
    .list-inline li:nth-child(2) {
        margin: 0 0 0 30px;
    }
    .list-inline.ratings_show.product-ratings.margin-bottom-10.tooltips > li {
        margin: 0;
    }
    .line-dash {
        border: 1px dashed hsl(0, 0%, 83%);
        float: left;
        margin: 11px 0 14px;
        width: 100%;
    }

    .order-now .col-md-7 > h3 {
        color: hsl(195, 100%, 55%);
        font-size: 16px;
        text-transform: uppercase;
    }
    .order-now .col-md-7 > p {
        color: hsl(0, 0%, 60%);
    }
    .order-now .col-md-5 > a {
        background: hsl(36, 100%, 50%) none repeat scroll 0 0;
        border-radius: 7px;
        color: hsl(0, 0%, 100%);
        float: right;
        font-size: 27px;
        font-weight: bold;
        margin: 9px 0 0;
        padding: 10px;
        text-align: center;
        width: 222px;
    }
    .get_quotation_under {
        background: #09bec9 none repeat scroll 0 0 !important;
        font-size: 24px !important;
    }

    .add_cart_wish li .btn-u {
        background: hsl(0, 0%, 100%) none repeat scroll 0 0;
        border: medium none !important;
        color: hsl(0, 0%, 60%) !important;
        font-size: 16px;
    }
    .add_cart_wish .list-inline.add_cart_wish a {
        color: hsl(0, 0%, 60%);
    }
    .list-inline.add_cart_wish a {
        color: hsl(0, 0%, 60%);
        font-size: 16px;
    }
    .list-inline.add_cart_wish > li {
        border: 1px solid hsl(0, 0%, 83%);
        float: left;
        height: 41px;
        line-height: 41px;
        margin: 0 0 0 -1px !important;
        text-align: center;
        width: 270px;
    }
    .list-inline.add_cart_wish > li:nth-child(3) {
        line-height: 36px;
    }

    .tab-holder .nav.nav-tabs {
        border: medium none;
    }
    .tab-holder .nav.nav-tabs a {
        border: medium none;
    }
    .tab-holder .active > a {
        border: medium none !important;
        color: hsl(195, 100%, 55%) !important;
    }
    .row.tab-holder {
        margin: 30px 0 0;
    }
    .nav-tabs li > a {
        font-size: 16px;
    }
    .tab-holder .tab-content {
        border: medium none;
    }
    .container.send-inquiry {
        background: hsl(0, 0%, 96%) none repeat scroll 0 0;
        border: 1px solid hsl(0, 0%, 83%);
        margin: 15px auto 0;
    }

    .send-inquiry .reg-block-header {
        float: left;
        margin: 0 0 10px;
        width: 100%;
    }
    .send-inquiry .reg-block-header > h2 {
        margin: 18px 0 0 20px;
        text-transform: none;
    }
    .send-inquiry .col-md-8 {
        float: none;
        margin: auto;
    }
    .send_inquiry_form {
        float: left;
        width: 100%;
    }
    .send-inquiry .input-group {
        float: left;
        width: 100%;
    }
    .send-inquiry .col-md-8 .send_inquiry_form section:nth-child(1) .input-group input {
    }
    #req_quot_form > section:nth-child(3) .input-group input {
        border: 1px solid hsl(0, 0%, 83%);
        border-radius: 5px;
        color: hsl(0, 0%, 83%) !important;
        float: left;
        margin: 0 4px 0 0;
        width: 49%;
    }
    .send-inquiry textarea {
        border: 1px solid hsl(0, 0%, 83%);
        border-radius: 5px !important;
        height: 85px;
        margin: 9px 0 0;
        width: 98.5% !important;
    }
    #req_quot_form > section {
        float: left;
        margin: 3px 0 0;
        width: 100%;
    }
    #req_quot_form p {
        color: hsl(0, 0%, 20%);
        font-size: 14px;
        font-style: italic;
        letter-spacing: 1px;
    }
    .send-inquiry .list-inline > li:nth-child(1) {
        color: hsl(0, 0%, 50%);
        font-size: 15px;
        margin: 2px 0 0;
    }
    .send-inquiry .list-inline {
        height: 41px;
    }
    .send-inquiry .list-inline > li {
        float: left;
    }
    .send-inquiry section .list-inline input:nth-child(1) {
        border: 1px solid hsl(0, 0%, 83%);
        border-radius: 3px;
        height: 28px;
        width: 66px;
    }
    .send_inquiry_form .list-inline > li:nth-child(2) {
        margin: 0;
    }
    .send-inquiry section li select {
        border: 1px solid hsl(0, 0%, 83%);
        border-radius: 2px;
        color: hsl(0, 0%, 83%);
        height: 27px;
        padding: 0 0 0 13px;
        width: 114px;
    }
    #req_quot_form span {
        color: hsl(0, 0%, 50%);
    }
    #req_quot_form > section:nth-child(9) {
        margin: 11px 0 0;
    }
    .send-inquiry .send_inquiry_form section:nth-child(9) input {
        border: 1px solid hsl(0, 0%, 83%);
        width: 141px;
    }
    .send-inquiry .input-group-addon.captcha-partner {
        background: hsl(0, 0%, 93%) none repeat scroll 0 0;
        color: hsl(0, 0%, 0%) !important;
    }
    .send-inquiry section:nth-child(9) label {
        width: 225px;
    }
    .send-inquiry .btn-u.btn-u-cust.btn-block.margin-bottom-20.reg_btn.req_quot_btn {
        border-radius: 5px;
        font-size: 16px;
        font-weight: bold;
        padding: 4px 0 7px;
    }

    .img-holder > img {
        height: auto;
        width: 100%;
    }
    .carousel .slide {
        margin: 0 28px 0 0;
        width: 224px !important;
    }
    .carousel .slide .img-holder {
        height: 199px;
        width: 224px;
    }
    .carousel .slide .heading {
        color: hsl(0, 0%, 20%);
        font-size: 13px;
        line-height: 16px;
        text-align: left;
        text-transform: uppercase;
    }
    .carousel .slide .title {
        color: hsl(36, 52%, 54%);
        font-size: 13px;
        margin: 0;
    }
    .carousel .slide > a {
        color: hsl(195, 100%, 55%);
    }

</style>
<?php
foreach ($product_data as $row) {
    ?>
    <!--=== Shop Product ===-->
    <div class="shop-product">
        <!-- Product Head -->
        <div class="container product_head" style="padding: 0px;">
            <div class="col-md-6" style="padding: 0px;">
                <ul class="breadcrumb-v5">
                    <li><a href="<?php echo base_url(); ?>index.php/home/"><i class="fa fa-home"></i></a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/home/category/"><?php echo translate('products'); ?></a></li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/home/category/<?php echo $row['category']; ?>">
                            <?php echo $this->crud_model->get_type_name_by_id('category', $row['category'], 'category_name'); ?>	 
                        </a>
                    </li>
                    <li >
                        <a href="<?php echo base_url(); ?>index.php/home/category/<?php echo $row['category']; ?>/<?php echo $row['sub_category']; ?>">
                            <?php echo $this->crud_model->get_type_name_by_id('sub_category', $row['sub_category'], 'sub_category_name'); ?>
                        </a>
                    </li>
                    <li class="active">
                        <a href="">
                            <b><?php echo $row['title']; ?></b>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6" style="padding-top:10px;">
                <!--                <div id="share"></div>-->
            </div>
        </div>
        <!-- Product Head -->


        <!-- Product Body -->
        <div class="container" style="border: 1px solid lightgray;">
            <div class="row product_body">
                <div class="col-md-4">
                    <div class="ms-showcase2-template">
                        <div id="slider" class="flexslider" style="overflow:hidden;">
                            <?php
                            $thumbs = $this->crud_model->file_view('product', $row['product_id'], '', '', 'thumb', 'src', 'multi', 'all');
                            $mains = $this->crud_model->file_view('product', $row['product_id'], '', '', 'no', 'src', 'multi', 'all');
                            ?>
                            <ul class="slides">
                                <?php
                                foreach ($mains as $row1) {
                                    ?>
                                    <li class="zoom">
                                        <img src="<?php echo $row1; ?>" class="img-responsive zoom" />
                                    </li>
                                    <?php
                                }
                                ?>
                                <!-- items mirrored twice, total of 12 -->
                            </ul>
                            <span id="zom" class="btn-u btn-u-xs btn-u-cust">
                                <i class="fa fa-search-plus"></i> <?php echo translate('preview'); ?>
                            </span>
                        </div>

                        <?php
                        if (count($mains) > 1) {
                            ?>
                            <div id="carousel" class="flexslider" style="overflow:hidden;">
                                <ul class="slides" >
                                    <?php
                                    $i = 0;
                                    foreach ($thumbs as $row1) {
                                        ?>
                                        <li style="border:4px solid #fff;">
                                            <a class="fancybox-button zoomer" data-rel="fancybox-button" title="<?php echo $row['title'] . ' (' . ($i + 1) . ')'; ?>" href="<?php echo $mains[$i]; ?>" ></a>
                                            <img src="<?php echo $row1; ?>" />

                                        </li>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                </ul>
                            </div>
                            <script>
                                $("#zom").click(function () {
                                    $('.flex-active-slide').find('a').click();
                                });
                            </script>
                            <?php
                        } else if (count($mains) == 1) {
                            ?>
                            <a class="fancybox-button zoomer fancyier" data-rel="fancybox-button" title="<?php echo $row['title']; ?>" href="<?php echo $mains[0]; ?>" ></a>
                            <script>
                                $("#zom").click(function () {
                                    $('.fancyier').click();
                                });
                            </script>
                            <?php
                        }
                        ?>
                    </div>
                    <section class="addsBanner" style="margin-top:30px;">
                        <?php
                        $place = 'after_filter';
                        $query = $this->db->order_by('banner_id', 'RANDOM');
                        $query = $this->db->limit(1);
                        $query = $this->db->get_where('banner', array('page' => 'single', 'place' => $place, 'status' => 'ok'));

                        $banners = $query->result_array();
                        foreach ($banners as $row) {
                            ?>
                            <a href="<?php echo $row['link']; ?>">
                                <img src="<?php echo $this->crud_model->file_view('banner', $row['banner_id'], '', '', 'no', 'src') ?>">
                            </a>

                            <?php
                        }
                        ?>

                    </section>
                </div>
                <div class="col-md-8">
                    <div class="shop-product-heading">
                        <h2><?php echo $row['title']; ?></h2>
                        <div class="col-md-6 shadow-wrapper">       
                        </div>
                    </div><!--/end shop product social-->
                    <div class="stars-ratings inp_rev list-inline" style="display:none;" data-pid='<?php echo $row['product_id']; ?>'>
                        <input type="radio" class="rate_it" name="rating" data-rate="5" id="rate-5">
                        <label for="rate-5"><i class="fa fa-star"></i></label>
                        <input type="radio" class="rate_it" name="rating" data-rate="4" id="rate-4">
                        <label for="rate-4"><i class="fa fa-star"></i></label>
                        <input type="radio" class="rate_it" name="rating" data-rate="3" id="rate-3">
                        <label for="rate-3"><i class="fa fa-star"></i></label>
                        <input type="radio" class="rate_it" name="rating" data-rate="2" id="rate-2">
                        <label for="rate-2"><i class="fa fa-star"></i></label>
                        <input type="radio" class="rate_it" name="rating" data-rate="1" id="rate-1">
                        <label for="rate-1"><i class="fa fa-star"></i></label>
                    </div>
                    <ul class="list-inline ratings_show product-ratings margin-bottom-10 tooltips"
                        data-original-title="<?php echo $rating = $this->crud_model->rating($row['product_id']); ?>"	
                        data-toggle="tooltip" data-placement="left" >
                            <?php
                            $r = $rating;
                            $i = 0;
                            while ($i < 5) {
                                $i++;
                                ?>
                            <li>
                                <i class="rating<?php
                                if ($i <= $rating) {
                                    echo '-selected';
                                } $r--;
                                ?> fa fa-star<?php
                                   if ($r < 1 && $r > 0) {
                                       echo '-half';
                                   }
                                   ?>"></i>
                            </li>
                            <?php
                        }
                        ?>
                        <?php
                        if ($this->session->userdata('user_login') == "yes") {
                            ?>
                            <li class="product-review-list pull-right">
                                <span>
                                    <a href="#" class='rev_show'><?php echo translate('give_a_rating'); ?></a>
                                </span>
                            </li>
                            <?php
                        }
                        ?>
                    </ul><!--/end shop product ratings-->
                    <script>
                        $('body').on('click', '.rev_show', function () {
                            $('.ratings_show li').each(function () {
                                $(this).hide('fast');
                            });
                            $('.inp_rev').show('slow');
                        });
                    </script>

                                                                                                                                                                        <!--<h5 class="text-justify"><?php echo $row['description']; ?></h5>-->
                    <u class="list-inline inquiry">
                        <li>Inquiries</li>
                        <li>200+</li>
                    </u>
                    <ul class="list-inline shop-product-prices">
                        <li>FOB Price</li>:
                        <?php if ($this->crud_model->get_type_name_by_id('product', $row['product_id'], 'discount') > 0) { ?>
                            <li class="shop-violet"><?php echo currency() . $this->crud_model->get_product_price($row['product_id']); ?></li>
                            <li class="line-through"><?php echo currency() . $row['sale_price']; ?></li>
                        <?php } else { ?>
                            <li class="shop-violet"><?php echo currency() . $row['sale_price']; ?></li>
                        <?php } ?>
                    </ul><!--/end shop product prices-->
                    <?php //echo $this->crud_model->is_added_to_cart($row['product_id'],'option'); ?>
                    <ul class="list-inline genral-option">
                        <li>Min.Order Quantity</li>:
                        <li>1 Set/Sets</li>
                    </ul>
                    <ul class="list-inline genral-option">
                        <li>Supply Ability</li>:
                        <li>40000 Set/Sets per Year</li>
                    </ul>
                    <ul class="list-inline genral-option">
                        <li>Port</li>:
                        <li>Kuwait</li>
                    </ul>
                    <ul class="list-inline genral-option">
                        <li>Payment Terms</li>:
                        <li>L/C,T/T,Western Union</li>
                    </ul>
                    <?php
                    echo form_open('', array(
                        'method' => 'post',
                        'class' => 'sky-form',
                    ));
                    ?>
                    <?php
                    $all_op = json_decode($row['options'], true);
                    $all_c = json_decode($row['color']);
                    if ($all_c) {
                        ?>

                        <ul class="list-inline product-color margin-bottom-30">
                            <li>Available Colors</li>:
                            <?php
                            $n = 0;
                            foreach ($all_c as $i => $p) {
                                $c = '';
                                $n++;
                                if ($a = $this->crud_model->is_added_to_cart($row['product_id'], 'option', 'color')) {
                                    if ($a == $p) {
                                        $c = 'checked';
                                    }
                                } else {
                                    if ($n == 1) {
                                        $c = 'checked';
                                    }
                                }
                                ?>
                                <li>
                                    <input type="radio" id="c-<?php echo $i; ?>" value="<?php echo $p; ?>" <?php echo $c; ?> name="color">
                                    <label style="background:<?php echo $p; ?>;border-radius: 26px; height: 35px; width: 35px; border: 1px solid lightgray;" for="c-<?php echo $i; ?>"></label>
                                </li>  
                                <?php
                            }
                        }
                        ?>
                    </ul>
                    <?php
                    if (!empty($all_op)) {
                        foreach ($all_op as $i => $row1) {
                            $type = $row1['type'];
                            $name = $row1['name'];
                            $title = $row1['title'];
                            $option = $row1['option'];
                            ?>
                            <h3 class="shop-product-title"><?php echo $title; ?></h3>
                            <div class="col-md-12 margin-bottom-10">
                                <?php
                                if ($type == 'radio') {
                                    ?>
                                    <?php
                                    foreach ($option as $op) {
                                        ?>
                                        <label class="toggle"><input type="radio" class="optional" name="<?php echo $name; ?>" value="<?php echo $op; ?>" <?php
                                            if ($this->crud_model->is_added_to_cart($row['product_id'], 'option', $name) == $op) {
                                                echo 'checked';
                                            }
                                            ?>  ><i></i><?php echo $op; ?></label>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    } else if ($type == 'text') {
                                        ?>
                                    <label class="textarea textarea-resizable">
                                        <textarea class="optional" rows="5" name="<?php echo $name; ?>"><?php echo $this->crud_model->is_added_to_cart($row['product_id'], 'option', $name); ?></textarea>
                                    </label>
                                    <?php
                                } else if ($type == 'single_select') {
                                    ?>
                                    <label class="select">
                                        <select name="<?php echo $name; ?>" class="optional">
                                            <option value=""><?php echo translate('choose_one'); ?></option>
                                            <?php
                                            foreach ($option as $op) {
                                                ?>
                                                <option value="<?php echo $op; ?>" <?php
                                                if ($this->crud_model->is_added_to_cart($row['product_id'], 'option', $name) == $op) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo $op; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                        </select>
                                        <i></i>
                                    </label>
                                    <?php
                                } else if ($type == 'multi_select') {
                                    ?>
                                    <?php
                                    foreach ($option as $op) {
                                        ?>
                                        <label class="toggle"><input type="checkbox" class="optional" name="<?php echo $name; ?>[]" value="<?php echo $op; ?>" <?php
                                            if (!is_array($chk = $this->crud_model->is_added_to_cart($row['product_id'], 'option', $name))) {
                                                $chk = array();
                                            } if (in_array($op, $chk)) {
                                                echo 'checked';
                                            }
                                            ?>  ><i></i><?php echo $op; ?></label>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                    ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <ul class="list-inline product-quantity">
                        <li><?php
                            if (!$this->crud_model->is_digital($row['product_id'])) {
                                ?>
                                <?php echo translate('quantity'); ?>
                                <?php
                            }
                            ?></li>:
                        <li>
                            <?php
                            if (!$this->crud_model->is_digital($row['product_id'])) {
                                ?>
                                <span class="product-quantity sm-margin-bottom-20">
                                    <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty();' value='-'>-</button>
                                    <input type='text' class="quantity-field cart_quantity" name='qty' value="<?php
                                    if ($a = $this->crud_model->is_added_to_cart($row['product_id'], 'qty')) {
                                        echo $a;
                                    } else {
                                        echo '1';
                                    }
                                    ?>" id='qty'/>
                                    <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty").value++;' value='+'>+</button>
                                </span>

                                <?php
                            } else {
                                ?>
                                <input type='hidden' class="quantity-field cart_quantity" name='qty' value="1" id='qty'/>
                                <?php
                            }
                            ?>
                        </li>
                    </ul>

                    <div class="line-dash"></div>
                    <div class="row order-now">
                        <div class="col-md-1">
                            <img src="<?php echo base_url(); ?>template/front/assets/img/img06.png">
                        </div>
                        <div class="col-md-6">
                            <h3>
                                Trade Protection
                            </h3>
                            <p>
                                Place order online and pay to the designated bank account to get full protection.
                            </p>
                        </div>
                        <div class="col-md-5">
                            <a href="" onclick="to_cart(97)">Order Now</a>

                            <a href="" class="get_quotation_under">Get Quotation</a>
                        </div>
                    </div>

                    <div class="line-dash"></div>

                    <ul class="list-inline add_cart_wish">
                        <li>
                            <button type="button" class="btn-u btn-brd btn-brd-hover rounded btn-u-vio btn-u-xs add_to_cart btn_cart" data-type="text"  data-pid='<?php echo $row['product_id']; ?>'>

                                <?php if ($this->crud_model->is_added_to_cart($row['product_id'])) { ?>
                                    <?php echo translate('added_to_cart'); ?>
                                <?php } else { ?>
                                    <?php echo translate('add_to_cart'); ?>
                                <?php } ?>
                            </button>
                        </li>
                        <li>
                            <?php
                            $wish = $this->crud_model->is_wished($row['product_id']);
                            ?>
                            <button type="button" data-pid='<?php echo $row['product_id']; ?>' 
                                    class="btn-u btn-brd btn-brd-hover rounded btn-u-pink btn-u-xs <?php if ($wish == 'yes') { ?>btn_wished<?php } else { ?>btn_wish<?php } ?>">

                                <?php if ($wish == 'yes') { ?>
                                    <?php echo translate('added_to_wishlist'); ?>
                                <?php } else { ?>
                                    <?php echo translate('add_to_wishlist'); ?>
                                <?php } ?>
                            </button>
                        </li>
                        <li>
                            <a href="">Contact Supplier</a>
                        </li>
                    </ul>
                </div>
                <!--/end product quantity--> 
                </form>
            </div>



            <div class="row tab-holder">
                <?php
                $discus_id = $this->db->get_where('general_settings', array('type' => 'discus_id'))->row()->value;
                $fb_id = $this->db->get_where('general_settings', array('type' => 'fb_comment_api'))->row()->value;
                $comment_type = $this->db->get_where('general_settings', array('type' => 'comment_type'))->row()->value;
                ?>
                <div class="tab-v2">
                    <ul class="nav nav-tabs" role="tablist">
                        <li <?php if ($comment_type == '') { ?>class="active"<?php } ?>><a href="#descrt" role="tab" data-toggle="tab"><?php echo translate('full_description'); ?></a></li>
                        <li><a href="#spec" role="tab" data-toggle="tab"><?php echo translate('additional_specification'); ?></a></li>
                        <li><a href="#shipmnt" role="tab" data-toggle="tab"><?php echo translate('shipment_info'); ?></a></li>
                        <li <?php if ($comment_type !== '') { ?>class="active"<?php } ?>><a href="#reviews" role="tab" data-toggle="tab"><?php echo translate('reviews'); ?></a></li>
                    </ul>

                    <div class="tab-content">
                        <!-- Description -->
                        <div class="tab-pane fade <?php if ($comment_type == '') { ?>in active<?php } ?>" id="descrt">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $row['description']; ?>
                                </div>
                            </div>
                        </div>
                        <!-- End Description -->

                        <!-- Reviews -->                
                        <div class="tab-pane fade" id="shipmnt">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    echo $this->db->get_where('business_settings', array('type' => 'shipment_info'))->row()->value;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- End Reviews --> 

                        <!-- Reviews -->                
                        <div class="tab-pane fade" id="spec">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-sea margin-bottom-40">
                                        <?php
                                        $a = $this->crud_model->get_additional_fields($row['product_id']);
                                        if (count($a) > 0) {
                                            ?>
                                            <div class="panel-heading">
                                                <h2 class="panel-title heading heading-v4" style="font-weight:100;"><?php echo translate('special_specifications'); ?></h2>
                                            </div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <?php
                                                    foreach ($a as $val) {
                                                        ?>
                                                        <tr>
                                                            <td style="text-align:center;"><?php echo $val['name']; ?></td>
                                                            <td style="text-align:center;"><?php echo $val['value']; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </div>       
                                </div>
                            </div>
                        </div>
                        <!-- End Reviews --> 
                        <!-- Reviews -->                
                        <div class="tab-pane fade <?php if ($comment_type !== '') { ?>in active<?php } ?>" id="reviews">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($comment_type == 'disqus') { ?>
                                        <div id="disqus_thread"></div>
                                        <script type="text/javascript">
                                            /* * * CONFIGURATION VARIABLES * * */
                                            var disqus_shortname = '<?php echo $discus_id; ?>';

                                            /* * * DON'T EDIT BELOW THIS LINE * * */
                                            (function () {
                                                var dsq = document.createElement('script');
                                                dsq.type = 'text/javascript';
                                                dsq.async = true;
                                                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                            })();
                                        </script>
                                        <script type="text/javascript">
                                            /* * * CONFIGURATION VARIABLES * * */
                                            var disqus_shortname = '<?php echo $discus_id; ?>';

                                            /* * * DON'T EDIT BELOW THIS LINE * * */
                                            (function () {
                                                var s = document.createElement('script');
                                                s.async = true;
                                                s.type = 'text/javascript';
                                                s.src = '//' + disqus_shortname + '.disqus.com/count.js';
                                                (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
                                            }());
                                        </script>
                                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                                        <?php
                                    } else if ($comment_type == 'facebook') {
                                        ?>

                                        <div id="fb-root"></div>
                                        <script>(function (d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=<?php echo $fb_id; ?>";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>
                                        <div class="fb-comments" data-href="<?php echo $this->crud_model->product_link($row['product_id']); ?>" data-numposts="5"></div>

                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- End Reviews -->                
                    </div>
                </div>
            </div>
        </div><!--/end row-->

        <div class="container send-inquiry">
            <div class="row ">

                <div class="reg-block-header">
                    <h2><?php echo 'Send quick inquiry to this supplier' ?></h2>

                </div>
                <div class="col-md-8">
                    <?php
                    echo form_open(base_url() . 'index.php/home/request_quotation/add_info/', array(
                        'class' => 'send_inquiry_form',
                        'method' => 'post',
                        'style' => 'padding:30px !important;',
                        'id' => 'req_quot_form'
                    ));
                    ?>                
                    <input type="hidden" value="" id="product_id_to_request_quotation" name="product_id">
                    <section>

                        <div class="input-group">
                            <input type="text" placeholder="<?php echo translate('name'); ?>" name="name" class="form-control" >
                            <input type="email" placeholder="<?php echo translate('email_address'); ?>" name="email" class="form-control" >
                        </div>

                    </section>                   
                    <section>

                        <div class="input-group">
                            <textarea name="quotation" class="form-control" placeholder="Write Your Message ....."></textarea>
                        </div>

                    </section>
                    <section>
                        <p>
                            NOTE:For better quotations, include: A self introduction, Special requests, if any. Your message must be between 20-8000 characters.
                        </p>
                    </section>
                    <section>
                        <ul class="list-inline">
                            <li>Quantity:</li>
                            <li>

                                <div class="input-group">
                                    <input type="text" placeholder="<?php echo translate('City'); ?>" name="name" class="form-control" >
                                </div>

                            </li>
                            <li>
                                <select class="custom color" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": true, "useCustomScroll": true}'>
                                    <option value="v1">Sets</option>
                                    <option value="v2">Option 1</option>
                                    <option value="v3">Option 2</option>
                                    <option value="v4">Option 3</option>
                                    <option value="v5">Option 4</option>
                                    <option value="v6">Option 5</option>
                                </select>
                            </li>
                        </ul>
                    </section>
                    <section>

                        <input type="checkbox" id="chk1">
                        <span>Recommend matching suppliers if this supplier doesn�t contact me on Message Center within 24 hours.</span>

                    </section>
                    <section>

                        <input type="checkbox" id="chk2">
                        <span>I agree to share my Business Card to the supplier.</span>

                    </section>
                    <section>
                        <label class="input login-input no-border-top cap-signup">
                            <div class="input-group">
                                <span class="input-group-addon captcha-partner">99 + 57 =</span>
                                <input type="text" class="form-control" name="captcha" placeholder="Solve verification">
                            </div>
                        </label>
                        <div class="col-xs-4 text-right">
                            <div class="btn-u btn-u-cust btn-block margin-bottom-20 reg_btn req_quot_btn" data-ing='<?php echo 'Requesting ..'; ?>' data-msg="" type="submit">
                                <?php echo 'Submit' ?>
                            </div>
                        </div>
                    </section>

                    </form>
                </div>
            </div>
        </div>

        <div class="container you-may-like">
            <div class="popular-products">
                <h3>You may also like:</h3>

                <div class="partner-block carousel">
                    <a href="#" class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                    <a href="#" class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    <div class="mask">
                        <div class="slideset">
                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png" alt="image description"></div>
                                <p class="heading">HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</p>
                                <p class="title">Gold Member - Supplier</p>
                                <a href="">Get Quotation >></a>
                            </div>

                        </div>
                    </div>
                </div>




            </div>
        </div>

    </div>    

    <!--=== End Product Body ===-->




    <?php
}
?>

<script>
    $(document).ready(function () {
        $('#share').share({
            networks: ['facebook', 'googleplus', 'twitter', 'linkedin', 'tumblr', 'in1', 'stumbleupon', 'digg'],
            theme: 'square'
        });
    });

    $(window).load(function () {
<?php
if (count($mains) > 1) {
    ?>
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 100,
                itemMargin: 5,
                asNavFor: '#slider'
            });
    <?php
}
?>

        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
        });
    });

    $(function () {
        $('.zoom').zoome({hoverEf: 'transparent', showZoomState: true, magnifierSize: [200, 200]});
    });

    function destroyZoome(obj) {
        if (obj.parent().hasClass('zm-wrap'))
        {
            obj.unwrap().next().remove();
        }
    }

</script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<script>
    $('body').on('click', '.quantity-button', function () {
        $('.add_to_cart').html('<i class="fa fa-shopping-cart"></i><?php echo translate('add_to_cart'); ?>');
    });
    $('body').on('change', '.optional', function () {
        $('.add_to_cart').html('<i class="fa fa-shopping-cart"></i><?php echo translate('add_to_cart'); ?>');
    });
</script>

<style>
    .heading_alt{
        font-size: 50px;
        font-weight: 100;
        color: #18BA9B;	
    }
</style>

<script>
    var request_sent = 'Your quoutation request is sent.';
    $('body').on('click', '.req_quot_btn', function () {
        var here = $(this); // alert div for show alert message
        var form = here.closest('form');
        var can = '';
        var ing = here.data('ing');
        var msg = here.data('msg');
        var prv = here.html();
        var formdata = false;
        if (window.FormData) {
            formdata = new FormData(form[0]);
        }
        $.ajax({
            url: form.attr('action'), // form action url
            type: 'POST', // form submit method get/post
            dataType: 'html', // request type html/json/xml
            data: formdata ? formdata : form.serialize(), // serialize form data 
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                here.html(ing); // change submit button text
            },
            success: function (data) {
                here.fadeIn();
                here.html(prv);
                if (data == 'done') {
                    here.closest('.modal-content').find('#v_close_logup_modal').click();
                    notify(request_sent, 'success', 'bottom', 'right');
                } else {
                    //here.closest('.modal-content').find('#v_close_logup_modal').click();
                    notify('Request Quotation Errors' + '<br>' + data, 'warning', 'bottom', 'right');
                    //vend_logup();
                }
            },
            error: function (e) {
                console.log(e)
            }
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/front/assets/js/jquery.carousel.js"></script>