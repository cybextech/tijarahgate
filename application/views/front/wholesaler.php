<script src="<?php echo base_url(); ?>template/front/assets/js/jquery.bxslider.min.js"></script>
<!--start of Html for membership-->
<div class="container">
    <section class="wholesalerbanner">
        <ul class="bxslider">
            <li>
                <img src="<?php echo base_url() ?>/template/front/assets/images/img32.png" />
                <div class="bannertext">
                    <strong>Wholesale</strong>
                    <h2>Rice, Wheats & Corns</h2>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url() ?>/template/front/assets/images/img32.png" />
                <div class="bannertext">
                    <strong>Wholesale</strong>
                    <h2>Rice, Wheats & Corns</h2>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url() ?>/template/front/assets/images/img32.png" />
                <div class="bannertext">
                    <strong>Wholesale</strong>
                    <h2>Rice, Wheats & Corns</h2>
                </div>
            </li>
        </ul>
    </section>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.bxslider').bxSlider();
        });
    </script>
    <section class="effectivtool">
        <h2>Wholesaler</h2>
        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
    </section>
    <h2 class="heading wholesaler-head">All Categories</h2>
    <section class="categories">
        <div class="cat">
            <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img35.png">Metallurgy, Chemicals, Rubber & Plastics</h2>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Minerals & Metallurgy</strong></li>
                    <li><a href="#">Steel</a> </li>
                    <li><a href="#">Metal Scrap</a> </li>
                    <li><a href="#">Aluminum</a> </li>
                    <li><a href="#">Ore</a> </li>
                    <li><a href="#">Ingots</a> </li>
                    <li><a href="#">Magnetic Materials</a> </li>
                    <li><a href="#">Wire Mesh</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Chemicals</strong></li>
                    <li><a href="#">Steel</a> </li>
                    <li><a href="#">Metal Scrap</a> </li>
                    <li><a href="#">Aluminum</a> </li>
                    <li><a href="#">Ore</a> </li>
                    <li><a href="#">Ingots</a> </li>
                    <li><a href="#">Magnetic Materials</a> </li>
                    <li><a href="#">Wire Mesh</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Rubber & Plastics</strong></li>
                    <li><a href="#">Steel</a> </li>
                    <li><a href="#">Metal Scrap</a> </li>
                    <li><a href="#">Aluminum</a> </li>
                    <li><a href="#">Ore</a> </li>
                    <li><a href="#">Ingots</a> </li>
                </ul>
            </div>
        </div>
        <div class="cat">
            <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img36.png">Agriculture & Food</h2>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Agriculture</strong></li>
                    <li><a href="#">Fruit</a> </li>
                    <li><a href="#">Vegetables</a> </li>
                    <li><a href="#">Grain (Rice)</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Food & Beverage</strong></li>
                    <li><a href="#">Seafood</a> </li>
                    <li><a href="#">Dairy</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
            </div>
        </div>
        <div class="cat">
            <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img37.png">Machinery, Industrial Parts & Tools</h2>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Machinery</strong></li>
                    <li><a href="#">Agriculture Machinery & Equipment</a> </li>
                    <li><a href="#">Engineering & Construction Machinery</a> </li>
                    <li><a href="#">Grain (Rice)</a> </li>
                    <li><a href="#">Plastic & Rubber Machinery</a> </li>
                    <li><a href="#">Apparel & Textile Machinery</a> </li>
                    <li><a href="#">GBuilding Material Machinery</a> </li>
                    <li><a href="#">Metal & Metallurgy Machinery</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><strong>Mechanical Parts AND Fabrication Services </strong></li>
                    <li><a href="#">Pumps & Parts</a> </li>
                    <li><a href="#">Valves</a> </li>
                    <li><a href="#">Moulds</a> </li>
                    <li><a href="#">Bearings</a> </li>
                    <li><a href="#">Pipe Fittings</a> </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4">
            </div>
        </div>
    </section>
    <h2 class="heading wholesaler-head">Featured Wholesaler</h2>
    <section class="WhlslrSlier">
        <!-- OWL CAROUSEL -->
        <div id="owl-demo" class="owl-carousel owl-theme">
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img38.png" alt="Owl Image">
                <div class="itemtxt">
                    <p>Company/Business Name Here</p>
                    <a href="#">Gold Member</a>
                </div>	
            </div>
        </div>
        <!-- OWL CAROUSEL -->
    </section>
    <h2 class="heading wholesaler-head">Featured Products</h2>
    <section class="WhlslrSlier product">
        <!-- OWL CAROUSEL -->
        <div id="my-demo" class="owl-carousel owl-theme">
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url() ?>/template/front/assets/images/img39.png" alt="Owl Image">
                <div class="itemtxt">
                    <strong>Electronic Cigarette</strong>
                    <p>Brand Name: S-body Battery: 650mah</p>
                    <a href="#">600 items <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- OWL CAROUSEL -->
    </section>
    <div class="">
        <section class="apparel">
            <h2>Apparel & Accessories <a href="#">See all <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></h2>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="apparelMain">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img40.png">
                        <div class="maintxt">
                            <h1>Tank Tops</h1>
                            <h2>From US$ 8.30</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="imgsmall">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="apparel">
            <h2>Consumer Electronics <a href="#">See all <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></h2>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="apparelMain">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img43.png">
                        <div class="maintxt bottom">
                            <h1>Redmi 3 pro</h1>
                            <h2>From US$ 140.60</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="imgsmall">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="apparel">
            <h2>Sports & Entertainment <a href="#">See all <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></h2>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="apparelMain">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img44.png">
                        <div class="maintxt bottom">
                            <h1>Electric Skateboard</h1>
                            <h2>From US$ 140.60</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="imgsmall">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="apparel">
            <h2>Timepieces, Jewelry, Eyewear <a href="#">See all <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></h2>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="apparelMain">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img45.png">
                        <div class="maintxt bottom">
                            <h1>Wedding Ring</h1>
                            <h2>From US$ 140.60</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="imgsmall">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="apparel">
            <h2>Lights & Lighting <a href="#">See all <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></h2>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="apparelMain">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img46.png">
                        <div class="maintxt bottom">
                            <h1>3w Bluetooth speaker led </h1>
                            <h2>From US$ 140.60</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="imgsmall">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img42.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                    <div class="imgsmall imgright bottomimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img41.png">
                        <a href="#" class="veriety">Sexy Underwear</a>
                        <a href="#" class="price">From US$ 0.45</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!--end of Html for membership-->