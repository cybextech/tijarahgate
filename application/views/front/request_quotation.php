<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
           <!-- <center>
                <a href="<?php echo base_url(); ?>">
                    <img id="logo-footer" class="footer-logo img-sm" width='100%'
                         src="<?php echo $this->crud_model->logo('home_bottom_logo'); ?>" alt="">
                </a>
            </center>
            <button aria-hidden="true" data-dismiss="modal" id="v_close_logup_modal" class="close" type="button">×</button>
            <br>
            -->
        </div>
        <div class="modal-body">

            <!--Reg Block-->
            <!--
            <div class="">
                <div class="reg-block-header">
                    <h2><?php echo 'Request Quotation' ?></h2>
                    <p style="font-weight:300 !important;">Request Quotation from the seller of current product</p>
                </div>
                <?php
                echo form_open(base_url() . 'index.php/home/request_quotation/add_info/', array(
                    'class' => 'req_quot sky-form',
                    'method' => 'post',
                    'style' => 'padding:30px !important;',
                    'id' => 'req_quot_form'
                ));
                ?>                
                <input type="hidden" value="" id="product_id_to_request_quotation" name="product_id">
                <section>
                    <label class="input login-input">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" placeholder="<?php echo translate('name'); ?>" name="name" class="form-control" >
                        </div>
                    </label>
                </section>                   
                 
                <section>
                    <label class="input login-input">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" placeholder="<?php echo translate('email_address'); ?>" name="email" class="form-control" >
                        </div>
                    </label>
                </section>
                <section>
                    <label class="input login-input">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                            <textarea name="quotation" class="form-control" placeholder="Write Your Quotaion ....."></textarea>
                        </div>
                    </label>
                </section>
                <div class="row margin-bottom-5">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4">
                        <button id="v_close_logup_modal" class="close" style="color: black; font-size: 14px; margin: 10px 0px 0px; text-decoration: underline;" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    </div>
                    <div class="col-xs-4 text-right">
                        <div class="btn-u btn-u-cust btn-block margin-bottom-20 reg_btn req_quot_btn" data-ing='<?php echo 'Requesting ..'; ?>' data-msg="" type="submit">
                            <?php echo 'Send Request' ?>
                        </div>
                    </div>
                </div>

                </form>
            </div>
            -->
            <div class="pop_quot">
                <h2>Post Buying Request</h2>
                <form class="popForm">
                    <div class="popUpper">
                        <div class="row">
                            <label>Product Name</label>
                            <input type="text" name="" placeholder="Product Name" />
                        </div>
                        <div class="row">
                            <label>Category</label>
                            <select>
                                <option value="v1">Select Category</option>
                                <option value="v2">Option 1</option>
                                <option value="v3">Option 2</option>
                                <option value="v4">Option 3</option>
                                <option value="v5">Option 4</option>
                                <option value="v6">Option 5</option>
                            </select>
                        </div>
                        <div class="row">
                            <label>Sub-Category</label>
                            <select>
                                <option value="v1">Select Sub-Category</option>
                                <option value="v2">Option 1</option>
                                <option value="v3">Option 2</option>
                                <option value="v4">Option 3</option>
                                <option value="v5">Option 4</option>
                                <option value="v6">Option 5</option>
                            </select>
                        </div>
                        <a href="#">Add another Category <span>+</span></a>
                        
                        <ul class="input_slct">
                            <li>
                                <label>Quantity</label>
                                <input type="text" name="" placeholder="1" />
                            </li>
                            <li>
                                <select>
                                    <option value="v1">Places</option>
                                    <option value="v2">Option 1</option>
                                    <option value="v3">Option 2</option>
                                    <option value="v4">Option 3</option>
                                    <option value="v5">Option 4</option>
                                    <option value="v6">Option 5</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="row1">
                        <label>Your Message</label>
                        <div class="inputDiv">
                            <p>Dear Sir/Madam,</p>
                            <p>I'm looking for products with the following specifications:</p>
                            <input type="text" name="" />
                            <div class="element">
                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                <span class="myFile"></span>
                              <input type="file" name="" id="vediocam">
                            </div>
                        </div>
                    </div>
                    <div class="accr">
                        <button type="button" class="popACC" data-toggle="collapse" data-target="#demo">Filter Recipients<i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <div id="demo" class="collapse">
                            <h3>Supplier Types</h3>
                            <ul class="forcheck">
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk1" />
                                        <label title="Unchecked state" for="chk1">Trade Protection</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk2" />
                                        <label title="Unchecked state" for="chk2">Gold Member</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk3" />
                                        <label title="Unchecked state" for="chk3">Assessed Supplier</label>
                                    </div>
                                </li>
                            </ul>
                            <h3>Export Country</h3>
                            <ul class="forcheck">
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk4" />
                                        <label title="Unchecked state" for="chk4">Bahrain</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk5" />
                                        <label title="Unchecked state" for="chk5">India</label>
                                    </div>
                                </li>
                                <li>
                                   <div class="quotCheck">
                                        <input type="checkbox" id="chk6" />
                                        <label title="Unchecked state" for="chk6">Kuwait</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk7" />
                                        <label title="Unchecked state" for="chk7">Oman</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk8" />
                                        <label title="Unchecked state" for="chk8">Qatar</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk33" />
                                        <label title="Unchecked state" for="chk33">UAE</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk9" />
                                        <label title="Unchecked state" for="chk9">Saudi Arabia</label>
                                    </div>
                                </li>
                            </ul>
                            <h3>Annual Sales Volume</h3>
                            <ul class="forcheck fortow">
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk10" />
                                        <label title="Unchecked state" for="chk10">Under US$1 Million</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk11" />
                                        <label title="Unchecked state" for="chk11">US$10 Million - US$50 Million</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk12" />
                                        <label title="Unchecked state" for="chk12">US$1 Million - US$2.5 Million</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk13" />
                                        <label title="Unchecked state" for="chk13">US$50 Million - US$100 Million</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk14" />
                                        <label title="Unchecked state" for="chk14">US$2.5 Million - US$5 Million</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk15" />
                                        <label title="Unchecked state" for="chk15">Over US$100 Million</label>
                                    </div> 
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk16" />
                                        <label title="Unchecked state" for="chk16">OUS$5 Million - US$10 Million</label>
                                    </div>
                                </li>
                            </ul>
                            <h3>Certifications</h3>
                            <ul class="forcheck">
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk17" />
                                        <label title="Unchecked state" for="chk17">ISO9001</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk18" />
                                        <label title="Unchecked state" for="chk18">CE</label>
                                    </div>
                                </li>
                            </ul>
                            <h3>Business Type</h3>
                            <ul class="forcheck">
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk19" />
                                        <label title="Unchecked state" for="chk19">Manufacturer</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk20" />
                                        <label title="Unchecked state" for="chk20">Trading Company</label>
                                    </div>
                                </li>
                            </ul>
                            <h3>Company Established</h3>
                            <ul class="forcheck">
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk21" />
                                        <label title="Unchecked state" for="chk21">1-5 years</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk22" />
                                        <label title="Unchecked state" for="chk22">6-10 years</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="quotCheck">
                                        <input type="checkbox" id="chk221" />
                                        <label title="Unchecked state" for="chk221">Over 10 years</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> 
                    <input type="submit" name="" value="Send Post Buying Request" class="popBtn" /> 
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var request_sent = 'Your quoutation request is sent.';
        $('body').on('click', '.req_quot_btn', function () {
            var here = $(this); // alert div for show alert message
            var form = here.closest('form');
            var can = '';
            var ing = here.data('ing');
            var msg = here.data('msg');
            var prv = here.html();
            var formdata = false;
            if (window.FormData) {
                formdata = new FormData(form[0]);
            }
            $.ajax({
                url: form.attr('action'), // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                data: formdata ? formdata : form.serialize(), // serialize form data 
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    here.html(ing); // change submit button text
                },
                success: function (data) {
                    here.fadeIn();
                    here.html(prv);
                    if (data == 'done') {
                        here.closest('.modal-content').find('#v_close_logup_modal').click();
                        notify(request_sent, 'success', 'bottom', 'right');
                    } else {
                        //here.closest('.modal-content').find('#v_close_logup_modal').click();
                        notify('Request Quotation Errors' + '<br>' + data, 'warning', 'bottom', 'right');
                        //vend_logup();
                    }
                },
                error: function (e) {
                    console.log(e)
                }
            });
        });
</script>
<script type="text/javascript">
    $(function() {
        jcf.replaceAll();
    });

    //for uploading vedio
    $(".fa-paperclip").click(function () {
      $("#vediocam").trigger('click');
    });
    
    $('#vediocam').on('change', function() {
      var val = $(this).val();
      $(this).siblings('.myFile').text(val);
    })
</script>