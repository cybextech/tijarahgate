<div class="container">
    <section class="countrybanner">
        <div class="banimg">
            <img src="<?php echo base_url() ?>/template/front/assets/images/img47.png" alt="">
        </div>
        <h2>The Gateway to Gulf Countries and India Trading</h2>
    </section>
    <section class="country">
        <h2>Find Your Preferred Suppliers By Country</h2>
        <ul>
            <li><a href="#">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img48.png">
                    <p>Bahrain</p>
                </a></li>	
            <li><a href="#">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img49.png">
                    <p>India</p>
                </a></li>
            <li><a href="#">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img50.png">
                    <p>Kuwait</p>
                </a></li>	
            <li><a href="#">
                    <img src="<?php echo base_url() ?>/template/front/assets/images/img51.png">
                    <p>Oman
                    <li><a href="#">
                            <img src="<?php echo base_url() ?>/template/front/assets/images/img52.png">
                            <p>Qatar</p>
                        </a></li>
                    <li><a href="#">
                            <img src="<?php echo base_url() ?>/template/front/assets/images/img53.png">
                            <p>Saudi <span>Arabia</span></p>
                        </a></li>	
                    <li><a href="#">
                            <img src="<?php echo base_url() ?>/template/front/assets/images/img54.png">
                            <p>UAE</p>
                        </a></li>		
        </ul>
    </section>
    <section class="counDetail">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="flag">
                    <div class="flagimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img55.png">
                    </div>
                    <strong>Kuwait</strong>
                </div>
            </div>
            <div class="col-md-10 col-sm-10">
                <h2>Industries</h2>
                <p>Textiles, Fashion Accessories, Consumer Electronics, Home Appliances, <a href="#"> View more<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a> </p>
                <h2>Popular keywords</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Southeast Asia</a></li>
                            <li><a href="#">China textile</a></li>
                            <li><a href="#">Thailand channel</a></li>
                            <li><a href="#">Pakistan </a></li>
                            <li><a href="#">GloveT-shirt</a></li>
                            <li><a href="#">Made from Vientam</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Australia & New Zealand Channel</a></li>
                            <li><a href="#">Indonesia furniture</a></li>
                            <li><a href="#">New Zealand honey</a></li>
                            <li><a href="#">Taiwan Cable</a></li>
                            <li><a href="#">Hong Kong bag</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Japan seafood</a></li>
                            <li><a href="#">Philippines banana</a></li>
                            <li><a href="#">South Korea skin care</a></li>
                            <li><a href="#">Malaysia white coffee</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="counDetail">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="flag">
                    <div class="flagimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img56.png">
                    </div>
                    <strong>Bahrain</strong>
                </div>
            </div>
            <div class="col-md-10 col-sm-10">
                <h2>Industries</h2>
                <p>Textiles, Fashion Accessories, Consumer Electronics, Home Appliances, <a href="#"> View more<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a> </p>
                <h2>Popular keywords</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Southeast Asia</a></li>
                            <li><a href="#">China textile</a></li>
                            <li><a href="#">Thailand channel</a></li>
                            <li><a href="#">Pakistan </a></li>
                            <li><a href="#">GloveT-shirt</a></li>
                            <li><a href="#">Made from Vientam</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Australia & New Zealand Channel</a></li>
                            <li><a href="#">Indonesia furniture</a></li>
                            <li><a href="#">New Zealand honey</a></li>
                            <li><a href="#">Taiwan Cable</a></li>
                            <li><a href="#">Hong Kong bag</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Japan seafood</a></li>
                            <li><a href="#">Philippines banana</a></li>
                            <li><a href="#">South Korea skin care</a></li>
                            <li><a href="#">Malaysia white coffee</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="counDetail">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="flag">
                    <div class="flagimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img57.png">
                    </div>
                    <strong>Saudi Arabia</strong>
                </div>
            </div>
            <div class="col-md-10 col-sm-10">
                <h2>Industries</h2>
                <p>Textiles, Fashion Accessories, Consumer Electronics, Home Appliances, <a href="#"> View more<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a> </p>
                <h2>Popular keywords</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Southeast Asia</a></li>
                            <li><a href="#">China textile</a></li>
                            <li><a href="#">Thailand channel</a></li>
                            <li><a href="#">Pakistan </a></li>
                            <li><a href="#">GloveT-shirt</a></li>
                            <li><a href="#">Made from Vientam</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Australia & New Zealand Channel</a></li>
                            <li><a href="#">Indonesia furniture</a></li>
                            <li><a href="#">New Zealand honey</a></li>
                            <li><a href="#">Taiwan Cable</a></li>
                            <li><a href="#">Hong Kong bag</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Japan seafood</a></li>
                            <li><a href="#">Philippines banana</a></li>
                            <li><a href="#">South Korea skin care</a></li>
                            <li><a href="#">Malaysia white coffee</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="counDetail">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="flag">
                    <div class="flagimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img58.png">
                    </div>
                    <strong>UAE</strong>
                </div>
            </div>
            <div class="col-md-10 col-sm-10">
                <h2>Industries</h2>
                <p>Textiles, Fashion Accessories, Consumer Electronics, Home Appliances, <a href="#"> View more<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a> </p>
                <h2>Popular keywords</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Southeast Asia</a></li>
                            <li><a href="#">China textile</a></li>
                            <li><a href="#">Thailand channel</a></li>
                            <li><a href="#">Pakistan </a></li>
                            <li><a href="#">GloveT-shirt</a></li>
                            <li><a href="#">Made from Vientam</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Australia & New Zealand Channel</a></li>
                            <li><a href="#">Indonesia furniture</a></li>
                            <li><a href="#">New Zealand honey</a></li>
                            <li><a href="#">Taiwan Cable</a></li>
                            <li><a href="#">Hong Kong bag</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Japan seafood</a></li>
                            <li><a href="#">Philippines banana</a></li>
                            <li><a href="#">South Korea skin care</a></li>
                            <li><a href="#">Malaysia white coffee</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="counDetail">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="flag">
                    <div class="flagimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img59.png">
                    </div>
                    <strong>Oman</strong>
                </div>
            </div>
            <div class="col-md-10 col-sm-10">
                <h2>Industries</h2>
                <p>Textiles, Fashion Accessories, Consumer Electronics, Home Appliances, <a href="#"> View more<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a> </p>
                <h2>Popular keywords</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Southeast Asia</a></li>
                            <li><a href="#">China textile</a></li>
                            <li><a href="#">Thailand channel</a></li>
                            <li><a href="#">Pakistan </a></li>
                            <li><a href="#">GloveT-shirt</a></li>
                            <li><a href="#">Made from Vientam</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Australia & New Zealand Channel</a></li>
                            <li><a href="#">Indonesia furniture</a></li>
                            <li><a href="#">New Zealand honey</a></li>
                            <li><a href="#">Taiwan Cable</a></li>
                            <li><a href="#">Hong Kong bag</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Japan seafood</a></li>
                            <li><a href="#">Philippines banana</a></li>
                            <li><a href="#">South Korea skin care</a></li>
                            <li><a href="#">Malaysia white coffee</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="counDetail">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <div class="flag">
                    <div class="flagimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img59.png">
                    </div>
                    <strong>India</strong>
                </div>
            </div>
            <div class="col-md-10 col-sm-10">
                <h2>Industries</h2>
                <p>Textiles, Fashion Accessories, Consumer Electronics, Home Appliances, <a href="#"> View more<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a> </p>
                <h2>Popular keywords</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Southeast Asia</a></li>
                            <li><a href="#">China textile</a></li>
                            <li><a href="#">Thailand channel</a></li>
                            <li><a href="#">Pakistan </a></li>
                            <li><a href="#">GloveT-shirt</a></li>
                            <li><a href="#">Made from Vientam</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Australia & New Zealand Channel</a></li>
                            <li><a href="#">Indonesia furniture</a></li>
                            <li><a href="#">New Zealand honey</a></li>
                            <li><a href="#">Taiwan Cable</a></li>
                            <li><a href="#">Hong Kong bag</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                            <li><a href="#">Japan seafood</a></li>
                            <li><a href="#">Philippines banana</a></li>
                            <li><a href="#">South Korea skin care</a></li>
                            <li><a href="#">Malaysia white coffee</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>