<div id="wrapper" class="verfication">
    <!--start of Html for membership-->
    <section class="finance">
        <div class="memdetailBan">
            <h2>Get Verified</2>
        </div>
    </section>
    <div class="container">
        <section class="effectivtool">
            <h2>Enjoy great benefits as a verified Tijara Gate member</h2>
            <p>With TijaraGate.com's Inspection Service, you get the supplier's confidence, fast refund thru our Trade  Protection order, You're prioritized in getting free product samples, and most especially, yoo get VIP Customer Service provided by our professional service staffs.</p>
            <a href="#" class="applyNow">Get Verified for FREE</a>
        </section>
        <section class="prviliged">
            <h2>Verification Process</h2>
            <div class="usefinance protection">
                <img src="<?php echo base_url() ?>/template/front/assets/images/TG-get-verified-3_03.jpg">   
            </div>
        </section>
    </div>
    <section class="faqs">
        <div class="container">
            <h2>FAQ</h2>
            <div class="row">
                <div class="col-md-6 colo-sm-6 col-xs-6">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#hlpcntr"> 
                                What do the three types of Business Identity mean?</a></li>
                        <li><a data-toggle="tab" href="#resolutioncntr">
                                Where can the supplier see my Business Identity?</a></li>
                        <li><a data-toggle="tab" href="#contacts">
                                Where can I see my Business Identity Icon?</a></li>
                        <li><a data-toggle="tab" href="#fourfaq">
                                Is there a country limit for Business Identity verification?</a></li>
                        <li><a data-toggle="tab" href="#fivefaq">
                                What is the difference between the 'Third Party Verification' and the 'Contacts Verification' methods?</a></li>
                    </ul>
                    <a href="#" class="readmore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-6 colo-sm-6 col-xs-6">
                    <div class="tab-content">
                        <div id="hlpcntr" class="tab-pane fade in active hlpcntr">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="resolutioncntr" class="tab-pane fade  resolutioncntr">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="contacts" class="tab-pane fade  contacts">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="fourfaq" class="tab-pane fade  fourfaq">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="fivefaq" class="tab-pane fade  fivefaq">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end of Html for membership-->
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>