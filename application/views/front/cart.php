<!--=== Content Medium Part ===-->
<div class="content-md margin-bottom-30 cart_page">
    <div class="container">
        <?php
        echo form_open(base_url() . 'index.php/home/cart_finish/go', array(
            'class' => 'shopping-cart',
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            'id' => 'cart_form'
        ));
        ?>    
        <div>
            <!--Cart step 1 products ****************************************************************************************************-->
            <div class="header-tags cart_page_stp">
                <span class="cart_round">
                    <h2 class="cart_step">Step</h2>
                    <h2 class="cart_step_number">1</h2>
                </span>
                <h2 class="cart_step_desc"><?php echo 'Start Order'; ?></h2>
            </div>
            <section>
                <div class="table-responsive cart_list">
                    <div class="supplier-info">
                        <div class="sipplier-img"><img src="<?php echo base_url() ?>template/front/assets/images/img38.png"></div>
                        <h2>Supplier Company Name Here</h2>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <td><?php echo 'Product/s'; ?></td>
                                <td><?php echo translate('choices'); ?></td>
                                <td><?php echo translate('price'); ?></td>
                                <td><?php echo translate('qty'); ?></td>
                                <td><?php echo translate('total'); ?></td>
                                <td style="text-align:right !important;"><?php echo translate('option'); ?></td>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($carted as $items) { ?>

                                <tr data-rowid="<?php echo $items['rowid']; ?>" >
                                    <td class="product-in-table">
                                        <img class="img-responsive" src="<?php echo $items['image']; ?>" alt="">
                                        <div class="product-it-in">
                                            <h3><?php echo $items['name']; ?></h3>
                                        </div>    
                                    </td>
                                    <td class="shop-product">
                                        <?php
                                        $color = $this->crud_model->is_added_to_cart($items['id'], 'option', 'color');
                                        if ($color) {
                                            ?>
                                            <div style="background:<?php echo $color; ?>; height:25px; width:25px;" ></div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        $all_o = json_decode($items['option'], true);
                                        foreach ($all_o as $l => $op) {
                                            if ($l !== 'color' && $op['value'] !== '' && $op['value'] !== NULL) {
                                                ?>
                                                <?php echo $op['title'] ?> : 
                                                <?php
                                                if (is_array($va = $op['value'])) {
                                                    echo $va = join(', ', $va);
                                                } else {
                                                    echo $va;
                                                }
                                                ?>
                                                <br>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <a href="<?php echo $this->crud_model->product_link($items['id']); ?>"><?php echo translate('change_choices'); ?></a>
                                    </td>
                                    <td class="pric"><?php echo currency() . $this->cart->format_number($items['price']); ?></td>
                                    <td>
                                        <?php
                                        if (!$this->crud_model->is_digital($items['id'])) {
                                            ?>
                                            <button type='button' class="quantity-button minus" value='minus'>-</button>
                                            <input type='text' disabled step='1' class="quantity-field quantity_field" data-rowid="<?php echo $items['rowid']; ?>" data-limit='no' value="<?php echo $items['qty']; ?>" id='qty1' onblur="check_ok(this);" />
                                            <button type='button' class="quantity-button plus" value='plus'>+</button>
                                            <span class="button limit" style="display:none;"></span>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="shop-red sub_total"><?php echo currency() . $this->cart->format_number($items['subtotal']); ?></td>
                                    <td class="text-center">
                                        <button type="button" class="close">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </section>

            <script>
                var ecredit = '<?php echo $this->crud_model->get_eCredit(); ?>';
                var tovalphp = '<?php echo $this->crud_model->get_type_name_by_id('business_settings', '8', 'value'); ?>';

                var add_to_cart = '<?php echo translate('add_to_cart'); ?>';
                var base_url = '<?php echo base_url(); ?>';
                set_cart_form();
            </script>
<!--            <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false"></script>-->
            <script src="<?php echo base_url(); ?>template/front/assets/js/custom/cart.js"></script>

            <!--Cart step 2 Shipping ****************************************************************************************************-->
            <div class="header-tags cart_page_stp">
                <span class="cart_round">
                    <h2 class="cart_step">Step</h2>
                    <h2 class="cart_step_number">2</h2>
                </span>
                <h2 class="cart_step_desc"><?php echo 'Shipping & Inspection'; ?></h2>
            </div>
            <section class="billing-info">
                <div class="row">
                    <div class="col-md-12 md-margin-bottom-40">
                        <h2 class="title-type shipping_address_title"><?php echo 'Shipping'; ?></h2>
                        <div class="billing-info-inputs checkbox-list">
                            <!--shipping address-->
                            <div class="row shipping_address_main">
                                <div class="col-sm-12">
                                    <h2 class="heading-shipping-address">Shipping Address</h2>
                                </div>
                                <div id="shippingaddress" class="col-sm-12 shipping_address_list">
									<?php
									$current_user_id = $this->session->userdata('user_id');
									$useraddress = $this->db->get_where('user_address', array('user_id' => $current_user_id))->result_array();
									if (count($useraddress) > 0) 
									{
										foreach($useraddress as $key => $singadd)
										{
											?>
											<ul class="shipping_address">
												<li><input type="radio" name="shippingAdd" value="<?php echo $singadd['id']; ?>"></li>
												<li><p class="name">Address <?php echo $key+1; ?></p></li>
												<li>
													<p class="description">
														<?php 
															$destcon = $this->db->where('code',$singadd['country_code'])->get('shipping_country')->result_array();
															$destcit = $this->db->where('id',$singadd['city_id'])->get('shipping_city')->result_array();
															echo $destcon[0]['name']." , ".$destcit[0]['name'];
														?>
													</p>
												</li>
											</ul>
											<?php
										}
									} 
									else 
									{
										echo "Please add address for shipping details!";
									}
									?>
                                </div>
                                <div class="shipping_divider"></div>
                                <div class="edit-add-shping-address">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#addaddressmodel"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add New Address</a>
                                    <a href="javascript:void(0);" id="editAddress"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Selected Address</a>
                                </div>
                            </div>
							<?php 
								$inspectors = array();
								foreach ($carted as $items) 
								{
									$data = $this->crud_model->get_prod_data($items['id']);
									$info = json_decode($data['added_by']);
									if($info->type == "admin")
									{
										$inspector = $this->db->where('country_code' , 'ABW')->get('inspector')->result_array();
										foreach($inspector as $sininspec)
										{
											$inspectors[] = $sininspec;
										}
									}
									else
									{
										$vendid = $this->crud_model->get_vendor($info->id);
										//print_r($vendid['address1']);
										$inspector = $this->db->where('country_code' , $vendid['address1'])->get('inspector')->result_array();
										foreach($inspector as $sininspec)
										{
											$inspectors[] = $sininspec;
										}
									}
								}
								if(count($inspectors) > 0)
								{
									?>
									<div class="row shipping_methods">
										<!--shipping methods start here-->
										<section class="sh_cal" id="allshippingsmain" style="display:none;">
										</section>
										<!--inspection options-->
										<section class="sh_cal">
											<div class="shtable inspection">
												<div class="col-sm-12 inspection-title">
													<h2 class="heading-shipping-address">Inspection <span>(optional)</span></h2>
												</div>
												<ul class="tablehead">
													<li>
														<span>Inspector Info</span>
														<span>Inspector Price</span>
														<span>Inspection Type</span>
														<span>Completed Transactions</span>
														<span>Serviced Locations</span>
														<span>Inspection Report Languages</span>
													</li>
												</ul>
												<ul class="tablehead second">
													<?php 
														foreach($inspectors as $singinps)
														{
															?>
															<li>
																<span class="">
																	<input class="inspect-methods1" id="shipping-1" type="radio" name="inspection" price="<?php echo $singinps['inspector_rate']; ?>" value="<?php echo $singinps['inspector_id']; ?>" />
																	<div class="inpector_detail">
																		<div class="imaag">
																			<img src="<?php echo base_url() ?>template/front/assets/images/img41.png">
																		</div>
																		<div class="descc">
																			<p><?php echo $singinps['inspector_name']; ?></p>
																			<!--
																			<a href="">Profile Page >></a>
																			-->
																		</div>
																	</div>
																</span>
																<span><p><span style="color:ccc;">USD</span> <?php echo $singinps['inspector_rate']; ?></p></span>
																<span><p><?php echo $singinps['type']; ?></p></span>
																<span><p>10</p> </span>
																<span><p><?php echo $singinps['country_code']; ?></p> </span>
																<span><p><?php echo $singinps['language']; ?></p> </span>
															</li>
														<?php
														}
													?>
												</ul>
											</div>

										</section>
									</div>
								<?php
								}
							?>
                            <script type="text/javascript">
                //console.log('hello');
                $(document).ready(function () {
                    $('.actions a').click(function () {
                        console.log('hello');
                        //console.log($('.billing-info').attr('aria-hidden'));
                    });
                });
                // on shipment selection
                $(document).on("change",".shipping-methods", function () 
				{
                    var shipping_id = $(this).val();
                    var shipping_fee = $('#shipping-rate-' + shipping_id).html();
                    var url = base_url + 'index.php/home/cart/calcs/custom_shipping/add/' + shipping_fee;
                    var total = $('#total');
                    var ship = $('#shipping');
                    var tax = $('#tax');
                    var grand = $('#grand');

                    $.ajax({
                        url: url,
                        beforeSend: function () {
                            total.html('...');
                            ship.html('...');
                            tax.html('...');
                            grand.html('...');
                        },
                        success: function (data) {
                            var res = data.split('-');
                            total.html(res[0]).fadeIn();
                            ship.html(res[1]).fadeIn();
                            tax.html(res[2]).fadeIn();
                            grand.html(res[3]).fadeIn();
                            other_action();
                        },
                        error: function (e) {
                            console.log(e)
                        }
                    });

                    //update_calc_cart();
                });
				
				$(document).on("change",".inspect-methods1", function () 
				{
                    var shipping_id 	= 	$(this).val();
                    var shipping_fee 	= 	$(this).attr('price');
					console.log(shipping_fee);
                    var url = base_url + 'index.php/home/cart/calcs/full/inspectionadd/' + shipping_fee;
                    var total = $('#total');
                    var ship = $('#shipping');
                    var tax = $('#tax');
                    var grand = $('#grand');
                    var inspection = $('#inspection');

                    $.ajax({
                        url: url,
                        beforeSend: function () {
                            total.html('...');
                            ship.html('...');
                            tax.html('...');
                            grand.html('...');
                            inspection.html('...');
                        },
                        success: function (data) {
                            var res = data.split('-');
                            total.html(res[0]).fadeIn();
                            ship.html(res[1]).fadeIn();
                            tax.html(res[2]).fadeIn();
                            grand.html(res[3]).fadeIn();
                            inspection.html(res[5]).fadeIn();
                            other_action();
                        },
                        error: function (e) {
                            console.log(e)
                        }
                    });
                    //update_calc_cart();
                });

                            </script>
                        </div>
                    </div>
                </div> 
            </section>

            <!--Cart step 3 payment ****************************************************************************************************-->
            <div class="header-tags cart_page_stp">
                <span class="cart_round">
                    <h2 class="cart_step">Step</h2>
                    <h2 class="cart_step_number">3</h2>
                </span>
                <h2 class="cart_step_desc"><?php echo 'Payment'; ?></h2> 
            </div>
            <section class="payment_methodss">
                <div class="row">
                    <div class="col-md-12 md-margin-bottom-50">
                        <h2 class="title-type"><?php echo translate('choose_a_payment_method'); ?></h2>
                        <div class="cc-selector">
                            <?php
                            $p_set = $this->db->get_where('business_settings', array('type' => 'paypal_set'))->row()->value;
                            $c_set = $this->db->get_where('business_settings', array('type' => 'cash_set'))->row()->value;
                            $s_set = $this->db->get_where('business_settings', array('type' => 'stripe_set'))->row()->value;
                            $ecredit = $this->crud_model->get_eCredit();
                            ?>
                            <input id="visa" class="ecreditCl" type="radio" name="payment_type" value="ecredit" />
                            <label class="drinkcard-cc ecredit ecreditCl" for="visa"></label>
                            <?php
                            if ($p_set == 'ok') {
                                ?>
                                <input id="visa" type="radio" name="payment_type" value="paypal" />
                                <label class="drinkcard-cc visa" for="visa"></label>
                                <?php
                            }
                            if ($s_set == 'ok') {
                                ?>
                                <input id="mastercardd" type="radio" name="payment_type" value="stripe" />
                                <label class="drinkcard-cc stripe" id="customButton" for="mastercardd"></label>
                                <script src="https://checkout.stripe.com/checkout.js"></script>
                                <script>
                var handler = StripeCheckout.configure({
                    key: '<?php echo $this->db->get_where('business_settings', array('type' => 'stripe_publishable'))->row()->value; ?>',
                    image: '<?php echo base_url(); ?>template/front/assets/img/stripe.png',
                    token: function (token) {
                        // Use the token to create the charge with a server-side script.
                        // You can access the token ID with `token.id`

                        $('#cart_form').append("<input type='hidden' name='stripeToken' value='" + token.id + "' />");
                        if ($("#visa").length) {
                            $("#visa").prop("checked", false);
                        }
                        if ($("#mastercard").length) {
                            $("#mastercard").prop("checked", false);
                        }
                        $("#mastercardd").prop("checked", true);
                        notify('<?php echo translate('your_card_details_verified!'); ?>', 'success', 'bottom', 'right');
                        setTimeout(function () {
                            $('#cart_form').submit();
                        }, 500);
                    }
                });

                $('#customButton').on('click', function (e) {
                    // Open Checkout with further options
                    var total = $('#grand').html();
                    total = total.replace("<?php echo currency(); ?>", '');
                    total = parseFloat(total.replace(",", ''));
                    total = total / parseFloat(<?php echo $this->crud_model->get_type_name_by_id('business_settings', '8', 'value'); ?>);
                    total = total * 100;
                    handler.open({
                        name: '<?php echo $system_title; ?>',
                        description: '<?php echo translate('pay_with_stripe'); ?>',
                        amount: total
                    });
                    e.preventDefault();
                });

                // Close Checkout on page navigation
                $(window).on('popstate', function () {
                    handler.close();
                });
                                </script>
                                <?php
                            } if ($c_set == 'ok') {
                                ?>
                                <input id="mastercard" type="radio" name="payment_type" value="cash_on_delivery" checked />
                                <label class="drinkcard-cc mastercard"for="mastercard"></label>
                                <?php
                            }
                            ?>
                        </div>
                    </div>


                </div>
            </section>
            <div class="header-tags cart_page_stp">
                <span class="cart_round">
                    <h2 class="cart_step">Step</h2>
                    <h2 class="cart_step_number">4</h2>
                </span>
                <h2 class="cart_step_desc"><?php echo 'Completed'; ?></h2> 
            </div>
            <section></section>

            <div class="coupon-code">
                <div class="row">
                    <div class="col-sm-4 sm-margin-bottom-30">
                        <h3>Quotation Code</h3>
                        <p>Enter your quotation code</p>
                        <span id="coupon_report">
                            <?php if ($this->cart->total_discount() <= 0 && $this->session->userdata('couponer') !== 'done' && $this->cart->get_coupon() == 0) { ?>
                                <input class="form-control margin-bottom-10 coupon_code" type="text">
                                <button type="button" class="btn-u btn-u-sea-shop coupon_btn"><?php echo translate('apply_coupon'); ?></button>
                            <?php } else { ?>
                                <h3><?php echo translate('coupon_already_activated'); ?></h3>  
                            <?php } ?>
                        </span>
                    </div>
                    <script type="text/javascript">
                        $('.coupon_btn').on('click', function () {
                            var txt = $(this).html();
                            var code = $('.coupon_code').val();
                            $('#coup_frm').val(code);
                            var form = $('#coupon_set');
                            var formdata = false;
                            if (window.FormData) {
                                formdata = new FormData(form[0]);
                            }
                            var datas = formdata ? formdata : form.serialize();
                            $.ajax({
                                url: '<?php echo base_url(); ?>index.php/home/coupon_check/',
                                type: 'POST', // form submit method get/post
                                dataType: 'html', // request type html/json/xml
                                data: datas, // serialize form data 
                                cache: false,
                                contentType: false,
                                processData: false,
                                beforeSend: function () {
                                    $(this).html("<?php echo translate('applying..'); ?>");
                                },
                                success: function (result) {
                                    if (result == 'nope') {
                                        notify("<?php echo translate('coupon_not_valid'); ?>", 'warning', 'bottom', 'right');
                                    } else {
                                        var re = result.split(':-:-:');
                                        var ty = re[0];
                                        var ts = re[1];
                                        $("#coupon_report").fadeOut();
                                        notify("<?php echo translate('coupon_discount_successful'); ?>", 'success', 'bottom', 'right');
                                        if (ty == 'total') {
                                            $(".coupon_disp").show();
                                            $("#disco").html(re[2]);
                                        }
                                        $("#coupon_report").html('<h3>' + ts + '</h3>');
                                        $("#coupon_report").fadeIn();
                                        update_calc_cart();
                                        update_prices();
                                    }
                                }
                            });
                        });
                    </script>
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="table-responsive cart_list">
                            <table class="table" style="border: 1px solid #ddd;">
                                <tr>
                                    <td><?php echo translate('subtotal'); ?></td>
                                    <td><span class="text-right" id="total"></span></td>
                                </tr>

                                <tr>
                                    <td><?php echo translate('tax'); ?></td>
                                    <td><span class="text-right" id="tax"></span></td>
                                </tr>
                                <tr>
                                    <td><?php echo translate('shipping'); ?></td>
                                    <td><span class="text-right" id="shipping"></span></td>
                                </tr>
								<tr>
                                    <td><?php echo translate('inspection'); ?></td>
                                    <td><span class="text-right" id="inspection"></span></td>
                                </tr>
                                <tr <?php if ($this->cart->total_discount() <= 0) { ?>style="display:none;"<?php } ?> class="coupon_disp">
                                    <td><?php echo translate('coupon_discount'); ?></td>
                                    <td><span class="text-right" id="disco"><?php echo currency() . $this->cart->total_discount(); ?></span></td>
                                </tr>
                                <tr>
                                    <td><?php echo translate('total'); ?></td>
                                    <td class="total-result-in"><span class="grand_total" id="grand"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        </form>
    </div><!--/end container-->
</div>
<input type="hidden" id="first" value="yes" />
<!--=== End Content Medium Part ===-->  
<script>

    jQuery(document).ready(function () {
        App.init();
        StepWizard.initStepWizard();
    });

    $(document).ready(function () {
        update_calc_cart();
    });
    $(document).ready(function () {
    });

    function update_calc_cart() {
        var url = base_url + 'index.php/home/cart/calcs/full';
        var total = $('#total');
        var ship = $('#shipping');
        var tax = $('#tax');
        var grand = $('#grand');
        var inspection = $('#inspection');

        $.ajax({
            url: url,
            beforeSend: function () {
                total.html('...');
                ship.html('...');
                tax.html('...');
                grand.html('...');
                inspection.html('...');
            },
            success: function (data) {
                var res = data.split('-');
                total.html(res[0]).fadeIn();
                ship.html(res[1]).fadeIn();
                tax.html(res[2]).fadeIn();
                grand.html(res[3]).fadeIn();
                inspection.html(res[5]).fadeIn();
                other_action();
            },
            error: function (e) {
                console.log(e)
            }
        });
    }

</script>

<?php
echo form_open('', array(
    'method' => 'post',
    'id' => 'coupon_set'
));
?>
<input type="hidden" id="coup_frm" name="code">
</form>
<script type="text/javascript">
	$(document).on("change", "#changeCountryup", function(){
		if(jQuery(this).val() != "")
		{
			jQuery('#citieslabelupdate').html("");
			jQuery.ajax({url: "<?php echo base_url(); ?>index.php/admin/getCitiesByCountry/"+jQuery(this).val()+"/location2", success: function(result){
				jQuery('#citieslabelupdate').html(result);
			}});
		}
	});
	$(document).on("click", "input[name=shippingAdd]", function(){
		if(jQuery(this).val() != "")
		{
			jQuery('#allshippingsmain').hide();
			jQuery.ajax({ 
				url: "<?php echo base_url(); ?>index.php/home/get_shipping_matches", 
				type: "POST",
				data: {address_id: jQuery(this).val()},
				success: function(result){
					result = JSON.parse(result);
					if(result.status == 1)
					{
						jQuery('#allshippingsmain').show();
						jQuery('#allshippingsmain').html(result.html);
					}
					else
					{
						alert(result.msg);
					}
				}
			});
		}
	});
	jQuery(document).ready(function(){
		jQuery('#changeCountry').change(function(){
			if(jQuery(this).val() != "")
			{
				jQuery('#citieslabel').html("");
				jQuery.ajax({url: "<?php echo base_url(); ?>index.php/admin/getCitiesByCountry/"+jQuery(this).val()+"/location1", success: function(result){
					jQuery('#citieslabel').html(result);
				}});
			}
		});
		
		jQuery('#addaddressform').submit(function(){
			if(jQuery('#changeCountry').val() == "" || jQuery('#location1_city_id').val() == "")
			{
				alert("Please select country and city!");
			}
			else
			{
				jQuery('#error_addaddress').text("");
				jQuery.ajax({
					url: "<?php echo base_url(); ?>index.php/home/add_shipping_address", 
					type: "POST",
					data: {country: jQuery('#changeCountry').val() , city : jQuery('#location1_city_id').val()},
					success: function(result)
					{
						result = JSON.parse(result);
						if(result.status == 1)
						{
							jQuery('#addaddressmodel').modal('hide');
							jQuery('#shippingaddress').html(result.html);
						}
						else
						{
							jQuery('#error_addaddress').text(result.msg);
						}
						//jQuery('#citieslabel').html(result);
					}
				});
			}
			return false;
		});
		jQuery('#editaddressform').submit(function(){
			if(jQuery('#changeCountryup').val() == "" || jQuery('#location2_city_id').val() == "" || typeof jQuery('#location2_city_id').val() == "undefined" )
			{
				alert("Please select country and city!");
			}
			else
			{
				jQuery('#error_addaddress').text("");
				jQuery.ajax({
					url: "<?php echo base_url(); ?>index.php/home/update_shipping_address", 
					type: "POST",
					data: {shippingID : jQuery('#shippingID').val() , country: jQuery('#changeCountryup').val() , city : jQuery('#location2_city_id').val()},
					success: function(result)
					{
						result = JSON.parse(result);
						if(result.status == 1)
						{
							jQuery('#editaddressmodel').modal('hide');
							jQuery('#shippingaddress').html(result.html);
						}
						else
						{
							jQuery('#error_editaddress').text(result.msg);
						}
						//jQuery('#citieslabel').html(result);
					}
				});
			}
			return false;
		});
		
		jQuery('.send_addaddresform').click(function(){
			jQuery('#addaddressform').submit();
		});
		jQuery('.send_editaddresform').click(function(){
			jQuery('#editaddressform').submit();
		});
		jQuery('#editAddress').click(function(){
			var selectval = jQuery("input[name=shippingAdd]:checked").val();
			if(typeof selectval === 'undefined')
			{
				alert("Please select address!");
			}
			else
			{
				jQuery('#preloader').show();
				jQuery.ajax({
					url: "<?php echo base_url(); ?>index.php/home/get_shipping_address", 
					type: "POST",
					data: {shipping_id: selectval},
					success: function(result)
					{
						jQuery('#preloader').hide();
						jQuery('#editaddressmodel').modal('show');
						jQuery('#updateHtml').html(result);
					}
				});
			}
		});
	});
</script>
<!--Model-->
<div class="modal fade in" id="addaddressmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog" style="margin-top:100px !important;">
		<div class="modal-content">
			<div class="modal-header">
				<center>
					<a href="<?php echo base_url(); ?>">
						<img id="logo-footer" class="footer-logo img-sm" width="100%" src="<?php echo base_url(); ?>uploads/logo_image/logo_3.png" alt="">
					</a>
				</center>
				<button aria-hidden="true" style="color:#fff;" data-dismiss="modal" id="close_log_modal" class="close" type="button">×</button>
			</div>
			<div class="modal-body">
				<div class="login_html">
					<form id="addaddressform" action="<?php echo base_url(); ?>/index.php/home/send_eCredit_request" class="log-reg-v3 sky-form" method="post" style="padding:30px 10px !important;" id="" accept-charset="utf-8">
						<div class="reg-block-header">
						<h2>Add Address</h2>
						</div>
						<div id="error_addaddress" style="color: red;"></div>
						<section>
							<?php 
								$countries = $this->db->get("shipping_country")->result();
							?>
							<label class="input login-input">
								<select id="changeCountry" name="country" class="form-control">
									<option value="">select country</option>
									<?php 
										foreach($countries as $countr)
										{
											?>
											<option value="<?php echo $countr->code; ?>"><?php echo $countr->name; ?>(<?php echo $countr->code; ?>)</option>
											<?php
										}
									?>
								</select>
							</label>
						</section>
						<section id="citieslabel">
							
						</section>
						<div class="row margin-bottom-5">
							<div class="col-xs-4 text-right">
								<span class="btn-u btn-u-cust btn-block margin-bottom-20 btn-labeled fa fa-tag send_addaddresform" type="submit">
									Submit                            
								</span>
							</div>
							<div class="col-xs-4 text-right">
								<span  data-dismiss="modal" class="btn-u btn-u-cust btn-block margin-bottom-20 btn-labeled fa fa-tag">
									Close                           
								</span>
							</div>
						</div>	
					</form> 
				</div>
			</div>
		</div>
	</div>
</div>
<!--Model-->
<div class="modal fade in" id="editaddressmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog" style="margin-top:100px !important;">
		<div class="modal-content">
			<div class="modal-header">
				<center>
					<a href="<?php echo base_url(); ?>">
						<img id="logo-footer" class="footer-logo img-sm" width="100%" src="<?php echo base_url(); ?>uploads/logo_image/logo_3.png" alt="">
					</a>
				</center>
				<button aria-hidden="true" style="color:#fff;" data-dismiss="modal" id="close_log_modal" class="close" type="button">×</button>
			</div>
			<div class="modal-body">
				<div class="login_html">
					<form id="editaddressform" action="#" class="log-reg-v3 sky-form" method="post" style="padding:30px 10px !important;" id="" accept-charset="utf-8">
						<div class="reg-block-header">
						<h2>Edit Address</h2>
						</div>
						<div id="error_editaddress" style="color: red;"></div>
						<div id="updateHtml">
						<section>
							<?php 
								$countries = $this->db->get("shipping_country")->result();
							?>
							<label class="input login-input">
								<select id="changeCountry" name="country" class="form-control">
									<option value="">select country</option>
									<?php 
										foreach($countries as $countr)
										{
											?>
											<option value="<?php echo $countr->code; ?>"><?php echo $countr->name; ?>(<?php echo $countr->code; ?>)</option>
											<?php
										}
									?>
								</select>
							</label>
						</section>
						<section id="citieslabel">
							
						</section>
						</div>
						<div class="row margin-bottom-5">
							<div class="col-xs-4 text-right">
								<span class="btn-u btn-u-cust btn-block margin-bottom-20 btn-labeled fa fa-tag send_editaddresform" type="submit">
									Submit                            
								</span>
							</div>
							<div class="col-xs-4 text-right">
								<span  data-dismiss="modal" class="btn-u btn-u-cust btn-block margin-bottom-20 btn-labeled fa fa-tag">
									Close                           
								</span>
							</div>
						</div>	
					</form> 
				</div>
			</div>
		</div>
	</div>
</div>