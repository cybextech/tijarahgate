<div id="wrapper" class="mainSupp">
    <div class="container ">
        <section class="sup_nav">
            <ul>
                <li class="active"><a href="<?php echo base_url() ?>/index.php/home/supplier_home">Home</a></li>
                <li>
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Product Categories
                        <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url() ?>/home/supplier_category">All Categories</a></li>
                          <li><a href="#">Category One</a></li>
                          <li><a href="#">Category Two</a></li>
                        </ul>
                    </div>
                </li>
                <li class="">
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Company Profile
                        <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                           <li><a href="<?php echo site_url() ?>/home/supplier_profile">Home</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradingSection">Trading</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#productionSection">Production</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#researchDevelopment">Research & Development</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#buyerInteractions">Buyer Interactions</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#transactionHistory">Transaction History</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradeShows">Trade Shows</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="<?php echo base_url() ?>/index.php/home/supplier_contact">Contact</a></li>
            </ul>
        </section>
        <section class="imgBanner">
            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img03.png"></a>
        </section>
        <section class="supplier">
            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img01.png">
        </section>
        <h1>Company Info</h1>
        <section class="co_info">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="co_detail">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inci didunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                        <p>Laboris nisi ut aliquip ex ea commodo consequat. Duis aute iru re dolor in reprehend erit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occae cat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est labo rum cillum dolore eu fugiat nu.</p>
                        <a href="#">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="co_ul">
                        <ul>
                            <li>
                                <label>Business Type:</label>
                                <span>Trading Company</span>
                            </li>
                            <li>
                                <label>Location:</label>
                                <span>Zhejiang, China (Mainland)</span>
                            </li>
                            <li>
                                <label>Year Established:</label>
                                <span>2012</span>
                            </li>
                            <li>
                                <label>Recent Transactions:</label>
                                <span>2</span>
                            </li>
                            <li>
                                <label>Total Annual Revenue:</label>
                                <span>US$5 Million - US$10 Million</span>
                            </li>
                            <li>
                                <label>Main Products:</label>
                                <span>gift wrapping paper,tissue paper,crepe paper,</span>
                            </li>
                            <li class="liBtn">
                                <a href="<?php echo site_url() ?>/home/contact_supplier" class="suplierbtn">Contact Supplier</a>
                                <a href="#" class="strtbtn">Start Order</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>	
        <section class="cat_name sup_home">
            <h2>Customer Service</h2>
            <div class="row">
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/h_img01.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/h_img01.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/h_img01.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/h_img01.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/h_img01.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="cat_name sup_home mail">
            <h2>Customise Content</h2>
            <div class="myimgdiv">
                <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/h_img02.png"></a>
            </div>
            <p>Product Name / Headline / Description Here</p>
        </section>
        <section class="cat_name sup_home">
            <h2>Customer Service</h2>
            <div class="row">
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="myCol">
                        <div class="imgCat">
                            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png"></a>
                        </div>
                        <p>Contact Person Name</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="cat_name sup_home mail">
            <h2 class="noMar">Main Categories</h2>
        </section>
        <section class="cat_name">
            <h2>Category 1 Name Here</h2>
            <span class="forborder"></span>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="cat_name">
            <h2>Category 2 Name Here</h2>
            <span class="forborder"></span>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>

            </div>	
        </section>
    </div>
</div>
<!--end of Html for supplier-->
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>