<link href="<?php echo base_url(); ?>template/front/assets/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>template/front/assets/css/bars.css" rel="stylesheet" type="text/css">
<div id="wrapper" class="mainSupp">
    <div class="container ">
        <section class="sup_nav">
            <ul>
                <li><a href="<?php echo base_url() ?>/index.php/home/supplier_home">Home</a></li>
                <li>
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Product Categories
                            <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url() ?>/home/supplier_category">All Categories</a></li>
                            <li><a href="#">Category One</a></li>
                            <li><a href="#">Category Two</a></li>
                        </ul>
                    </div>
                </li>
                <li  class="active">
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Company Profile
                            <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile">Home</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradingSection">Trading</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#productionSection">Production</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#researchDevelopment">Research & Development</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#buyerInteractions">Buyer Interactions</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#transactionHistory">Transaction History</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradeShows">Trade Shows</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="<?php echo base_url() ?>/index.php/home/supplier_contact">Contact</a></li>
            </ul>
        </section>
        <section class="imgBanner">
            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img03.png"></a>
        </section>
        <section class="supplier">
            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img01.png">
        </section>
        <h1 class="blueheading">Business Capabilities</h1>
        <h2 class="headingh2"><a name="tradingSection"></a>Trading</h2>
        <section class="tradingCoun">
            <h3>Trading Countries</h3>
            <div class="counHoldr">
                <div class="imghldr">
                    <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/img48.png"></a>
                    <span>Bahrain</span>
                </div>
                <div class="counTxt">
                    <strong>100.00%</strong>
                    <p>Total Revenue</p>
                </div>
            </div>	
            <div class="counHoldr">
                <div class="imghldr">
                    <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/img49.png"></a>
                    <span>India</span>
                </div>
                <div class="counTxt">
                    <strong>50.00%</strong>
                    <p>Total Revenue</p>
                </div>
            </div>	
            <div class="counHoldr">
                <div class="imghldr">
                    <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/img50.png"></a>
                    <span>Kuwait</span>
                </div>
                <div class="counTxt">
                    <strong>38.00%</strong>
                    <p>Total Revenue</p>
                </div>
            </div>	
            <div class="counHoldr">
                <div class="imghldr">
                    <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/img51.png"></a>
                    <span>Oman</span>
                </div>
                <div class="counTxt">
                    <strong>10.00%</strong>
                    <p>Total Revenue</p>
                </div>
            </div>
            <div class="counHoldr">
                <div class="imghldr">
                    <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/img52.png"></a>
                    <span>Qatar</span>
                </div>
                <div class="counTxt">
                    <strong>65.00%</strong>
                    <p>Total Revenue</p>
                </div>
            </div>
            <div class="counHoldr">
                <div class="imghldr">
                    <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/img53.png"></a>
                    <span>Saudi Arabia</span>
                </div>
                <div class="counTxt">
                    <strong>17.00%</strong>
                    <p>Total Revenue</p>
                </div>
            </div>
            <div class="counHoldr uae">
                <div class="imghldr">
                    <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/img54.png"></a>
                    <span>UAE</span>
                </div>
                <div class="counTxt">
                    <strong>41.00%</strong>
                    <p>Total Revenue</p>
                </div>
            </div>
        </section>
        <section class="millions">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="ussa">
                        <span>Total Annual Revenue</span>
                        <strong>US$5 Million - US$10 Million</strong>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="export">
                        <span>Export Percentage</span>
                        <strong>91% - 100%</strong>
                    </div>
                </div>
            </div>
        </section>
        <section class="proUl">
            <h2>Business Terms</h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <label>Accepted Delivery Terms:</label>
                        <span>FOB, CIF, EXW</span>
                    </li>
                    <li>
                        <label>Accepted Payment Currency:</label>
                        <span>USD, EUR, HKD, CNY</span>
                    </li>
                    <li>
                        <label>Accepted Payment Type:</label>
                        <span>T/T, L/C, PayPal, Cash</span>
                    </li>
                    <li>
                        <label>Nearest Port:</label>
                        <span>Shenzhen, Guangzhou</span>
                    </li>
                </ul>
            </div>
        </section>
        <section class="proUl">
            <h2>Trade Ability</h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <label>No. of Employees:</label>
                        <span>6-10 People</span>
                    </li>
                    <li>
                        <label>Average Lead Time:</label>
                        <span>28 Day(s)</span>
                    </li>
                    <li>
                        <label>Export Mode:</label>
                        <span>Using an agent </span>
                    </li>

                </ul>
            </div>
        </section>
        <h2 class="headingh2"><a name="productionSection"></a>Production</h2>	
        <section class="proUl">
            <h2>Factory Information</h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <label>Factory Size:</label>
                        <span>1,000-3,000 square meters</span>
                    </li>
                    <li>
                        <label>Factory Location:</label>
                        <span>5F, No.555, Liansheng Road North, Humen Town, Dongguan, Guangdong, China</span>
                    </li>
                    <li>
                        <label>No. of Production Lines:</label>
                        <span>5</span>
                    </li>
                    <li>
                        <label>Contract Manufacturing:	</label>
                        <span>OEM Service Offered   Design Service Offered  </span>
                    </li>
                    <li>
                        <label>Annual Output Value:	</label>
                        <span>US$5 Million - US$10 Million</span>
                    </li>
                </ul>
            </div>
        </section>
        <section class="proUl">
            <h2>Production Capacity</h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <label>Product Name:</label>
                        <span>Dresses</span>
                    </li>
                    <li>
                        <label>Units Produced (Previous Year):</label>
                        <span>600000</span>
                    </li>
                    <li>
                        <label>Highest Ever Annual Output:</label>
                        <span></span>
                    </li>
                    <li>
                        <label>Unit Type:</label>
                        <span>Piece/Pieces </span>
                    </li>
                </ul>
            </div>
        </section>
        <a name="researchDevelopment"></a>
        <h2 class="headingh2">Research & Development</h2>
        <section class="proUl">
            <h2>Research & Development</h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <span>There is/are 5 - 10 People R&D Engineer(s) in the company.</span>
                    </li>

                </ul>
            </div>
        </section>
        <a name='buyerInteractions'></a>
        <h1 class="blueheading">Business Performance</h1>
        <h2 class="headingh2">Buyer Interactions</h2>
        <section class="proUl">
            <h2>Membership History</h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <label>Year Joined:</label>
                        <span>2016</span>
                    </li>
                    <li>
                        <label>Membership Year:	 </label>
                        <span>Gold Member</span>
                    </li>
                    <li>
                        <label>Online Postings:</label>
                        <span>Company Profile, Products, Wholesaler</span>
                    </li>
                </ul>
            </div>
        </section>
        <section class="proUl">
            <h2>Response Rate <span class="hItalic">(for the last 30 days)</span></h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <span>94.3% of buyers who contacted this supplier received a response within 72 hours.</span>
                    </li>

                </ul>
            </div>
        </section>
        <section class="proUl">
            <h2>Average Response Time <span class="hItalic">(7 days)</span></h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <span>The supplier's average response time of received inquiries is within 53 hours.</span>
                    </li>

                </ul>
            </div>
        </section>
        <section class="proUl">
            <h2>Quotation Performance <span class="hItalic">(for the last 30 days) </span></h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <span>The supplier have sent 12 quotes to buyers</span>
                    </li>
                </ul>
            </div>
        </section>
        <a name='transactionHistory'></a>
        <h2 class="headingh2">Transaction History</h2>
        <section class="proUl">
            <div class="co_ul withBtns">
                <ul>
                    <li>
                        <span>Below is the information about the supplier's transactions conducted via TijaraGate.com. </span><br>
                        <span>If you require further details regarding the transaction data, please contact the supplier directly.</span>
                        <div class="liBtn">
                            <a href="#" class="suplierbtn">Contact Supplier</a>
                            <a href="#" class="strtbtn">Start Order</a>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="proUl noBoreder">
            <h2>Transaction Overview</h2>
        </section>
        <section class="myGraph">
            <div class="sliderGraph">
                <ul class="bxslider">
                    <li>
                        <strong>June 2015  -  May 2016</strong>
                        <div class="graphleft">
                            <h3>Transactions</h3>
                            <div class='wrap_right'>
                                <div class='bar_group'>
                                    <div class='bar_group__bar thin' label='4' show_values='true' tooltip='true' value='235'></div>
                                    <div class='bar_group__bar thin' label='3.5' show_values='true' tooltip='true' value='675'></div>
                                    <div class='bar_group__bar thin' label='3' show_values='true' tooltip='true' value='456'></div>
                                    <div class='bar_group__bar thin' label='2.5' show_values='true' tooltip='true' value='245'></div>
                                    <div class='bar_group__bar thin' label='2' show_values='true' tooltip='true' value='343'></div>
                                    <div class='bar_group__bar thin' label='1.5' show_values='true' tooltip='true' value='235'></div>
                                    <div class='bar_group__bar thin' label='1' show_values='true' tooltip='true' value='675'></div>
                                    <div class='bar_group__bar thin' label='0.5' show_values='true' tooltip='true' value='456'></div>
                                    <div class='bar_group__bar thin' label='0' show_values='true' tooltip='true' value='245'></div>
                                </div>
                                <ul class="xExis">
                                    <li>Jan</li>
                                    <li>Feb</li>
                                    <li>Mar</li>
                                    <li>Apr</li>
                                    <li>May</li>
                                    <li>Jun</li>
                                    <li>Jul</li>
                                    <li>Aug</li>
                                    <li>Sep</li>
                                    <li>Oct</li>
                                    <li>Nov</li>
                                    <li>dec</li>
                                </ul>
                            </div>
                        </div>
                        <div class="graphRight">
                            <h3>Export Markets</h3>
                            <div class="graphHlder">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">No. of Transactions</a></li>
                                    <li><a data-toggle="tab" href="#menu1">Transaction Value</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class='wrap_right'>
                                            <div class='bar_group'>
                                                India <div class='bar_group__bar thick' value='390'></div>
                                                Kuwait <div class='bar_group__bar thick' value='290'></div>
                                                Saudi Arabia <div class='bar_group__bar thick' value='190'></div>
                                            </div>
                                            <ul class="xExis">
                                                <li>0</li>
                                                <li>0.5</li>
                                                <li>1</li>
                                                <li>1.5</li>
                                                <li>2</li>
                                                <li>2.5</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <div class='wrap_right'>
                                            <div class='bar_group'>
                                                svnzvc <div class='bar_group__bar thick' value='230'></div>
                                                zxc <div class='bar_group__bar thick' value='130'></div>
                                                zx <div class='bar_group__bar thick' value='160'></div>
                                                zx  <div class='bar_group__bar thick' value='340'></div>
                                                z x <div class='bar_group__bar thick' value='290'></div>
                                            </div>
                                            <p>askdjf bsdn sdvdhb bsadvb</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </li>
                    <li>
                        <strong>June 2015  -  May 2016</strong>
                        <div class="graphleft">
                            <h3>Transactions</h3>
                            <div class='wrap_right'>
                                <div class='bar_group'>
                                    <div class='bar_group__bar thin' label='4' show_values='true' tooltip='true' value='235'></div>
                                    <div class='bar_group__bar thin' label='3.5' show_values='true' tooltip='true' value='675'></div>
                                    <div class='bar_group__bar thin' label='3' show_values='true' tooltip='true' value='456'></div>
                                    <div class='bar_group__bar thin' label='2.5' show_values='true' tooltip='true' value='245'></div>
                                    <div class='bar_group__bar thin' label='2' show_values='true' tooltip='true' value='343'></div>
                                    <div class='bar_group__bar thin' label='1.5' show_values='true' tooltip='true' value='235'></div>
                                    <div class='bar_group__bar thin' label='1' show_values='true' tooltip='true' value='675'></div>
                                    <div class='bar_group__bar thin' label='0.5' show_values='true' tooltip='true' value='456'></div>
                                    <div class='bar_group__bar thin' label='0' show_values='true' tooltip='true' value='245'></div>
                                </div>
                                <ul class="xExis">
                                    <li>Jan</li>
                                    <li>Feb</li>
                                    <li>Mar</li>
                                    <li>Apr</li>
                                    <li>May</li>
                                    <li>Jun</li>
                                    <li>Jul</li>
                                    <li>Aug</li>
                                    <li>Sep</li>
                                    <li>Oct</li>
                                    <li>Nov</li>
                                    <li>dec</li>
                                </ul>
                            </div>
                        </div>
                        <div class="graphRight">
                            <h3>Export Markets</h3>
                            <div class="graphHlder">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">No. of Transactions</a></li>
                                    <li><a data-toggle="tab" href="#menu1">Transaction Value</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class='wrap_right'>
                                            <div class='bar_group'>
                                                India <div class='bar_group__bar thick' value='390'></div>
                                                Kuwait <div class='bar_group__bar thick' value='290'></div>
                                                Saudi Arabia <div class='bar_group__bar thick' value='190'></div>
                                            </div>
                                            <ul class="xExis">
                                                <li>0</li>
                                                <li>0.5</li>
                                                <li>1</li>
                                                <li>1.5</li>
                                                <li>2</li>
                                                <li>2.5</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <div class='wrap_right'>
                                            <div class='bar_group'>
                                                svnzvc <div class='bar_group__bar thick' value='230'></div>
                                                zxc <div class='bar_group__bar thick' value='130'></div>
                                                zx <div class='bar_group__bar thick' value='160'></div>
                                                zx  <div class='bar_group__bar thick' value='340'></div>
                                                z x <div class='bar_group__bar thick' value='290'></div>
                                            </div>
                                            <p>askdjf bsdn sdvdhb bsadvb</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </li>
                    <li>
                        <strong>June 2015  -  May 2016</strong>
                        <div class="graphleft">
                            <h3>Transactions</h3>
                            <div class='wrap_right'>
                                <div class='bar_group'>
                                    <div class='bar_group__bar thin' label='4' show_values='true' tooltip='true' value='235'></div>
                                    <div class='bar_group__bar thin' label='3.5' show_values='true' tooltip='true' value='675'></div>
                                    <div class='bar_group__bar thin' label='3' show_values='true' tooltip='true' value='456'></div>
                                    <div class='bar_group__bar thin' label='2.5' show_values='true' tooltip='true' value='245'></div>
                                    <div class='bar_group__bar thin' label='2' show_values='true' tooltip='true' value='343'></div>
                                    <div class='bar_group__bar thin' label='1.5' show_values='true' tooltip='true' value='235'></div>
                                    <div class='bar_group__bar thin' label='1' show_values='true' tooltip='true' value='675'></div>
                                    <div class='bar_group__bar thin' label='0.5' show_values='true' tooltip='true' value='456'></div>
                                    <div class='bar_group__bar thin' label='0' show_values='true' tooltip='true' value='245'></div>
                                </div>
                                <ul class="xExis">
                                    <li>Jan</li>
                                    <li>Feb</li>
                                    <li>Mar</li>
                                    <li>Apr</li>
                                    <li>May</li>
                                    <li>Jun</li>
                                    <li>Jul</li>
                                    <li>Aug</li>
                                    <li>Sep</li>
                                    <li>Oct</li>
                                    <li>Nov</li>
                                    <li>dec</li>
                                </ul>
                            </div>
                        </div>
                        <div class="graphRight">
                            <h3>Export Markets</h3>
                            <div class="graphHlder">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">No. of Transactions</a></li>
                                    <li><a data-toggle="tab" href="#menu1">Transaction Value</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class='wrap_right'>
                                            <div class='bar_group'>
                                                India <div class='bar_group__bar thick' value='390'></div>
                                                Kuwait <div class='bar_group__bar thick' value='290'></div>
                                                Saudi Arabia <div class='bar_group__bar thick' value='190'></div>
                                            </div>
                                            <ul class="xExis">
                                                <li>0</li>
                                                <li>0.5</li>
                                                <li>1</li>
                                                <li>1.5</li>
                                                <li>2</li>
                                                <li>2.5</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <div class='wrap_right'>
                                            <div class='bar_group'>
                                                svnzvc <div class='bar_group__bar thick' value='230'></div>
                                                zxc <div class='bar_group__bar thick' value='130'></div>
                                                zx <div class='bar_group__bar thick' value='160'></div>
                                                zx  <div class='bar_group__bar thick' value='340'></div>
                                                z x <div class='bar_group__bar thick' value='290'></div>
                                            </div>
                                            <p>askdjf bsdn sdvdhb bsadvb</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </li>
                </ul>
            </div>
        </section>	
        <section class="proUl noBoreder">
            <h2>Transaction Details</h2>
        </section>
        <section class="ShipDestntn">
            <ul>
                <li>
                    <span class="firstSpn">Shipping Destination</span>
                    <span class="secondSpn">Transaction Value</span>
                    <span class="thrdSpn">Transaction Date</span>
                </li>
                <li>
                    <span class="firstSpn">India</span>
                    <span class="secondSpn">USD $*,***,***.**</span>
                    <span class="thrdSpn">June 20 - 2016</span>
                </li>
                <li>
                    <span class="firstSpn">Kuwait</span>
                    <span class="secondSpn">USD $*,***,***.**</span>
                    <span class="thrdSpn">June 20 - 2016</span>
                </li>
                <li>
                    <span class="firstSpn">Saudi Arabia</span>
                    <span class="secondSpn">USD $*,***,***.**</span>
                    <span class="thrdSpn">May 10 - 2016</span>
                </li>
            </ul>
        </section>
        <a name='tradeShows'></a>
        <h1 class="blueheading">Additional Information</h1>
        <h2 class="headingh2">Trade Shows</h2>
        <section class="proUl">
            <h2>Trade Show Name Here</h2>
            <div class="co_ul">
                <ul>
                    <li>
                        <label>Date Attended:</label>
                        <span>January 2016</span>
                    </li>
                    <li>
                        <label>Host Country/Region:</label>
                        <span>India</span>
                    </li>
                </ul>
            </div>
        </section>
        <section class="lastBtns">
            <div class="liBtn">
                <a href="#" class="suplierbtn">Contact Supplier</a>
                <a href="#" class="strtbtn">Start Order</a>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>template/front/assets/js/jquery.bxslider.min"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/front/assets/js/bars.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/front/assets/js/jquery.bxslider.js"></script>

<script type="text/javascript">
    $('.bxslider').bxSlider({
        autoControls: true
    });
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>