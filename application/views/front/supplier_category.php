<div id="wrapper " class="mainSupp">
    <div class="container ">
        <section class="sup_nav">
            <ul>
                <li><a href="<?php echo base_url() ?>/index.php/home/supplier_home">Home</a></li>
                <li class="active">
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Product Categories
                        <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url() ?>/home/supplier_category">All Categories</a></li>
                          <li><a href="#">Category One</a></li>
                          <li><a href="#">Category Two</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="dropdown">
                        <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Company Profile
                        <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                           <li><a href="<?php echo site_url() ?>/home/supplier_profile">Home</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradingSection">Trading</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#productionSection">Production</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#researchDevelopment">Research & Development</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#buyerInteractions">Buyer Interactions</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#transactionHistory">Transaction History</a></li>
                            <li><a href="<?php echo site_url() ?>/home/supplier_profile#tradeShows">Trade Shows</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="<?php echo base_url() ?>/index.php/home/supplier_contact">Contact</a></li>
            </ul>
        </section>
        <section class="imgBanner">
            <a href="#"><img src="<?php echo base_url(); ?>template/front/assets/images/s_img03.png"></a>
        </section>
        <section class="supplier">
            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img01.png">
        </section>
        <h1>Categories</h1>
        <section class="cat_name">
            <h2>Category 1 Name Here</h2>
            <span class="forborder"></span>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>

            </div>	
            <a href="viewAllbtn">View All</a>
        </section>
        <section class="cat_name">
            <h2>Category 1 Name Here</h2>
            <span class="forborder"></span>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>

            </div>	
            <a href="viewAllbtn">View All</a>
        </section>
        <section class="cat_name">
            <h2>Category 1 Name Here</h2>
            <span class="forborder"></span>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="myCol">
                        <div class="imgCat">
                            <img src="<?php echo base_url(); ?>template/front/assets/images/s_img02.png">
                        </div>
                        <p>Product Name Here</p>
                    </div>
                </div>

            </div>	
            <a href="viewAllbtn">View All</a>
        </section>
    </div>
</div>
<!--end of Html for supplier-->
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>