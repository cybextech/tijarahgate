<section class="memberBanner">
    <div class="memdetailBan">
        <div class="memberlogo">
            <img src="<?php echo base_url() ?>/template/front/assets/images/img16.png">
        </div>
        <p>Be a Member of Tijara Gate to maximise your credibility and help you acquire trust from your buyers quickly which means more profit to your business!</p>
    </div>
</section>
<div class="container">
    <section class="effectivtool">
        <h2>The most effective tool for your B2B business</h2>
        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
    </section>
    <section class="prviliged">
        <h2>Choose which membership privileges you need</h2>
        <ul>
            <li class="preHeading">
                <span class="feature">Main Features</span>
                <span class="free">FREE</span>
                <span class="selvir">SILVER</span>
                <span class="gold">GOLD</span>
            </li>
            <li class="prebody">
                <span class="feature">Priority Ranking</span>
                <span class="free">3rd</span>
                <span class="selvir">2nd</span>
                <span class="gold">1st</span>
            </li>
            <li class="prebody">
                <span class="feature">Product Posting</span>
                <span class="free">50</span>
                <span class="selvir">Unlimited</span>
                <span class="gold">Unlimited</span>
            </li>
            <li class="prebody">
                <span class="feature">Product Showcases</span>
                <span class="free">--</span>
                <span class="selvir">12</span>
                <span class="gold">28</span>
            </li>
            <li class="prebody">
                <span class="feature">Ability to quote Post Buying</span>
                <span class="free"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="selvir"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="gold"><i class="fa fa-check" aria-hidden="true"></i></span>
            </li>
            <li class="prebody">
                <span class="feature">Verified Icon</span>
                <span class="free"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="selvir"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="gold"><i class="fa fa-check" aria-hidden="true"></i></span>
            </li>
            <li class="prebody">
                <span class="feature">Customized Website</span>
                <span class="free"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="selvir"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="gold"><i class="fa fa-check" aria-hidden="true"></i></span>
            </li>
            <li class="prebody">
                <span class="feature">Personalized Customer Service</span>
                <span class="free"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="selvir"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span class="gold"><i class="fa fa-check" aria-hidden="true"></i></span>
            </li>
            <li class="prebody lastli">
                <span class="feature"></span>
                <span class="free"><a href="<?php echo base_url() ?>/index.php/home/register">Join Free</a></span>
                <span class="selvir"><a href="#">Apply Now</a></span>
                <span class="gold"><a href="#">Apply Now</a></span>
            </li>
        </ul>
    </section>
</div>