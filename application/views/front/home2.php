<style>
    .selected_products .img-holder > img {
        height: auto;
        width: 100%;
    }
    .selected_products .carousel .slide {
        margin: 0 25px 0 0;
        width: 205px !important;
    }
    .selected_products .carousel .slide dl {
        display: block;
        margin: 0;
        overflow: hidden;
        padding: 0;
    }
    .selected_products .carousel .slide dl dt, .selected_products .carousel .slide dl dd {
        display: inline-block;
        margin: 0;
        padding: 0;
        vertical-align: top;
    }
    .selected_products .carousel .slide .img-holder {
        height: 182px;
        width: 182px;
    }
    .selected_products .carousel .slide .heading {
        color: hsl(0, 0%, 20%);
        font-size: 18px;
        font-weight: 500;
        line-height: 16px;
        margin: 0 0 9px;
        text-align: left;
        text-transform: capitalize;
    }
    .selected_products .carousel .slide .title {
        color: #999999;
        font-size: 13px;
        margin: 0;
    }
    .selected_products .carousel .slide > a {
        color: hsl(195, 100%, 55%);
    }
    .super_deals .section > h2 {
        color: hsl(0, 0%, 0%);
    }
    .img-holder {
        height: 182px !important;
        width: 100% !important;
    }
    .img-holder > img {
        height: auto;
        max-width: 100%;
    }
    .slideset .heading {
        color: hsl(0, 0%, 0%);
        padding: 0;
        text-align: left;
    }
    .slideset .price {
        color: hsl(195, 100%, 55%);
        float: left;
        font-size: 18px;
        width: 100%;
    }
    .slideset .star-rate {
        float: left;
        width: 100%;
    }
    .text-center.compare-wish {
        float: left;
        width: 100%;
    }
    .btn-u.btn-u-cust.btn_wish {
        border-right: 1px solid hsl(0, 0%, 83%);
    }
    .btn-u.btn-u-cust.btn_compare {
        margin: 0 -3px 0 0;
        width: 101px;
    }

    .compare-wish .btn-u {
        background: hsl(0, 0%, 100%) none repeat scroll 0 0;
        border-bottom: 1px solid hsl(0, 0%, 83%);
        border-left: 1px solid hsl(0, 0%, 83%);
        border-top: 1px solid hsl(0, 0%, 83%);
        color: hsl(0, 0%, 83%);
        padding: 0;
        width: 99px;
        text-transform: uppercase;
    }
    .btn-u.btn-u-cust.btn_wish {
        border-right: 1px solid hsl(0, 0%, 83%);
    }

    .btn-u.req-quot {
        background: hsl(0, 0%, 100%) none repeat scroll 0 0;
        border-bottom: 1px solid hsl(0, 0%, 83%);
        border-left: 1px solid hsl(0, 0%, 83%);
        border-right: 1px solid hsl(0, 0%, 83%);
        color: hsl(37, 100%, 50%);
        padding: 3px 0 2px;
        width: 98%;
        text-transform: uppercase;
    }
    .btn-u.req-quot:hover {
        background: hsl(37, 100%, 50%) none repeat scroll 0 0;
        color: hsl(0, 0%, 100%);
    }
    .star-rate .list-inline.product-ratings.col-md-12.col-sm-12.col-xs-12.tooltips.text-center {
        float: left;
        padding: 0;
    }
    .star-rate .list-inline.product-ratings.col-md-12.col-sm-12.col-xs-12.tooltips.text-center > li {
        float: left;
    }
    .super_deals .img-holder {
        margin: 0 !important;
    }
    .super_deals .heading {
        color: #333333 !important;
        font-size: 18px;
        line-height: 21px;
        margin: 5px 0 1px;
    }
    .star-rate .col-md-12 {
        padding: 0;
    }
    .selected_products .img-holder {
        margin: 0 !important;
    }
    .selected_products .heading {
        margin: 12px 0 5px !important;
    }

    .star-rate .list-inline.product-ratings.col-md-12.col-sm-12.col-xs-12.tooltips.text-center > li {
        margin: 0 0 0 2px;
    }
    @media only screen and (min-width: 768px) and (max-width: 1023px) {
        .selected_products .carousel .slide {
            width: 178px !important;
        }
    }
    @media only screen and (min-width: 1024px) and (max-width: 1279px) {
        .selected_products .carousel .slide {
            width: 192px !important;
        }
    }

</style>
<script src="<?php echo base_url(); ?>template/front/assets/js/jquery.bxslider.min.js"></script>
<div class="container">
    <section class="content-holder">
        <ul class="bxslider">
            <li>
                <img src="<?php echo base_url(); ?>template/front/assets/img/main-slide1.jpg" />

            </li>
            <li>
                <img src="<?php echo base_url(); ?>template/front/assets/img/home-main-slide-2.jpg" />

            </li>
            <li>
                <img src="<?php echo base_url(); ?>template/front/assets/img/home-main-slide-3.jpg" />

            </li>

        </ul>
        <ul class="data-list">
            <li><i class="fa fa-glass" aria-hidden="true"></i><a href="<?php echo base_url() . 'index.php/home/category_suppliers' ?>">Products</a></li>
            <li><i class="fa fa-users" aria-hidden="true"></i><a href="<?php echo base_url() . 'index.php/home/supplier_search' ?>">Suppliers</a></li>
            <li><i class="fa fa-cubes" aria-hidden="true"></i><a href="<?php echo base_url() . 'index.php/home/wholesaler'; ?>">Wholesalers</a></li>
        </ul>
    </section>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.bxslider').bxSlider({
                controls: false,
                auto: true,
            });
        });
    </script>
    <section class="addsBanner">
        <?php
        $place = 'after_slider';
        $query = $this->db->order_by('banner_id', 'RANDOM');
        $query = $this->db->limit(1);
        $query = $this->db->get_where('banner', array('page' => 'home', 'place' => $place, 'status' => 'ok'));

        $banners = $query->result_array();
        foreach ($banners as $row) {
            ?>
            <a href="<?php echo $row['link']; ?>">
                <img src="<?php echo $this->crud_model->file_view('banner', $row['banner_id'], '', '', 'no', 'src') ?>">
            </a>
            <?php
        }
        ?>
    </section>
    <section class="section trade-section">
        <h2>Trade Services</h2>
        <div class="row">
            <div class="data-box">
                <img src="<?php echo base_url(); ?>template/front/assets/img/img02.jpg" height="300" width="620" alt="image description">
                <div class="outer">
                    <h3>e-Finance</h3>
                    <p>Finance your trade<br> with Tijara Gate</p>
                    <a href="<?php echo base_url() ?>/index.php/home/finance" class="more">Learn more &raquo;</a><br>
                    <a href="#" class="btn">Yes, I want to apply now!</a>
                </div>
            </div>
            <div class="data-box">
                <img src="<?php echo base_url(); ?>template/front/assets/img/img03.jpg" height="300" width="620" alt="image description">
                <div class="outer">
                    <h3>Shipping</h3>
                    <p>Fast &amp; Reliable Logistic Service</p>
                    <ul class="list">
                        <li><i class="fa fa-check" aria-hidden="true"></i>Competitive Shipping Fees</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Total Delivery Service Assurance</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Get Fast Logistics Solutions</li>
                    </ul>
                    <a href="<?php echo base_url() ?>/index.php/home/shipping_calculator" class="btn blue">Learn more &raquo;</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="data-box">
                <img src="<?php echo base_url(); ?>template/front/assets/img/img04.jpg" height="300" width="620" alt="image description">
                <div class="outer">
                    <h3>Inspection</h3>
                    <p>Be assured and get your order inspected</p>
                    <ul class="list">
                        <li><i class="fa fa-check" aria-hidden="true"></i>Business Specific Inspectors</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Cost Efficient &amp; Convenient</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Low Product Inspection Fees</li>
                    </ul>
                    <a href="<?php echo base_url() ?>/index.php/home/inspection" class="btn blue">Learn more &raquo;</a>
                </div>
            </div>
            <div class="data-box">
                <img src="<?php echo base_url(); ?>template/front/assets/img/img05.jpg" height="300" width="620" alt="image description">
                <div class="outer">
                    <h3>Get Verified</h3>
                    <p>Let buyers & suppliers know your credibility</p>
                    <ul class="list">
                        <li><i class="fa fa-check" aria-hidden="true"></i>Show your VERIFIED status</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Ease of transaction from buyers & suppliers</li>
                    </ul>
                    <a href="<?php echo base_url() ?>/index.php/home/get_verification" class="more">Learn more &raquo;</a>
                    <a href="#" class="btn">Yes, I want to get verified now!</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section trade-protection">
        <h2>Trade Protection</h2>
        <div class="trade-block" style="background:url(<?php echo base_url(); ?>template/front/assets/img/bg-trade.jpg) no-repeat;">
            <div class="left-area">
                <h3>Trade Protection</h3>
                <p>You are completely covered with <br>Tijara Gate.</p>
                <ul class="list">
                    <li><i class="fa fa-check" aria-hidden="true"></i>Secured Payment Process</li>
                    <li><i class="fa fa-check" aria-hidden="true"></i>Product Quality Assurance</li>
                </ul>
            </div>
            <div class="right-area">
                <strong class="title">Get your order fully protected:</strong>
                <ol>
                    <li>Order confirmation by a Trade Protection supplier</li>
                    <li>Direct payment, thru credit card or bank transfer, to the supplier's <span class=badge>bank</span> account assigned by TijaraGate.com </li>
                </ol>
                <a href="<?php echo base_url() ?>/index.php/home/trade_protection" class="btn"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Order with Trade Protection</a>
            </div>
        </div>
    </section>

    <section class="section">
        <h2>Post Buying</h2>
        <div class="buying-block">
            <div class="left-area">
                <h3>Search suppliers for your products.</h3>
                <ul class="supplier-list">
                    <li>
                        <i class="fa fa-leaf"></i>
                        <span class="title">Simple</span>
                        <p>Post a buying request <br>in no time</p>
                    </li>
                    <li>
                        <i class="fa fa-cogs" aria-hidden="true"></i>
                        <span class="title">Efficient</span>
                        <p>Receive numerous quotes<br>right-away & hassle-free</p>
                    </li>
                    <li>
                        <i class="fa fa-bullseye"></i>
                        <span class="title">ALL-in-one</span>
                        <p>Compare quotations, samples and deals</p>
                    </li>
                </ul>
                <a href="<?php echo base_url() . 'index.php/home/get_quotation' ?>" class="btn"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Get Quotation Now</a>
            </div>
            <div class="right-area">
                <img src="<?php echo base_url(); ?>template/front/assets/img/img07.png" height="330" width="620" alt="image description">
                <div class="outer"><h3>Post Buying</h3></div>
            </div>
        </div>
    </section>

</div>


<div class="container selected_products">
    <section class="section">
        <h2>Selected Products <a href="#">See all &raquo;</a></h2>

        <div class="partner-block carousel">
            <a href="#" class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
            <a href="#" class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            <div class="mask">
                <div class="slideset">
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img16.jpg"></div>
                        <p class="heading">Electronic Cigarette</p>

                        <dl>
                            <dt>Brand Name:</dt>
                            <dd>S-body</dd>
                        </dl>
                        <dl>
                            <dt>Battery:</dt>
                            <dd>650mah</dd>
                        </dl>

                        <a href="">600 items >></a>
                    </div>

                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img17.jpg"></div>
                        <p class="heading">Cardboard Box</p>

                        <dl>
                            <dt>Feature:</dt>
                            <dd>Recyclable</dd>
                        </dl>
                        <dl>
                            <dt>Material:</dt>
                            <dd>Paper</dd>
                        </dl>

                        <a href="">3831 items >></a>
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img18.jpg"></div>
                        <p class="heading">Rechargable Fan</p>

                        <dl>
                            <dt>Power Source:</dt>
                            <dd>Battery</dd>
                        </dl>
                        <dl>
                            <dt>Installation</dt>
                            <dd>Portable</dd>
                        </dl>

                        <a href="">545 items >></a>
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img11.png"></div>
                        <p class="heading">Electronic Cigarette</p>

                        <dl>
                            <dt>Battery:</dt>
                            <dd>280mah</dd>
                        </dl>
                        <dl>
                            <dt>Brand Name:</dt>
                            <dd>BBTank</dd>
                        </dl>

                        <a href="">306 items >></a>
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img19.jpg"></div>
                        <p class="heading">Vegetable</p>

                        <dl>
                            <dt>Material:</dt>
                            <dd>Metal</dd>
                        </dl>
                        <dl>
                            <dt>Fruit and Veg Tools Type:</dt>
                            <dd>Vegetable...</dd>
                        </dl>

                        <a href="">156 items >></a>
                    </div>

                </div>
            </div>
        </div>




    </section>
</div>

<div class="container super_deals">
    <section class="section">
        <h2>Super Deals <a href="#">See all &raquo;</a></h2>

        <div class="partner-block carousel">
            <a href="#" class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
            <a href="#" class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            <div class="mask">
                <div class="slideset">
                    <?php
                    foreach ($featured_data as $row1) {
                        if ($this->crud_model->is_publishable($row1['product_id'])) {
                            ?>
                            <div class="slide">
                                <div class="img-holder"><img src="<?php echo $this->crud_model->file_view('product', $row1['product_id'], '', '', 'thumb', 'src', 'multi', 'one'); ?>" alt="image description"></div>
                                <a href="<?php echo $this->crud_model->product_link($row1['product_id']); ?>"><p class="heading"><?php echo $row1['title'] ?></p></a>

                                <div class="star-rate">
                                    <div class="col-md-12"> 
                                        <ul class="list-inline product-ratings col-md-12 col-sm-12 col-xs-12 tooltips text-center"
                                            data-original-title="<?php echo $rating = $this->crud_model->rating($row1['product_id']); ?>"	
                                            data-toggle="tooltip" data-placement="top" >
                                                <?php
                                                $rating = $this->crud_model->rating($row1['product_id']);
                                                $r = $rating;
                                                $i = 0;
                                                while ($i < 5) {
                                                    $i++;
                                                    ?>
                                                <li>
                                                    <i class="rating<?php
                                                    if ($i <= $rating) {
                                                        echo '-selected';
                                                    } $r--;
                                                    ?> fa fa-star<?php
                                                       if ($r < 1 && $r > 0) {
                                                           echo '-half';
                                                       }
                                                       ?>"></i>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <span class="price"><?php echo currency() . $this->crud_model->get_product_price($row1['product_id']); ?></span>
                                <div class=" text-center compare-wish"> 

                                    <a class="btn-u btn-u-cust  <?php if ($this->crud_model->is_compared($row1['product_id']) == 'yes') { ?>disabled<?php } else { ?>btn_compare<?php } ?>"  data-pid='<?php echo $row1['product_id']; ?>' >
                                        <?php if ($this->crud_model->is_compared($row1['product_id']) == 'yes') { ?>
                                            <?php echo translate('compared'); ?>
                                        <?php } else { ?>
                                            <?php echo translate('compare'); ?>
                                        <?php } ?>
                                    </a>

                                    <?php
                                    $wish = $this->crud_model->is_wished($row1['product_id']);
                                    ?>

                                    <a rel="outline-outward" class="btn-u btn-u-cust   <?php if ($wish == 'yes') { ?>btn_wished<?php } else { ?>btn_wish<?php } ?>" style="" data-pid='<?php echo $row1['product_id']; ?>'>
                                        <?php if ($wish == 'yes') { ?>
                                            <?php echo translate('wished'); ?>
                                        <?php } else { ?>
                                            <?php echo translate('wish'); ?>
                                        <?php } ?>
                                    </a>

                                </div>
                                <div class=" text-center" >
                                    <a class="btn-u req-quot" href="<?php echo site_url() ?>/home/get_quotation"   type="button"  data-pid='<?php echo $row1['product_id']; ?>' >
                                        <?php echo 'Get Quotation' ?>  
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

    </section>
</div>

<div class="container">
    <section class="section">
        <h2>Top Suppliers <a href="#">See all &raquo;</a></h2>
        <div class="partner-block carousel">
            <a href="#" class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
            <a href="#" class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            <div class="mask">
                <div class="slideset">
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" height="166" width="146" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img14.png" height="122" width="194" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" height="166" width="146" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img14.png" height="122" width="194" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" height="166" width="146" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img14.png" height="122" width="194" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" height="166" width="146" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img14.png" height="122" width="194" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img13.png" height="166" width="146" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                    <div class="slide">
                        <div class="img-holder"><img src="<?php echo base_url(); ?>template/front/assets/img/img14.png" height="122" width="194" alt="image description"></div>
                        <strong class="title"><a href="<?php echo site_url() ?>/home/supplier_home">Supplier Name Here</a></strong>
                        From Kuwait
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <?php
        $place = 'after_suppliers';
        $query = $this->db->order_by('banner_id', 'RANDOM');
        $query = $this->db->limit(2);
        $query = $this->db->get_where('banner', array('page' => 'home', 'place' => $place, 'status' => 'ok'));

        $banners = $query->result_array();
        foreach ($banners as $row) {
            ?>
            <div class="col-dm-6 col-sm-6">
                <section class="addsBanner">
                    <a href="<?php echo $row['link']; ?>">
                        <img src="<?php echo $this->crud_model->file_view('banner', $row['banner_id'], '', '', 'no', 'src') ?>">
                    </a>
                </section>
            </div>

            <?php
        }
        ?>


    </div>
    <section class="benifits-section holder">
        <div class="left-area">
            <h2>Benefits in using <span class="colored">Tijara Gate</span></h2>
            <div class="row">
                <div class="box">
                    <strong class="title">For Buyers:</strong>
                    <ul class="list">
                        <li><i class="fa fa-check" aria-hidden="true"></i>Increase Profit</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Better Cash Flow</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Overhead Savings</li>
                    </ul>
                </div>
                <div class="box">
                    <strong class="title">For Suppliers:</strong>
                    <ul class="list">
                        <li><i class="fa fa-check" aria-hidden="true"></i>Increase Sales</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i>Increase Profit</li>
                    </ul>
                </div>
            </div>
            <a href="<?php echo base_url(); ?>index.php/home/register" class="btn"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Sign up for FREE!</a>
        </div>
        <div class="right-area"><img src="<?php echo base_url(); ?>template/front/assets/img/img15.png" height="370" width="620" alt="video place holder"></div>
    </section>

    <section class="search-section holder">
        <div class="row">
            <div class="box">
                <div class="frame">
                    <h2>Search Wholesaler</h2>
                    <p>Save a lot of time. Find your specific product wholesaler fast and accurate.</p>
                    <form class="search-from wholesaler" action="<?php echo base_url() ?>/index.php/home/wholesaler">
                        <fieldset>
                            <div class="input-holder">
                                <input type="search" placeholder="Type product keyword here..." name="search">
                                <input type="submit" value="GO" class="btn blue">
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="box">
                <div class="frame">
                    <h2>Find Suppliers by Countries</h2>
                    <ul class="flag-list">
                        <li><a href="<?php echo base_url() ?>/index.php/home/conurty_single/kuwait"><img src="<?php echo base_url(); ?>template/front/assets/img/kuwait.png" height="47" width="70" alt="kuwait">Kuwait</a></li>
                        <li><a href="<?php echo base_url() ?>/index.php/home/conurty_single/behrain"><img src="<?php echo base_url(); ?>template/front/assets/img/behrain.png" height="47" width="70" alt="behrain">Bahrain</a></li>
                        <li><a href="<?php echo base_url() ?>/index.php/home/conurty_single/saudiarabia"><img src="<?php echo base_url(); ?>template/front/assets/img/saudiarabia.png" height="47" width="70" alt="sauidarabia">Saudi Arabia</a></li>
                        <li><a href="<?php echo base_url() ?>/index.php/home/conurty_single/uae"><img src="<?php echo base_url(); ?>template/front/assets/img/uae.png" height="47" width="70" alt="uae">UAE</a></li>
                        <li><a href="<?php echo base_url() ?>/index.php/home/conurty_single/oman"><img src="<?php echo base_url(); ?>template/front/assets/img/oman.png" height="47" width="70" alt="oman">Oman</a></li>
                        <li><a href="<?php echo base_url() ?>/index.php/home/conurty_single/qatar"><img src="<?php echo base_url(); ?>template/front/assets/img/qatar.png" height="47" width="70" alt="qatar">Qatar</a></li>
                        <li><a href="<?php echo base_url() ?>/index.php/home/conurty_single/india"><img src="<?php echo base_url(); ?>template/front/assets/img/india.png" height="47" width="70" alt="india">India</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="addsBanner">
        <?php
        $place = 'bottom';
        $query = $this->db->order_by('banner_id', 'RANDOM');
        $query = $this->db->limit(1);
        $query = $this->db->get_where('banner', array('page' => 'home', 'place' => $place, 'status' => 'ok'));

        $banners = $query->result_array();
        foreach ($banners as $row) {
            ?>
            <a href="<?php echo $row['link']; ?>">
                <img src="<?php echo $this->crud_model->file_view('banner', $row['banner_id'], '', '', 'no', 'src') ?>">
            </a>
            <?php
        }
        ?>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('.drops').dropdown();
        $('.dropss').dropdown();
    });

    $('body').on('click', '.category_drop .cd-dropdown li', function () {
        var category = $(this).data('value');
        var list1 = $('.sub_category_drop');
        $.ajax({
            url: '<?php echo base_url(); ?>index.php/home/others/get_sub_by_cat/' + category,
            beforeSend: function () {
                list1.html('');
            },
            success: function (data) {
                var res = ""
                        + " <select name='sub_category' onchange='get_pricerange(this.value)' class='dropss cd-select'  id='sub_category'>"
                        + " 	<option value='0'><?php echo translate('choose_sub_category'); ?></option>"
                        + data
                        + " </select>"
                list1.html(res);
                $('body .dropss').dropdown();
            },
            error: function (e) {
                console.log(e)
            }
        });
        $.ajax({
            url: '<?php echo base_url(); ?>index.php/home/others/get_home_range_by_cat/' + category,
            beforeSend: function () {
            },
            success: function (data) {
                var myarr = data.split("-");
                var res = ''
                        + '<div class="nstSlider" '
                        + '	data-range_min="' + myarr[0] + '" data-range_max="' + myarr[1] + '" '
                        + '	data-cur_min="' + myarr[0] + '"  data-cur_max="' + myarr[1] + '">'
                        + '<div class="highlightPanel"></div> '
                        + '<div class="bar"></div>   '
                        + '<div class="leftGrip"></div> '
                        + '<div class="rightGrip"></div>'
                        + '</div>';
                $('.nstSlider').remove();
                $('#ranog').html(res);
                take_range(myarr[0], myarr[1]);
            },
            error: function (e) {
                console.log(e)
            }
        });
    });
    $('body').on('click', '.sub_category_drop .cd-dropdown li', function () {
        var sub_category = $(this).data('value');
        var list2 = $('#range');
        $.ajax({
            url: '<?php echo base_url(); ?>index.php/home/others/get_home_range_by_sub/' + sub_category,
            beforeSend: function () {
            },
            success: function (data) {
                var myarr = data.split("-");
                var res = ''
                        + '<div class="nstSlider" '
                        + '	data-range_min="' + myarr[0] + '" data-range_max="' + myarr[1] + '" '
                        + '	data-cur_min="' + myarr[0] + '"  data-cur_max="' + myarr[1] + '">'
                        + '<div class="highlightPanel"></div> '
                        + '<div class="bar"></div>   '
                        + '<div class="leftGrip"></div> '
                        + '<div class="rightGrip"></div>'
                        + '</div>';
                $('.nstSlider').remove();
                $('#ranog').html(res);
                take_range(myarr[0], myarr[1]);
            },
            error: function (e) {
                console.log(e)
            }
        });
    });
    function filter() {}
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/front/assets/js/jquery.carousel.js"></script>