<div>
    <div class="panel-body">
    	<div class="col-sm-2"><strong>Location</strong></div>
        <div class="col-sm-2"><strong>Destination</strong></div>
        <div class="col-sm-2"><strong>Volume</strong></div>
        <div class="col-sm-2"><strong>Weight</strong></div>
		<div class="col-sm-2"><strong>Freight Rate</strong></div>
        <div class="col-sm-2"><strong>Destination Rate</strong></div>
		<?php
		foreach($shipping_rates as $sr) 
		{
			$country = $shipping_countries[$sr['country_id']];
			$location = $shipping_cities[$sr['location']];
			$destination = $shipping_cities[$sr['destination']];
			?>
            <div class="col-sm-2"><?php echo $shipping_cities[$sr['departure_city_id']-1]['name'] ?></div>
            <div class="col-sm-2"><?php echo $shipping_cities[$sr['destination_city_id']-1]['name'] ?></div>
            <div class="col-sm-2"><?php echo $sr['volume'] ?></div>
            <div class="col-sm-2"><?php echo $sr['weight'] ?></div>
            <div class="col-sm-2"><?php echo $sr['freight_rate'] ?></div>
            <div class="col-sm-2"><?php echo $sr['destination_rate'] ?></div>
            <?php
		}
		?>
    </div>
</div>