<?php
foreach ($inspector_data as $data_row) {
    ?>
    <div class="tab-pane fade active in" id="edit">
        <?php
        echo form_open(base_url() . 'index.php/admin/inspector/update/' . $data_row['inspector_id'], array(
            'class' => 'form-horizontal',
            'method' => 'post',
            'id' => 'inspector_edit',
            'enctype' => 'multipart/form-data'
        ));
        ?>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-4 control-label" for="departure_country_code">Country: </label>
                <div class="col-sm-6">
                    <select name="country_code" onchange="get_cities(this.value, 'location')">
                        <?php
                        foreach ($shipping_countries as $sc) {
                            ?>
                            <option value="<?php echo $sc['code'] ?>" <?php if($data_row['country_code'] == $sc['code']) echo 'selected' ?>>
                                <?php echo $sc['name'] . ' (' . $sc['code'] . ')' ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group location-cities">
                <?php
                if ($data_row['city'] != ''):
                    $cities = $this->db->get_where('shipping_city', array('countryCode' => $data_row['country_code']))->result_array();
                    ?>
                    <label class="col-sm-4 control-label" for="demo-hor-1">Select City: </label>
                    <div class="col-sm-6">
                        <select name="location_city_id">
                            <?php foreach ($cities as $city) { ?>
                                <option value="<?php echo $city['id'] ?>" <?php if ($city['id'] == $data_row['city']) echo 'selected' ?>>
                                    <?php echo $city['name'] ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="industry">Industry: </label>
                <div class="col-sm-6">
                    <select name="industry">
                        <?php
                        foreach ($inspection_industries as $row) {
                            ?>
                            <option value="<?php echo $row['id'] ?>" <?php if($row['id'] == $data_row['industry'] )echo 'selected' ?>>
                                <?php echo $row['name'] ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="language"> language: </label>
                <div class="col-sm-6">
                    <select name="language">
                        <?php
                        foreach ($inspection_languages as $row) {
                            ?>
                            <option value="<?php echo $row['id'] ?>" <?php if($row['id'] == $data_row['language'])echo 'selected' ?>>
                                <?php echo $row['name'] ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Inspector Type: </label>
                <div class="col-sm-6">
                    <select name="type" >
                        <?php
                        foreach ($inspector_types as $row) {
                            ?>
                            <option value="<?php echo $row['id'] ?>" <?php if($row['id'] == $data_row['type'])echo 'selected' ?>>
                                <?php echo $row['name'] ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1">
                    Inspector Name
                </label>
                <div class="col-sm-6">
                    <input type="text" name="inspector_name"  
                           value="<?php echo $data_row['inspector_name']; ?>" id="demo-hor-1" 
                           class="form-control required" placeholder="Inspector Name" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-2">
                    Inspector Rate
                </label>
                <div class="col-sm-6">
                    <input type="text" name="inspector_rate"  
                           value="<?php echo $data_row['inspector_rate']; ?>" id="demo-hor-2" 
                           class="form-control required" placeholder="Inspector Rate" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Medium: </label>
                <div class="col-sm-6">
                    <input type="checkbox" name="inspection_type[]" value="inspection_type1" class="" <?php if($data_row['inspection_type1'] == 1)echo 'checked' ?> /> Container Loading Check<br/>
                    <input type="checkbox" name="inspection_type[]" value="inspection_type2" class="" <?php if($data_row['inspection_type2'] == 1)echo 'checked' ?> />  During Production Inspection <br/>
                    <input type="checkbox" name="inspection_type[]" value="inspection_type3" class="" <?php if($data_row['inspection_type3'] == 1)echo 'checked' ?> /> Factory Audit Final Random Inspection<br/>
                    <input type="checkbox" name="inspection_type[]" value="inspection_type4" class="" <?php if($data_row['inspection_type4'] == 1)echo 'checked' ?> /> Full Inspection Initial Production Inspection<br/>
                    <input type="checkbox" name="inspection_type[]" value="inspection_type5" class="" <?php if($data_row['inspection_type5'] == 1)echo 'checked' ?> />  Lab Testing<br/>
                    <input type="checkbox" name="inspection_type[]" value="inspection_type6" class="" <?php if($data_row['inspection_type6'] == 1)echo 'checked' ?> /> Production Monitoring<br/>
                    <input type="checkbox" name="inspection_type[]" value="inspection_type7" class="" <?php if($data_row['inspection_type7'] == 1)echo 'checked' ?> />  Social Audit<br/>
                    <input type="checkbox" name="inspection_type[]" value="inspection_type8" class="" <?php if($data_row['inspection_type8'] == 1)echo 'checked' ?> /> Supplier Verification<br/>
                </div>
            </div>
            <div class="form-group btm_border">
                <label class="col-sm-4 control-label" for="demo-hor-12"><?php echo translate('images'); ?></label>
                <div class="col-sm-6">
                    <span class="pull-left btn btn-default btn-file"> <?php echo translate('choose_file'); ?>
                        <input type="file" multiple name="images[]" onchange="preview(this);" id="demo-hor-12" class="form-control required">
                    </span>
                    <br><br>
                    <span id="previewImg" ></span>
                </div>
            </div>
        </div>
    </form>
    </div>
    <?php
}
?>
<script>
    window.preview = function (input) {
        if (input.files && input.files[0]) {
            $("#previewImg").html('');
            $(input.files).each(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this);
                reader.onload = function (e) {
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>");
                }
            });
        }
    }
    $(document).ready(function () {
        $("form").submit(function (e) {
            return false;
        });
    });
    function get_cities(country, para) {
        console.log(country);
        var url = base_url + 'index.php/admin/getCitiesByCountry/' + country + '/' + para;
        $.ajax({
            url: url,
            beforeSend: function () {},
            success: function (data) {
                $('.' + para + '-cities').html(data);
            },
            error: function (e) {
                console.log(e)
            }
        });
    }
</script>