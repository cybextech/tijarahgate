<div>
    <div class="panel-body">
        <?php
        //print_r($shipping_countries);
        //print_r($shipping_rates);
        ?>
        <?php
        echo form_open(base_url() . 'index.php/admin/manage_shipping/do_set_rates/' . $shipping_id, array(
            'class' => 'form-horizontal',
            'method' => 'post',
            'id' => 'set_rates',
            'enctype' => 'multipart/form-data'
        ));
        ?>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-4 control-label" for="departure_country_code">Select location Country: </label>
                <div class="col-sm-6">
                    <select name="departure_country_code" onchange="get_cities(this.value, 'location')">
                        <?php
                        foreach ($shipping_countries as $sc) {
                            ?>
                            <option value="<?php echo $sc['code'] ?>">
                                <?php echo $sc['name'] . ' (' . $sc['code'] . ')' ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group location-cities">

            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="destination_country_code">Select destination Country: </label>
                <div class="col-sm-6">
                    <select name="destination_country_code" onchange="get_cities(this.value, 'destination')">
                        <?php
                        foreach ($shipping_countries as $sc) {
                            ?>
                            <option value="<?php echo $sc['code'] ?>">
                                <?php echo $sc['name'] . ' (' . $sc['code'] . ')' ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group destination-cities">

            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-2">Volume: </label>
                <div class="col-sm-6">
                    <input type="text" name="volume"  
                           value="" id="demo-hor-2" 
                           class="form-control required" placeholder="Volume" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-3">Weight: </label>
                <div class="col-sm-6">
                    <input type="text" name="weight"  
                           value="" id="demo-hor-3" 
                           class="form-control required" placeholder="Weight" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-4">Validity: </label>
                <div class="col-sm-6">
                    <input type="date" name="validity"  
                           value="" id="demo-hor-4" 
                           class="form-control required" placeholder="mm/dd/yy" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-4">Shipping Time: </label>
                <div class="col-sm-6">
                    <input type="date" name="shipping_time"  
                           value="" id="demo-hor-4" 
                           class="form-control required" placeholder="Number of days" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Medium: </label>
                <div class="col-sm-6">
                    <input type="checkbox" name="medium[]" value="land" class="" /> Land<br/>
                    <input type="checkbox" name="medium[]" value="air" class="" /> Air<br/>
                    <input type="checkbox" name="medium[]" value="express" class="" /> Express
                    <input type="checkbox" name="medium[]" value="sea_lcl_half_contain" class="" /> Sea LCL (Less then container load)
                    <input type="checkbox" name="medium[]" value="sea_lcl_full_contain" class="" /> Sea LCL (Full container load)
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1">Freight Rate: </label>
                <div class="col-sm-6">
                    <input type="text" name="freight_rate"  
                           value="" id="demo-hor-1" 
                           class="form-control required" placeholder="Freight Cost" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1">Destination Rate: </label>
                <div class="col-sm-6">
                    <input type="text" name="destination_rate"  
                           value="" id="demo-hor-1" 
                           class="form-control required" placeholder="Destination Cost" >
                </div>
            </div>

        </div>
        </form>
    </div>
</div>
<script>
    function get_cities(country, para) {
        console.log(country);
        var url = base_url + 'index.php/admin/getCitiesByCountry/' + country + '/' + para;
        $.ajax({
            url: url,
            beforeSend: function () {},
            success: function (data) {
                console.log(para + '-cities');
                $('.' + para + '-cities').html(data);
            },
            error: function (e) {
                console.log(e)
            }
        });
    }
</script>