<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow" >Pay to vendor</h1>
	</div>
	<div class="tab-base">
		<div class="panel">
			<div class="panel-body">
				<?php 
					if(isset($paid))
					{
						echo "Paid to vendor successfully!";
					}
					else
					{
						$escro = $this->crud_model->check_escro_vend_sale($sale_id , $vendor_id);
						if($escro)
						{
							$vend = $this->crud_model->get_vendor($vendor_id);
							$subtotal = $this->crud_model->get_vendor_escro_pay($sale_id , $vendor_id);
							if(isset($vend))
							{
								if($vend['stripe_set'] == "ok")
								{
									?>
										<span>Pay with stripe</span>
									<?php
									$stri = $vend['stripe_details'];
									if(!empty($stri))
									{
										$star = json_decode($stri);
										?>
										<form action="" method="POST">
										  <script
											src="https://checkout.stripe.com/checkout.js" class="stripe-button"
											data-key="<?php echo $star->publishable; ?>"
											data-amount="<?php echo $subtotal*100; ?>"
											data-name="Tijarahgate.com"
											data-description="<?php echo $this->crud_model->get_type_name_by_id('user',$row['vendor_id'],'username'); ?>"
											data-image="http://tijarahgate.com/dev/uploads/logo_image/logo_3.png"
											data-locale="auto">
										  </script>
										</form>
										<?php
									}
								}
								if($vend['paypal_set'] == "ok")
								{
									$stri = $vend['paypal_email'];
									if(!empty($stri))
									{	
										?>
										<span>Pay with Paypal</span>
										<br>
										<a href="https://www.paypal.com/row/cgi-bin/webscr?cmd=_xclick&business=<?php echo $stri; ?>&item_name=Total Amount&amount=<?php echo $subtotal; ?>&notify_url=<?php echo base_url(); ?>index.php/admin/pay_vendor/<?php echo $sale_id; ?>/<?php echo $vendor_id; ?> " class="btn btn-success">Pay with paypal</a>
										<?php 
									}
								}
								if($vend['cash_set'] == "ok")
								{
									?>
										<div class="clear"></div>
										<span>Cash Pay</span>
										<br>
										<a href="<?php echo base_url(); ?>index.php/admin/pay_vendor/<?php echo $sale_id; ?>/<?php echo $vendor_id; ?>/cash" class="btn btn-primary">Cash Pay</a>
									<?php 
								}
							}
						}
						else
						{
							redirect('home');
						}
					}
				?>
			</div>
        </div>
	</div>
</div>

