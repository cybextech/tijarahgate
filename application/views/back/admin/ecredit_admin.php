<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow" >eCredit Requests</h1>
	</div>
	<div class="col-md-12">
		<?php 
			if(isset($success))
			{
				?>
				<div class="alert alert-success">
					<?php echo $success; ?>
				</div>
				<?php
			}
			else if(isset($error))
			{
				?>
				<div class="alert alert-danger">
					<?php echo $error; ?>
				</div>
				<?php
			}
		?>
	</div>
	<div class="tab-base">
		<div class="panel">
			<div class="panel-body">
                <!-- LIST -->
                <div class="tab-pane fade active in" id="list">
                
                </div>
			</div>
        </div>
	</div>
</div>

<script>
	var base_url = '<?php echo base_url(); ?>'
	var user_type = 'admin';
	var module = 'req_ecredit';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
</script>

