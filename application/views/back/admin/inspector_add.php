<div>
    <?php
    echo form_open(base_url() . 'index.php/admin/inspector/do_add/', array(
        'class' => 'form-horizontal',
        'method' => 'post',
        'id' => 'inspector_add',
        'enctype' => 'multipart/form-data'
    ));
    ?>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-sm-4 control-label" for="departure_country_code">Country: </label>
            <div class="col-sm-6">
                <select name="country_code" onchange="get_cities(this.value, 'location')">
                    <?php
                    foreach ($shipping_countries as $sc) {
                        ?>
                        <option value="<?php echo $sc['code'] ?>">
                            <?php echo $sc['name'] . ' (' . $sc['code'] . ')' ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group location-cities">

        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="industry">Industry: </label>
            <div class="col-sm-6">
                <select name="industry">
                    <?php
                    foreach ($inspection_industries as $row) {
                        ?>
                        <option value="<?php echo $row['id'] ?>">
                            <?php echo $row['name'] ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="language"> language: </label>
            <div class="col-sm-6">
                <select name="language">
                    <?php
                    foreach ($inspection_languages as $row) {
                        ?>
                        <option value="<?php echo $row['id'] ?>">
                            <?php echo $row['name'] ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Inspector Type: </label>
            <div class="col-sm-6">
                <select name="type" >
                    <?php
                    foreach ($inspector_types as $row) {
                        ?>
                        <option value="<?php echo $row['id'] ?>">
                            <?php echo $row['name'] ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="demo-hor-1">
                <?php //echo translate('category_name');?>Inspector Name
            </label>
            <div class="col-sm-6">
                <input type="text" name="inspector_name" id="demo-hor-1" 
                       class="form-control required" placeholder="Inspector Name" >
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="demo-hor-2">
                Inspector Rate
            </label>
            <div class="col-sm-6">
                <input type="text" name="inspector_rate" id="demo-hor-2" 
                       class="form-control required" placeholder="Inspector Rate" >
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Medium: </label>
            <div class="col-sm-6">
                <input type="checkbox" name="inspection_type[]" value="inspection_type1" class="" /> Container Loading Check<br/>
                <input type="checkbox" name="inspection_type[]" value="inspection_type2" class="" />  During Production Inspection <br/>
                <input type="checkbox" name="inspection_type[]" value="inspection_type3" class="" /> Factory Audit Final Random Inspection<br/>
                <input type="checkbox" name="inspection_type[]" value="inspection_type4" class="" /> Full Inspection Initial Production Inspection<br/>
                <input type="checkbox" name="inspection_type[]" value="inspection_type5" class="" />  Lab Testing<br/>
                <input type="checkbox" name="inspection_type[]" value="inspection_type6" class="" /> Production Monitoring<br/>
                <input type="checkbox" name="inspection_type[]" value="inspection_type7" class="" />  Social Audit<br/>
                <input type="checkbox" name="inspection_type[]" value="inspection_type8" class="" /> Supplier Verification<br/>
            </div>
        </div>
        <div class="form-group btm_border">
            <label class="col-sm-4 control-label" for="demo-hor-12"><?php echo translate('images'); ?></label>
            <div class="col-sm-6">
                <span class="pull-left btn btn-default btn-file"> <?php echo translate('choose_file'); ?>
                    <input type="file" multiple name="images[]" onchange="preview(this);" id="demo-hor-12" class="form-control required">
                </span>
                <br><br>
                <span id="previewImg" ></span>
            </div>
        </div>
    </div>
</form>
</div>

<script>
    window.preview = function (input) {
        if (input.files && input.files[0]) {
            $("#previewImg").html('');
            $(input.files).each(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this);
                reader.onload = function (e) {
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>");
                }
            });
        }
    }
    $(document).ready(function () {
        $("form").submit(function (e) {
            return false;
        });
    });
    function get_cities(country, para) {
        console.log(country);
        var url = base_url + 'index.php/admin/getCitiesByCountry/' + country + '/' + para;
        $.ajax({
            url: url,
            beforeSend: function () {},
            success: function (data) {
                $('.' + para + '-cities').html(data);
            },
            error: function (e) {
                console.log(e)
            }
        });
    }
</script>