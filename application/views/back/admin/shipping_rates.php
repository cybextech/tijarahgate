<div id="content-container">
    <div id="page-title">
            <!--<h1 class="page-header text-overflow" ><?php //echo translate('manage_categories');   ?></h1>-->
        <h1 class="page-header text-overflow" >Manage Shipping</h1>
    </div>
    <div class="tab-base">
        <div class="panel">
            <div class="panel-body">
                <div class="tab-content">
                    <div style="border-bottom: 1px solid #ebebeb;padding: 25px 5px 5px 5px;"
                         class="col-md-12" >
                    </div>
                    <br>
                    <div class="tab-pane fade active in" 
                         id="list" style="border:1px solid #ebebeb; border-radius:4px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'manage_shipping';
    var list_cont_func = 'list_shipping_rates';
    var dlt_cont_func = 'delete_ship_rate';
</script>

