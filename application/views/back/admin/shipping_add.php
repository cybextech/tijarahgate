<div>
    <?php
		echo form_open(base_url() . 'index.php/admin/manage_shipping/do_add/', array(
			'class' => 'form-horizontal',
			'method' => 'post',
			'id' => 'shipping_add',
			'enctype' => 'multipart/form-data'
		));
	?>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1">
                	<?php //echo translate('category_name');?>
                    	Shipping Name
                        </label>
                <div class="col-sm-6">
                    <input type="text" name="shipping_name" id="demo-hor-1" 
                    	class="form-control required" placeholder="Shipping Name<?php //echo translate('category_name');?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-2">
                	<?php //echo translate('category_name');?>
                    	Shipping Email
                        </label>
                <div class="col-sm-6">
                    <input type="text" name="shipping_email" id="demo-hor-2" 
                    	class="form-control required" placeholder="Shipping Email<?php //echo translate('category_name');?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-3">
                	<?php //echo translate('category_name');?>
                    	Shipping Password
                        </label>
                <div class="col-sm-6">
                    <input type="password" name="shipping_password" id="demo-hor-3" 
                    	class="form-control required" placeholder="Shipping Password<?php //echo translate('category_name');?>" >
                </div>
            </div>
        </div>
	</form>
</div>

<script>
	$(document).ready(function() {
		$("form").submit(function(e){
			return false;
		});
	});
</script>