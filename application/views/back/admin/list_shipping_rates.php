<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,2" data-show-toggle="true" data-show-columns="false" data-search="true" >s
        <thead>
            <tr>
                <th><?php echo translate('no'); ?></th>
                <th><?php echo translate('Departure'); ?></th>
                <th><?php echo translate('Destination'); ?></th>
                <th><?php echo translate('Volume'); ?></th>
                <th><?php echo translate('Weight'); ?></th>
                <th><?php echo translate('Price Valid Until'); ?></th>
                <th><?php echo translate('Shipping Time Direct'); ?></th>
                <th><?php echo translate('Freight Cost'); ?></th>
                <th><?php echo translate('Destination Cost'); ?></th>
                <th><?php echo translate('Medium'); ?></th>
                <th class="text-right"><?php echo translate('options'); ?></th>
            </tr>
        </thead>

        <tbody >
            <?php
            $i = 0;
            foreach ($shipping_rates as $row) {
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <?php
                    $departure_country = array();
                    $departure_city = '';
                    $departure_country = $this->db->get_where('shipping_country', array('code' => $row['departure_country_code']))->result_array();
                    $departure_city = $this->db->get_where('shipping_city', array('id' => $row['departure_city_id']))->result_array();
                    if (!empty($departure_city))
                        $departure_city = $departure_city[0]['name'] . ', ';
                    else
                        $departure_city = '';
                    ?>
                    <td><?php echo $departure_city . $departure_country[0]['name']; ?></td>
                    <?php
                    $destination_country = array();
                    $destination_city = '';
                    $destination_country = $this->db->get_where('shipping_country', array('code' => $row['destination_country_code']))->result_array();
                    $destination_city = $this->db->get_where('shipping_city', array('id' => $row['destination_city_id']))->result_array();
                    if (!empty($destination_city))
                        $destination_city = $destination_city[0]['name'] . ', ';
                    else
                        $destination_city = '';
                    ?>
                    <td><?php echo $destination_city . $destination_country[0]['name'] ?></td>
                    <td><?php echo $row['volume']; ?>cm</td>
                    <td><?php echo $row['weight']; ?>kg</td>
                    <td><?php echo $row['shipping_time']; ?></td>
                    <td><?php echo $row['validity']; ?></td>
                    <td><?php echo $row['freight_rate']; ?></td>
                    <td><?php echo $row['destination_rate']; ?></td>
                    <?php
                    $medium = '';
                    if ($row['air'] == 1)
                        $medium[] = 'Air';
                    if ($row['land'] == 1)
                        $medium[] = 'Land';
                    if ($row['sea_lcl_half_contain'] == 1)
                        $medium[] = 'Sea LCL (Less then container load)';
                    if ($row['sea_lcl_full_contain'] == 1)
                        $medium[] = 'Sea LCL (Full container load)';

                    if (!empty($medium) && is_array($medium))
                        $medium = implode(',', $medium);
                    else
                        $medium = '';
                    ?>
                    <td><?php echo $medium ?></td>
                    <td class="text-right">

                        <a class="btn btn-success btn-xs btn-labeled" data-toggle="tooltip" 
                           onclick="ajax_modal('set_rates', 'Set Rates', 'Success', 'set_rates', '<?php echo $row['shipping_id']; ?>')" 
                           data-original-title="Edit" data-container="body">
                            Set Rates
                        </a>
                        <a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" 
                           onclick="ajax_modal('edit_shipping_rate', 'Edit Shipping rate', '<?php echo translate('successfully_edited!'); ?>', 'edit_shipping_rate', '<?php echo $row['id']; ?>')" 
                           data-original-title="Edit" data-container="body">
                               <?php echo translate('edit'); ?>
                        </a>
                        <a onclick="delete_confirm('<?php echo $row['id']; ?>', '<?php echo translate('really_want_to_delete_this?'); ?>')" class="btn btn-danger btn-xs btn-labeled fa fa-trash" data-toggle="tooltip" 
                           data-original-title="Delete" data-container="body">
                               <?php echo translate('delete'); ?>
                        </a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>

<div id='export-div'>
        <!--<h1 style="display:none;"><?php echo translate('category'); ?></h1>-->
    <h1 style="display:none;">Shipping</h1>
    <table id="export-table" data-name='category' data-orientation='p' style="display:none;">
        <thead>
            <tr>
                <th><?php echo translate('no'); ?></th>
                <th><?php echo translate('Departure'); ?></th>
                <th><?php echo translate('Destination'); ?></th>
                <th><?php echo translate('Volume'); ?></th>
                <th><?php echo translate('Weight'); ?></th>
                <th><?php echo translate('Price Valid Until'); ?></th>
                <th><?php echo translate('Shipping Time Direct'); ?></th>
                <th><?php echo translate('Freight Cost'); ?></th>
                <th><?php echo translate('Destination Cost'); ?></th>
                <th><?php echo translate('Medium'); ?></th>
                <th class="text-right"><?php echo translate('options'); ?></th>
            </tr>
        </thead>
        <tbody >
            <?php
            $i = 0;
            foreach ($shipping_rates as $row) {
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <?php
                    $departure_country = array();
                    $departure_city = '';
                    $departure_country = $this->db->get_where('shipping_country', array('code' => $row['departure_country_code']))->result_array();
                    $departure_city = $this->db->get_where('shipping_city', array('id' => $row['departure_city_id']))->result_array();
                    if (!empty($departure_city))
                        $departure_city = $departure_city[0]['name'] . ', ';
                    else
                        $departure_city = '';
                    ?>
                    <td><?php echo $departure_city . $departure_country[0]['name']; ?></td>
                    <?php
                    $destination_country = array();
                    $destination_city = '';
                    $destination_country = $this->db->get_where('shipping_country', array('code' => $row['destination_country_code']))->result_array();
                    $destination_city = $this->db->get_where('shipping_city', array('id' => $row['destination_city_id']))->result_array();
                    if (!empty($destination_city))
                        $destination_city = $destination_city[0]['name'] . ', ';
                    else
                        $destination_city = '';
                    ?>
                    <td><?php echo $destination_city . $destination_country[0]['name'] ?></td>
                    <td><?php echo $row['volume']; ?>cm</td>
                    <td><?php echo $row['weight']; ?>kg</td>
                    <td><?php echo $row['shipping_time']; ?></td>
                    <td><?php echo $row['validity']; ?></td>
                    <td><?php echo $row['freight_rate']; ?></td>
                    <td><?php echo $row['destination_rate']; ?></td>
                    <?php
                    $medium = '';
                    if ($row['air'] == 1)
                        $medium[] = 'Air';
                    if ($row['land'] == 1)
                        $medium[] = 'Land';
                    if ($row['sea_lcl_half_contain'] == 1)
                        $medium[] = 'Sea LCL (Less then container load)';
                    if ($row['sea_lcl_full_contain'] == 1)
                        $medium[] = 'Sea LCL (Full container load)';

                    if (!empty($medium) && is_array($medium))
                        $medium = implode(',', $medium);
                    else
                        $medium = '';
                    ?>
                    <td><?php echo $medium ?></td>
                    <td class="text-right">

                        <a class="btn btn-success btn-xs btn-labeled" data-toggle="tooltip" 
                           onclick="ajax_modal('set_rates', 'Set Rates', 'Success', 'set_rates', '<?php echo $row['shipping_id']; ?>')" 
                           data-original-title="Edit" data-container="body">
                            Set Rates
                        </a>
                        <a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" 
                           onclick="ajax_modal('edit_shipping_rate', 'Edit Shipping rate', '<?php echo translate('successfully_edited!'); ?>', 'edit_shipping_rate', '<?php echo $row['id']; ?>')" 
                           data-original-title="Edit" data-container="body">
    <?php echo translate('edit'); ?>
                        </a>
                        <a onclick="delete_confirm('<?php echo $row['id']; ?>', '<?php echo translate('really_want_to_delete_this?'); ?>')" class="btn btn-danger btn-xs btn-labeled fa fa-trash" data-toggle="tooltip" 
                           data-original-title="Delete" data-container="body">
    <?php echo translate('delete'); ?>
                        </a>
                    </td>
                </tr>
    <?php
}
?>
        </tbody>
    </table>
</div>

