<?php foreach ($shipping_rate_data as $row) { ?>
    <div>

        <div class="panel-body">
            <?php
            //print_r($shipping_countries);
            //print_r($shipping_rates);
            ?>
            <?php
            echo form_open(base_url() . 'index.php/admin/manage_shipping/do_update_set_rates/' . $row['id'], array(
                'class' => 'form-horizontal',
                'method' => 'post',
                'id' => 'edit_shipping_rate',
                'enctype' => 'multipart/form-data'
            ));
            ?>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="departure_country_code">Select location Country: </label>
                    <div class="col-sm-6">
                        <select name="departure_country_code" onchange="get_cities(this.value, 'location')">
                            <?php
                            foreach ($shipping_countries as $sc) {
                                ?>
                                <option value="<?php echo $sc['code'] ?>" <?php if ($row['departure_country_code'] == $sc['code']) echo 'selected' ?>>
                                    <?php echo $sc['name'] . ' (' . $sc['code'] . ')' ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group location-cities">
                    <?php
                    if ($row['departure_city_id'] != ''):
                        $depatrure_cities = $this->db->get_where('shipping_city', array('countryCode' => $row['departure_country_code']))->result_array();
                        ?>
                        <label class="col-sm-4 control-label" for="demo-hor-1">Select City: </label>
                        <div class="col-sm-6">
                            <select name="location_city_id">
                                <?php foreach ($depatrure_cities as $city) { ?>
                                    <option value="<?php echo $city['id'] ?>" <?php if ($city['id'] == $row['depature_city_id']) echo 'selected' ?>>
                                        <?php echo $city['name'] ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="destination_country_code">Select destination Country: </label>
                    <div class="col-sm-6">
                        <select name="destination_country_code" onchange="get_cities(this.value, 'destination')">
                            <?php
                            foreach ($shipping_countries as $sc) {
                                ?>
                                <option value="<?php echo $sc['code'] ?>" <?php if ($sc['code'] == $row['destination_country_code']) echo 'selected'; ?>>
                                    <?php echo $sc['name'] . ' (' . $sc['code'] . ')' ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group destination-cities">
                    <?php
                    if ($row['destination_city_id'] != ''):
                        $destination_cities = $this->db->get_where('shipping_city', array('countryCode' => $row['destination_country_code']))->result_array();
                        ?>
                        <label class="col-sm-4 control-label" for="demo-hor-1">Select City: </label>
                        <div class="col-sm-6">
                            <select name="destination_city_id">
                                    <?php foreach ($destination_cities as $citiy) { ?>
                                    <option value="<?php echo $citiy['id'] ?>" <?php if ($citiy['id'] == $row['destination_city_id']) echo 'selected' ?>>
                                    <?php echo $citiy['name'] ?>      
                                    </option>
                                    <?php } ?>
                            </select>
                        </div>
                        <?php endif; ?>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-2">Volume: </label>
                    <div class="col-sm-6">
                        <input type="text" name="volume"  
                               value="<?php echo $row['volume'] ?>" id="demo-hor-2" 
                               class="form-control required" placeholder="Volume" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-3">Weight: </label>
                    <div class="col-sm-6">
                        <input type="text" name="weight"  
                               value="<?php echo $row['weight'] ?>" id="demo-hor-3" 
                               class="form-control required" placeholder="Weight" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-4">Validity: </label>
                    <div class="col-sm-6">
                        <input type="date" name="validity"  
                               value="<?php echo $row['validity'] ?>" id="demo-hor-4" 
                               class="form-control required" placeholder="mm/dd/yy" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-4">Shipping Time: </label>
                    <div class="col-sm-6">
                        <input type="date" name="shipping_time"  
                               value="<?php echo $row['shipping_time'] ?>" id="demo-hor-4" 
                               class="form-control required" placeholder="Number of days" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Medium: </label>
                    <div class="col-sm-6">
                        <input type="checkbox" name="medium[]" value="land" <?php if ($row['land'] == 1) echo 'checked' ?> class="" /> Land<br/>
                        <input type="checkbox" name="medium[]" value="air" <?php if ($row['air'] == 1) echo 'checked' ?> class="" /> Air<br/>
                        <input type="checkbox" name="medium[]" value="express" <?php if ($row['express'] == 1) echo 'checked' ?> class="" /> Express
                        <input type="checkbox" name="medium[]" value="sea_lcl_half_contain" <?php if ($row['sea_lcl_half_contain'] == 1) echo 'checked' ?> class="" /> Sea LCL (Less then container load)
                        <input type="checkbox" name="medium[]" value="sea_lcl_full_contain" <?php if ($row['sea_lcl_full_contain'] == 1) echo 'checked' ?> class="" /> Sea LCL (Full container load)
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-1">Freight Rate: </label>
                    <div class="col-sm-6">
                        <input type="text" name="freight_rate"  
                               value="<?php echo $row['freight_rate'] ?>" id="demo-hor-1" 
                               class="form-control required" placeholder="Freight Cost" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-1">Destination Rate: </label>
                    <div class="col-sm-6">
                        <input type="text" name="destination_rate"  
                               value="<?php echo $row['destination_rate'] ?>" id="demo-hor-1" 
                               class="form-control required" placeholder="Destination Cost" >
                    </div>
                </div>

            </div>
            </form>
        </div>
    </div>
<?php } ?>
<script>
    function get_cities(country, para) {
        console.log(country);
        var url = base_url + 'index.php/admin/getCitiesByCountry/' + country + '/' + para;
        $.ajax({
            url: url,
            beforeSend: function () {},
            success: function (data) {
                console.log(para + '-cities');
                $('.' + para + '-cities').html(data);
            },
            error: function (e) {
                console.log(e)
            }
        });
    }
</script>
