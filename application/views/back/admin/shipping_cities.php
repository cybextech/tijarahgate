<?php 
	if(isset($shipping_cities))
	{
		?>
			<label class="col-sm-4 control-label" for="demo-hor-1">Select City: </label>
			<div class="col-sm-6">
			<select name="<?php echo $para ?>_city_id">
				<?php
				foreach($shipping_cities as $sc) {
					?>
					<option value="<?php echo $sc['id'] ?>">
						<?php echo $sc['name'] ?>
					</option>
					<?php
				}
				?>
			</select>
			</div>
		<?php
	}
	else if(isset($shipping_cities1))
	{
		?>
		<label class="input login-input">
			<span class="jcf-select jcf-unselectable jcf-select-form-control">
				<select class="form-control jcf-reset-appearance" id="<?php echo $para ?>_city_id" name="<?php echo $para ?>_city_id">
					<?php
					foreach($shipping_cities1 as $sc) 
					{
						?>
						<option value="<?php echo $sc['id'] ?>"><?php echo $sc['name'] ?></option>
						<?php
					}
					?>
				</select>
			</span>
		</label>
		<?php
	}
	else if(isset($shipping_cities2))
	{
		?>
		<label class="input login-input">
			<span class="jcf-select jcf-unselectable jcf-select-form-control">
				<select class="form-control jcf-reset-appearance" id="<?php echo $para ?>_city_id" name="<?php echo $para ?>_city_id">
					<?php
					foreach($shipping_cities2 as $sc) 
					{
						?>
						<option value="<?php echo $sc['id'] ?>"><?php echo $sc['name'] ?></option>
						<?php
					}
					?>
				</select>
			</span>
		</label>
		<?php
	}
?>