<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true"  data-show-toggle="true" data-show-columns="true" data-search="true" >

        <thead>
            <tr>
				<th><?php echo translate('ID');?></th>
				<th><?php echo translate('name');?></th>
				<th><?php echo translate('email');?></th>
				<th><?php echo translate('discrition');?></th>
				<th><?php echo translate('date');?></th>
				<th><?php echo translate('amount');?></th>
				<th><?php echo translate('status');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>     
        <tbody>
        <?php
            $i = 0;
            foreach($all_sales as $row){
                $i++; 
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $this->crud_model->get_type_name_by_id('user',$row['uid'],'username'); ?></td>
			<td><?php echo $this->crud_model->get_type_name_by_id('user',$row['uid'],'email'); ?></td>
            <td><?php echo $row['discription']; ?></td>
            <td><?php echo $row['date_time']; ?></td>
            <td><?php echo currency().$this->cart->format_number($row['amount']); ?></td>
            <td>
                <div class="label label-<?php if($row['pay_status'] == 1){ ?>success<?php } else if($row['pay_status'] == 2){ ?>danger<?php } else { ?>info<?php } ?>">
					<?php 
						if($row['pay_status'] == 0)
						{
							echo "Pending";
						}
						else if($row['pay_status'] == 2)
						{
							echo "Rejected";
						}
						else
						{
							echo "Release Payment";
						}
					?>
                </div>
            </td>
            <td class="text-right">
				<?php 
				if($row['pay_status'] == 0)
				{
					?>
                <a href="<?php echo base_url(); ?>index.php/admin/accept_ecredit/<?php echo $row['id']; ?>" class="btn btn-success btn-xs btn-labeled fa fa-usd">
					Accept
                </a>
				<a href="<?php echo base_url(); ?>index.php/admin/notacc_ecredit/<?php echo $row['id']; ?>" class="btn btn-danger btn-xs btn-labeled fa fa-usd">
					Reject
                </a>
				<?php 
				}
				?>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>  
<style type="text/css">
	.pending{
		background: #D2F3FF  !important;
	}
	.pending:hover{
		background: #9BD8F7 !important;
	}
</style>